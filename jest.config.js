module.exports = {
  globals: {
    "ts-jest": {
      tsConfig: "./tsconfig.test.json",
      diagnostics: true,
    },
  },
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  testRegex: "(/__tests__/.*|\\.(test|spec))(?<!d)\\.(ts|tsx|js)$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
};
