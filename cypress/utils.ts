export function promisify<T>(param: any): Promise<T> {
  return new Promise(res => param.then(res)) as Promise<T>;
}
