describe("Labs UI Bundle Flow", () => {
  const bundleName = "Test Bundle from E2E Test";
  before(() => {
    cy.login();
  });
  beforeEach(() => {
    cy.getCookie("double-cookie").then(cookie => expect(cookie).to.not.be.null);
    Cypress.Cookies.preserveOnce("idp-session");
    Cypress.Cookies.preserveOnce("double-cookie");
  });
  it("Should enter the Labs UI", () => {
    cy.contains("Bundles").click();
  });
  it("Should render list of bundles", () => {
    cy.visit("/");
    cy.contains("Create/Import Bundle");
  });

  it("Should create a bundle", () => {
    cy.contains("Contiamo").click();
    cy.contains("Frontends").click();
    cy.wait(300);
    cy.contains("Import Bundle").click({ force: true });
    cy.contains("Name")
      .find("input")
      .type(bundleName);
    cy.contains("Git URL")
      .find("input")
      .type("git@github.com:contiamo/fibonacci-demo-bundle.git");
    cy.contains("Branch")
      .find("input")
      .type("master");

    cy.get("button").click();
    cy.contains("button", "Deploy Bundle", { timeout: 100000 });
  });

  it("Should start an editor session", () => {
    cy.contains(bundleName)
      .siblings()
      .find("button")
      .click();

    cy.contains("Create Editor").click();
    cy.contains("Editor starting");
    cy.contains("div", "Editor running", { timeout: 10000 });
  });

  it("Should sync a bundle", () => {
    cy.contains("Overview").click();
    cy.contains("Sync Bundle").click();
    cy.contains("Jobs").click();
    cy.contains("labs-sync").click();
    cy.contains("in progress");
  });

  it("Should deploy a bundle", () => {
    cy.contains("Overview").click();
    cy.contains("Deploy Bundle").click();
    cy.contains("Jobs").click();
    cy.contains("labs-deploy").click();
    cy.contains("in progress");
  });

  it("Should rename a bundle", () => {
    cy.contains(bundleName);
    cy.contains("Settings").click();
    cy.contains("Bundle name")
      .find("input")
      .clear()
      .type("New Name");
    cy.contains("Rename").click();
    cy.contains("Bundle: New Name");
  });

  it("Should delete a bundle", () => {
    cy.contains("Bundle: New Name");
    cy.contains("Settings").click();
    cy.contains("Delete this bundle").click();
    cy.get("[data-cypress=bundle-delete-confirm]").click();
    cy.contains("Bundle has been deleted");
  });
});
