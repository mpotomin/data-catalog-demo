import { promisify } from "../utils";

describe("Pantheon", () => {
  before(() => {
    cy.login();
  });

  it("Should pass Pantheon demo happy path", async function happyPath() {
    const testRandomSeed = Math.floor(Date.now() / 1000);
    const projectName = `e2e-test-project-${testRandomSeed}`;
    const dataSourceName = `e2e-test-data-source-${testRandomSeed}`;
    const schemaName = `e2e-test-schema-${testRandomSeed}`;
    const endpointName = `e2e-test-endpoint-${testRandomSeed}`;
    const serviceAccountName = `e2e-test-service-account-${testRandomSeed}`;

    cy.viewport(1280, 1024);

    cy.visit("/");
    cy.wait(3000);

    cy.createProject(projectName);
    cy.wait(1000);

    cy.openProject(projectName);
    cy.wait(1000);

    cy.addDataSource(dataSourceName, "foodmart");
    cy.wait(3000);

    const schemaTemplate = await promisify<string>(cy.loadSchemaTemplate(schemaName, dataSourceName));
    cy.createSchema(schemaName, schemaTemplate);

    const endpointUrl = await promisify<string>(cy.createEndpoint(schemaName, endpointName));
    const serviceAccountToken = await promisify<string>(cy.createServiceAccount(serviceAccountName));
    cy.get("[data-cy=idp--user-management--service-accounts--create-service-account__close-button]").click();

    cy.modifyEndpointPermissions(endpointName, serviceAccountName);

    cy.queryEndpoint(endpointUrl, serviceAccountToken);

    cy.contains(projectName).click();
    cy.contains("Organization").click();
    cy.deleteProject(projectName);
  });
});
