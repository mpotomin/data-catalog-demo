// add new command to the existing Cypress interface
declare global {
  namespace Cypress {
    interface Chainable {
      login: typeof login;
      createProject: typeof createProject;
      openProject: typeof openProject;
      addDataSource: typeof addDataSource;
      loadSchemaTemplate: typeof loadSchemaTemplate;
      createSchema: typeof createSchema;
      createEndpoint: typeof createEndpoint;
      createServiceAccount: typeof createServiceAccount;
      modifyEndpointPermissions: typeof modifyEndpointPermissions;
      queryEndpoint: typeof queryEndpoint;
      deleteProject: typeof deleteProject;
      recursivelyDeleteProjectsWithNamePrefix: typeof recursivelyDeleteProjectsWithNamePrefix;
    }
  }
}

Cypress.Commands.add("login", login);
Cypress.Commands.add("createProject", createProject);
Cypress.Commands.add("openProject", openProject);
Cypress.Commands.add("addDataSource", addDataSource);
Cypress.Commands.add("loadSchemaTemplate", loadSchemaTemplate);
Cypress.Commands.add("createSchema", createSchema);
Cypress.Commands.add("createEndpoint", createEndpoint);
Cypress.Commands.add("createServiceAccount", createServiceAccount);
Cypress.Commands.add("modifyEndpointPermissions", modifyEndpointPermissions);
Cypress.Commands.add("queryEndpoint", queryEndpoint);
Cypress.Commands.add("deleteProject", deleteProject);
Cypress.Commands.add("recursivelyDeleteProjectsWithNamePrefix", recursivelyDeleteProjectsWithNamePrefix);

export function login(): void {
  cy.fixture("user").then(({ email, tenant, password }) => {
    cy.request({
      url: "",
      method: "POST",
      body: {
        email,
        password,
        tenant,
      },
    });
  });
}

export function createProject(name: string): void {
  cy.get("[data-cy=idp--projects-tab__create-project]").click();
  cy.get("[data-cy=idp--create-project-popup__name-input]").type(name, { delay: 1 });
  cy.get("[data-cy=idp--create-project-popup__submit-button]").click();
}

export function openProject(projectName: string): void {
  cy.get("[data-cy=idp--projects-tab__project-card]")
    .contains(projectName)
    .click();
}

export function addDataSource(name: string, dataSourceType: string): void {
  cy.get("[data-cy=pantheon--sidenav__data-sources]").click();
  cy.get("[data-cy=pantheon--data-sources__create-datasource-button]").click();
  cy.get(`[data-cy=pantheon--data-source-wizard--source__demo-data-${dataSourceType}]`).click();
  cy.get("[data-cy=pantheon--data-source-wizard__name_input]").type(name);
  cy.get("[data-cy=pantheon--data-source-wizard__test-connection]").click();
  cy.get("[data-cy=pantheon--data-source-wizard--connection-test__success]");
  cy.get("[data-cy=pantheon--data-source-wizard__create-button]").click();
}

export function loadSchemaTemplate(schemaName: string, dataSourceName: string): Cypress.Chainable<string> {
  return cy.fixture("schema.psl").then((schemaString: string) => {
    schemaString = schemaString.replace(/<SCHEMA_NAME>/, schemaName);
    schemaString = schemaString.replace(/<DATA_SOURCE_NAME>/, dataSourceName);
    return schemaString;
  });
}

export function createSchema(schemaName: string, schemaTemplate: string): void {
  cy.get("[data-cy=pantheon--sidenav__schemas]").click();
  cy.wait(3000);
  cy.get("[data-cy=pantheon--schemas__create-schema]").click();
  cy.get("[data-cy=pantheon--new-schema__name-input]").type(schemaName);
  cy.get("[data-cy=pantheon--new-schema__save-schema]").click();

  cy.wait(3000);
  cy.get("[data-cy=pantheon--schema__edit-button]").click();

  cy.get(".monaco-editor .inputarea")
    .type("{command}A")
    .type("{backspace}");

  cy.get(".monaco-editor")
    .find(".inputarea")
    .then(el => {
      el.val(schemaTemplate);
      // Hack: Is needed to make the monaco editor validate the input
      // https://github.com/cypress-io/cypress/issues/1123
      el.trigger("change");
    })
    // Hack: Forcing the monaco editor to refresh after pasting
    .then(() => {
      cy.get(".monaco-editor .inputarea").type("a");
      cy.get(".monaco-editor .inputarea").type("{backspace}");
    })
    .wait(3000)
    .get("[data-cy=pantheon--schema__save-button]")
    .click();
}

export function createEndpoint(schemaName: string, endpointName: string): Cypress.Chainable<string> {
  cy.get("[data-cy=pantheon--sidenav__schemas]").click();
  cy.contains(schemaName).click();
  cy.get("[data-cy=pantheon--schema__query-editor-button]").click();

  // Adding measures
  cy.contains("unitSales")
    .trigger("mousedown")
    .trigger("dragstart")
    .trigger("drag");

  cy.get("[data-cy=pantheon--aggreate-view__measures-drop-area]")
    .trigger("dragover")
    .trigger("drop")
    .trigger("dragend")
    .trigger("mouseup");

  cy.get("[data-cy=pantheon--aggreate-view__run-query-button]").click();
  cy.wait(3000);

  // unitSales query result
  cy.contains("509987");

  // Adding Dimensions
  cy.contains("city")
    .trigger("mousedown")
    .trigger("dragstart")
    .trigger("drag");

  cy.get("[data-cy=pantheon--aggreate-view__rows-drop-area]")
    .trigger("dragover")
    .trigger("drop")
    .trigger("dragend")
    .trigger("mouseup");

  cy.get("[data-cy=pantheon--aggreate-view__run-query-button]").click();

  cy.wait(3000);

  // Validate some results
  cy.contains("Acapulco");
  cy.contains("14590");

  cy.contains("La Cruz");
  cy.contains("8636");

  cy.contains("Sedro Woolley");
  cy.contains("545");

  cy.contains("Anacortes");
  cy.contains("631");

  // Create endpoint
  cy.get("[data-cy=pantheon--aggreate-view__create-endpoint-button]").click();

  cy.get("[data-cy=pantheon--new-endpoint-modal__name-field]").type(endpointName);
  cy.get("[data-cy=pantheon--new-endpoint-modal__save-button]").click();

  cy.wait(1000);

  cy.get("[data-cy=pantheon--sidenav__endpoints]").click();
  cy.contains(endpointName).click();

  cy.wait(3000);

  cy.contains("Test Endpoint").click();

  return cy.get("[data-cy=pantheon--endpoints__endpoint-url]").then(el => {
    const endpointUrl = el.val() as string;
    return endpointUrl;
  });
}

export function createServiceAccount(serviceAccountName: string): Cypress.Chainable<string> {
  cy.get("[data-cy=contiamo-ui__user-management]").click();
  cy.wait(1000);
  cy.contains("Service Accounts").click();

  cy.wait(1000);
  cy.get("[data-cy=idp--user-management--service-accounts__create-service-account]").click();

  cy.get("[data-cy=idp--user-management--service-accounts--create-service-account__name-input]").type(
    serviceAccountName,
  );

  cy.get("[data-cy=idp--user-management--service-accounts--create-service-account__create-button]").click();

  cy.wait(1000);

  return cy.get("[data-cy=idp--user-management--service-accounts--create-service-account__token]").then(el => {
    const token = el[0].innerText;
    return token;
  });
}

export function modifyEndpointPermissions(endpointName: string, serviceAccountName: string): void {
  cy.get("[data-cy=pantheon--sidenav__endpoints]").click();
  cy.wait(1000);

  // Cypress hover issue https://github.com/cypress-io/cypress/issues/2928
  cy.contains(endpointName)
    .parent("td")
    .siblings()
    .last()
    .trigger("mouseover")
    .click();

  cy.contains("Permissions").click();

  cy.wait(1000);

  cy.get("[data-cy=resource-role-assigner--permissions]")
    .contains("Service Accounts")
    .click();

  cy.contains(serviceAccountName).click();
  cy.wait(1000);
  cy.contains("[data-cy=resource-role-assigner__close-button]").click();
}

export function queryEndpoint(endpointUrl: string, token: string): void {
  cy.request({
    url: endpointUrl,
    failOnStatusCode: false,
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
      accept: "*/*",
    },
    body: JSON.stringify({
      _page: 1,
      _pageSize: 1,
    }),
  }).then(response => {
    expect(response)
      .property("status")
      .to.equal(200);
  });
}

export function deleteProject(projectName: string): void {
  cy.get("[data-cy=idp--projects-tab__project-card]")
    .contains(projectName)
    .parents("[data-cy=idp--projects-tab__project-card]")
    .find("[data-cy=card-section__action-menu]")
    .click()
    .contains("Delete")
    .click();

  cy.get("[data-cy=confirm__actions]")
    .contains("Delete")
    .click();
}

export function recursivelyDeleteProjectsWithNamePrefix(namePrefix: string): void {
  cy.deleteProject(namePrefix);
  cy.wait(3000);
  cy.recursivelyDeleteProjectsWithNamePrefix(namePrefix);
  return;
}
