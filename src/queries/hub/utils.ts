import { dropColumnOptionValue } from "../../DataStore/components/DiscoverDataStructure/MapSchemaView/columnMappingUtils";
import { ErrorResponse, getFieldError, isErrorResponse } from "../../util/errors";
import { ManagedTableColumn, TableFieldSpec } from "./hub";

import {
  ColumnMapping,
  DataSourceResponse,
  FieldErrorResponse,
  FilePreviewResponse,
  GeneralErrorResponse,
  SchemaPreviewCell,
} from "./hub";

export const schemaColumnPrefix = "schema.columns";
export const schemaColumnNameSuffix = ".name";

export const getColumnHeaderError = (error: ErrorResponse, index: number) => {
  const headerError = getFieldError(error, `${schemaColumnPrefix}[${index}]`);
  if (headerError && headerError.length) {
    return headerError;
  }

  const alternativeFormatError = getFieldError(error, `${schemaColumnPrefix}[${index}]${schemaColumnNameSuffix}`);
  return alternativeFormatError ? alternativeFormatError : "";
};

export const getSchemaColumnHeaderErrorsCount = (error: ErrorResponse | null) => {
  if (!isErrorResponse(error)) {
    return 0;
  }

  const columnErrors = error.errors.filter(err => {
    if (err.type !== "FieldError") {
      return false;
    }
    return err.key.startsWith(schemaColumnPrefix);
  });

  return columnErrors.filter(err => err.message.length > 0).length;
};

export const getColumnMappingStatus = (columnMapping: ColumnMapping) => {
  const nonDroppedColumns = columnMapping.filter(item => item.target !== dropColumnOptionValue);
  if (nonDroppedColumns.length < 1) {
    return "all-columns-dropped";
  }

  const uniqueTargetColumnNames = nonDroppedColumns.reduce(
    (accumulator, column) => accumulator.add(column.target),
    new Set(),
  );
  if ([...uniqueTargetColumnNames].length !== nonDroppedColumns.length) {
    return "non-unique-target-columns";
  }

  return "valid";
};

export const getPreviewCellErrorsCount = (preview: FilePreviewResponse["preview"], columnMapping?: ColumnMapping) => {
  if (!preview) {
    return 0;
  }

  const rowErrors = preview.map((row: SchemaPreviewCell[]) =>
    row.filter((cell, index) => {
      if (columnMapping && columnMapping[index].target === dropColumnOptionValue) {
        return false;
      }
      return cell.error.length > 0;
    }),
  );

  return rowErrors.reduce((counter: number, row: SchemaPreviewCell[]) => counter + row.length, 0);
};

export const defaultSchemaColumn: ManagedTableColumn = Object.freeze({
  name: "",
  type: "text" as const,
  required: false,
});

export const getEmptyErrorTemplate = (): ErrorResponse => ({ errors: [] });

export const getEmptyTableFieldSpec = (): TableFieldSpec => ({ tables: [], columns: [] });

export const isDataSourceSynching = (dataSource: DataSourceResponse) =>
  dataSource.syncTask === null || dataSource.syncTask.status === "waiting" || dataSource.syncTask.status === "running";

export type HubErrorResponse = GeneralErrorResponse | FieldErrorResponse;

export const isHubErrorResponse = (input: any): input is HubErrorResponse =>
  input.errors !== undefined && Array.isArray(input.errors);
