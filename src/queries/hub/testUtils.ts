import { TableResponse } from "./hub";
import { ErrorResponse } from "../../util/errors";
import { defaultSchemaColumn, schemaColumnNameSuffix, schemaColumnPrefix } from "./utils";

import {
  ColumnMapping,
  ColumnResponse,
  DataSourceActions,
  DataSourceResponse,
  DataSourceSyncTask,
  FieldErrorResponse,
  FieldSpec,
  FileOptions,
  ManagedTableSchema,
  PagedDataSourceResponse,
  PagedTableResponse,
  TableFieldSpec,
  TableInstanceResponse,
} from "./hub";

export const getCollectionTemplate = (): DataSourceResponse => ({
  id: "",
  projectId: "",
  name: "",
  icon: "",
  type: "managed" as const,
  description: "",
  documentation: "",
  isPublic: true,
  createdAt: "",
  updatedAt: "",
  props: {},
  syncTask: null,
  resourceActions: {
    role: "admin",
    read: true,
    createTable: true,
    edit: true,
    editMetadata: true,
    delete: true,
    upload: true,
    grant: true,
    query: true,
    refresh: true,
  },
});

export const getTestErrorTemplate = (): FieldErrorResponse => ({
  errors: [
    {
      type: "FieldError",
      message: "error message 1",
      key: "Description",
    },
    {
      type: "FieldError",
      message: "some name error",
      key: "Name",
    },
  ],
});

export const getSchemaColumnErrorTemplate = (): ErrorResponse => ({
  errors: [
    {
      type: "FieldError",
      message: "error description",
      key: `${schemaColumnPrefix}[2]${schemaColumnNameSuffix}`,
    },
  ],
});

export const getSchemaTemplate = (): ManagedTableSchema => ({
  columns: [
    { ...defaultSchemaColumn, name: "col0", required: false },
    { ...defaultSchemaColumn, name: "col1", required: false },
    { ...defaultSchemaColumn, name: "col2", required: false },
  ],
  indexes: [],
});

export const getAlternativeSchemaTemplate = (): ManagedTableSchema => ({
  columns: [
    { ...defaultSchemaColumn, name: "A", required: false },
    { ...defaultSchemaColumn, name: "col1", required: false },
    { ...defaultSchemaColumn, name: "C", required: false },
  ],
  indexes: [],
});

export const getColumnMappingTemplate = (): ColumnMapping => [
  { source: "source-1", target: "target-1" },
  { source: "source-2", target: "target-2" },
  { source: "source-3", target: "target-3" },
];

export const getFileOptionsTemplate = (): FileOptions => ({
  preferredDateFormat: "dayFirst",
  delimiter: "comma",
  decimalSeparator: "numericPeriod",
  encoding: "UTF-8",
  trimSpaces: false,
  hasHeader: false,
  ignoreHeader: false,
  commentCharacter: "",
});

export const getTableInstanceResponseTemplate = (): TableInstanceResponse => ({
  fieldspec: {
    tables: [],
    columns: [],
  },
  data: {
    id: "",
    dataSourceId: "",
    name: "",
    type: "RegularTable" as const,
    description: "",
    createdAt: "",
    updatedAt: "",
    columns: [],
    props: {},
  },
});

export const getFieldSpecTemplate = (key: number | string): FieldSpec => ({
  name: `fieldSpecTemplate ${key}`,
  description: `fieldSpecTemplate description ${key}`,
  filterable: true,
  displayName: `fieldSpecTemplate ${key} display name`,
  type: "text",
  kind: "custom",
  validValues: [`value ${key}-3`, `value ${key}-2`, `value ${key}-3`],
});

const getDataSourceSyncTaskTemplate = (key: number): DataSourceSyncTask => ({
  taskId: `taskId-${key}`,
  createdAt: "",
  startedAt: null,
  updatedAt: null,
  finishedAt: null,
  message: null,
  status: "finished",
});

const getDataSourceActionsTemplate = (): DataSourceActions => ({
  role: "admin",
  read: true,
  createTable: true,
  edit: true,
  editMetadata: true,
  delete: true,
  upload: true,
  grant: true,
  query: true,
  refresh: true,
});

export const getDataSourceResponseTemplate = (key: number): DataSourceResponse => ({
  id: `id-${key}`,
  projectId: "projectId",
  name: `name-${key}`,
  displayName: `displayName-${key}`,
  icon: "",
  type: "external",
  description: `description-${key}`,
  documentation: `documentation-${key}`,
  isPublic: true,
  createdAt: "",
  updatedAt: "",
  props: {},
  syncTask: getDataSourceSyncTaskTemplate(key),
  resourceActions: getDataSourceActionsTemplate(),
});

export const getPagedDataSourceResponseTemplate = (): PagedDataSourceResponse => ({
  page: {
    itemCount: 10,
    itemsPerPage: 5,
    unfilteredItemCount: 20,
    current: 1,
  },
  fieldspec: [getFieldSpecTemplate(0), getFieldSpecTemplate(1), getFieldSpecTemplate(2)],
  data: [
    getDataSourceResponseTemplate(0),
    getDataSourceResponseTemplate(1),
    getDataSourceResponseTemplate(2),
    getDataSourceResponseTemplate(3),
    getDataSourceResponseTemplate(4),
  ],
});

export const getColumnResponseTemplate = (key: number): ColumnResponse => ({
  name: `col-name-${key}`,
  description: `col-descr-${key}`,
  nullable: false,
  type: "varchar",
  props: { prop1: 1, prop2: 2, prop3: 3 },
});

export const getPropTemplate = (key: number): { [key: string]: any } => ({
  [`prop-key-${key}`]: key,
});

export const getTableResponseTemplate = (key: number): TableResponse => ({
  id: `table-${key}`,
  dataSourceId: "ds-id",
  name: `table-name-${key}`,
  type: "RegularTable",
  description: `description-${key}`,
  sql: "",
  createdAt: "",
  updatedAt: "",
  columns: [getColumnResponseTemplate(0), getColumnResponseTemplate(1), getColumnResponseTemplate(2)],
  props: [getPropTemplate(0), getPropTemplate(1), getPropTemplate(2)],
});

export const getTableFieldSpecTemplate = (key: number): TableFieldSpec => ({
  tables: [getFieldSpecTemplate(`${key}-0`), getFieldSpecTemplate(`${key}-1`), getFieldSpecTemplate(`${key}-2`)],
  columns: [getFieldSpecTemplate(`${key}-0`), getFieldSpecTemplate(`${key}-1`), getFieldSpecTemplate(`${key}-2`)],
});

export const getPagedTableResponseTemplate = (): PagedTableResponse => ({
  page: {
    itemCount: 10,
    itemsPerPage: 5,
    unfilteredItemCount: 20,
    current: 1,
  },
  fieldspec: getTableFieldSpecTemplate(0),
  data: [
    getTableResponseTemplate(0),
    getTableResponseTemplate(1),
    getTableResponseTemplate(2),
    getTableResponseTemplate(3),
    getTableResponseTemplate(4),
  ],
});
