export interface InputRestrictions {
  minLength: number;
  maxLength: number;
  pattern: string;
}
