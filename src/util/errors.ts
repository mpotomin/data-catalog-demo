import get from "lodash/get";
import { GetDataError } from "restful-react";

/**
 * Error format use by:
 *  - old version Pantheon
 *  - Labs
 *  - Idp
 *  - DataStore
 */
export interface LegacyErrorResponse {
  errors?: string[];
  fieldErrors?: {
    [fieldName: string]: string[];
  };
}

interface GeneralError {
  type: "GeneralError";
  message: string;
}

export interface FieldErrorResponse {
  errors: FieldError[];
}

export interface GeneralErrorResponse {
  errors: any;
}

interface ConcurrentModification {
  type: "ConcurrentModification";
  message: string;
}

interface ConstraintViolation {
  type: "ConstraintViolation";
  message: string;
}

interface FieldError {
  type: "FieldError";
  message: string;
  key: string;
}

const isFieldError = (error: Error): error is FieldError => error.type === "FieldError";

interface SchemaError {
  type: "SchemaError";
  message: string;
  startLineNumber: number;
  endLineNumber: number;
  startColumn: number;
  endColumn: number;
}

export type Error = GeneralError | FieldError | SchemaError | ConcurrentModification | ConstraintViolation;

/**
 * Error format use by:
 *   - Pantheon
 */
export interface ErrorResponse {
  errors: Error[];
}

export type ErrorType = LegacyErrorResponse | ErrorResponse | string;

export const isLegacyError = (error: any): error is LegacyErrorResponse => {
  return (
    (typeof error === "object" && Array.isArray(error.errors) && typeof error.errors[0] === "string") ||
    typeof error.fieldErrors === "object"
  );
};

export const isFieldErrorResponse = (
  e: GetDataError<GeneralErrorResponse | FieldErrorResponse> | null,
): e is GetDataError<FieldErrorResponse> => Boolean(get(e, "data.errors.0.key", false));

export const isErrorResponse = (error: any): error is ErrorResponse => {
  if (error === undefined || error === null) {
    return false;
  }

  return Array.isArray(error.errors);
};

/**
 * Retrieve the error data, contained in the error response.
 *
 * @param error API error response
 */
export const getErrorData = (error: any): ErrorResponse => {
  if (error === undefined || error === null || !isErrorResponse(error.data)) {
    return { errors: [] };
  }

  return error.data;
};

/**
 * Retrieve the corresponding field error.
 *
 * @param error API error response
 * @param key Name of the field
 */
export const getFieldError = (error: ErrorType | undefined, key: string) => {
  if (error === undefined || error === null || typeof error !== "object" || !error.errors) {
    return;
  } else if (isLegacyError(error)) {
    if (error.fieldErrors) {
      const fieldError = error.fieldErrors[key];
      if (!fieldError || !Array.isArray(fieldError)) {
        return undefined;
      }
      return error.fieldErrors[key].join(", ");
    } else {
      return undefined;
    }
  } else {
    const fieldError = error.errors.find(i => i.type === "FieldError" && i.key === key);

    return fieldError ? fieldError.message : undefined;
  }
};

/**
 * Retrieve field errors in the error response.
 *
 * @param error API error response
 */
export const getFieldErrors = (error: ErrorType) => {
  if (error === undefined || error === null || typeof error !== "object" || !error.errors) {
    return {};
  } else if (isLegacyError(error)) {
    if (error.fieldErrors) {
      return Object.entries(error.fieldErrors).reduce((mem, [key, value]) => ({ ...mem, [key]: value.join(", ") }), {});
    } else {
      return {};
    }
  } else {
    return error.errors.filter(isFieldError).reduce((mem, i) => ({ ...mem, [i.key]: i.message }), {});
  }
};

/**
 * Retrieve the general errors.
 *
 * @param error API error response
 */
export const getGeneralErrors = (error: ErrorType | undefined) => {
  try {
    if (typeof error !== "object") {
      return null;
    } else if (isLegacyError(error)) {
      return (error.errors || []).join(", ");
    } else {
      return error.errors
        .filter(i => i.type !== "FieldError")
        .map(i => i.message)
        .join(", ");
    }
  } catch {
    return null; // Just in case somebody is not respecting the standard error format.
  }
};
