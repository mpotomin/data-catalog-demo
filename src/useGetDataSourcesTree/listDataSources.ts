import qs from "qs";
import { getConfig } from "../../../Config";
import { customFetch } from "../../../util/customFetch";
import { errors } from "../../pantheon/executeQuery";
import { ListDataSourcesQueryParams, PagedDataSourceResponse } from "../queries/hub/hub";
import { HubErrorResponse } from "../queries/hub/utils";

export const listDataSources = (
  projectId: string,
  params?: ListDataSourcesQueryParams,
): Promise<PagedDataSourceResponse | HubErrorResponse> => {
  const search = params ? `?${qs.stringify(params)}` : "";
  const url = `${getConfig("hubBackend")}/api/v1/${projectId}/datasources${search}`;

  return customFetch(url).then(res => {
    if ((res.headers.get("content-type") || "").includes("application/json")) {
      return res.json();
    }

    return {
      errors: [
        {
          type: "GeneralError",
          message: errors[res.status] || errors[500],
        },
      ],
    };
  });
};
