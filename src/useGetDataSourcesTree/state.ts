import { Maybe } from "../types";
import { DataSourceResponse, FieldSpec, TableResponse } from "../queries/hub/hub";
import { getEmptyTableFieldSpec } from "../queries/hub/utils";

export interface DataSourceState {
  dataSource: DataSourceResponse;
  tables: typeof initialTablesState;
}

export type DataSourcesTreeState = typeof initialDataSourcesTreeState;

export const initialDataSourcesTreeState = Object.freeze({
  list: [] as DataSourceState[],
  fieldSpec: [] as FieldSpec[],
  currentPage: 0,
  isLoading: false,
  canLoadMore: true,
  error: undefined as Maybe<string>,
});

export const initialTablesState = Object.freeze({
  list: [] as TableResponse[],
  fieldSpec: getEmptyTableFieldSpec(),
  currentPage: 0,
  isLoading: false,
  canLoadMore: true,
  error: undefined as Maybe<string>,
});
