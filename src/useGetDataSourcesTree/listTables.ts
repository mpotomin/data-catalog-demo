import qs from "qs";
import { getConfig } from "../../../Config";
import { customFetch } from "../../../util/customFetch";
import { errors } from "../../pantheon/executeQuery";
import { ListTablesQueryParams, PagedTableResponse } from "../queries/hub/hub";
import { HubErrorResponse } from "../queries/hub/utils";

export const listTables = (
  projectId: string,
  dataSourceId: string,
  params?: ListTablesQueryParams,
): Promise<PagedTableResponse | HubErrorResponse> => {
  const search = params ? `?${qs.stringify(params)}` : "";
  const url = `${getConfig("hubBackend")}/api/v1/${projectId}/datasources/${dataSourceId}/tables${search}`;

  return customFetch(url).then(res => {
    if ((res.headers.get("content-type") || "").includes("application/json")) {
      return res.json();
    }

    return {
      errors: [
        {
          type: "GeneralError",
          message: errors[res.status] || errors[500],
        },
      ],
    };
  });
};
