import {
  getDataSourceResponseTemplate,
  getPagedDataSourceResponseTemplate,
  getPagedTableResponseTemplate,
  getTableResponseTemplate,
} from "../queries/hub/testUtils";

import { isDataSourceSynching } from "../queries/hub/utils";
import { reducer } from "./reducer";
import { DataSourcesTreeState, initialDataSourcesTreeState, initialTablesState } from "./state";

describe("useGetDataSourcesTree reducer", () => {
  describe("[Data sources] load start", () => {
    const initialState: DataSourcesTreeState = {
      ...initialDataSourcesTreeState,
      error: "Some error",
    };

    const newState = reducer(initialState, {
      type: "[Data sources] load start",
    });

    it("should enable the loading flag", () => {
      expect(newState.isLoading).toBeTruthy();
    });

    it("should disallow loading the next page", () => {
      expect(newState.canLoadMore).toBeFalsy();
    });

    it("should clear the error", () => {
      expect(initialState.error).not.toEqual(newState.error);
      expect(newState.error).toBeUndefined();
    });
  });

  describe("[Data sources] load error", () => {
    const initialState = {
      ...initialDataSourcesTreeState,
      isLoading: true,
      canLoadMore: false,
    };

    const newState = reducer(initialState, {
      type: "[Data sources] load error",
    });

    it("should disable the loading flag", () => {
      expect(initialState.isLoading).not.toEqual(newState.isLoading);
      expect(newState.isLoading).toBeFalsy();
    });

    it("should allow to retry loading the page", () => {
      expect(initialState.canLoadMore).not.toEqual(newState.canLoadMore);
      expect(newState.canLoadMore).toBeTruthy();
    });

    it("should set the error value", () => {
      expect(initialState.error).not.toEqual(newState.error);
      expect(newState.error).not.toBeUndefined();
    });
  });

  describe("[Data sources] load success", () => {
    const initialState: DataSourcesTreeState = {
      ...initialDataSourcesTreeState,
      list: [
        {
          dataSource: getDataSourceResponseTemplate(-1),
          tables: initialTablesState,
        },
      ],
      isLoading: true,
      canLoadMore: false,
    };

    const response = getPagedDataSourceResponseTemplate();

    const newState = reducer(initialState, {
      type: "[Data sources] load success",
      response,
    });

    it("should disable the loading flag", () => {
      expect(initialState.isLoading).not.toEqual(newState.isLoading);
      expect(newState.isLoading).toBeFalsy();
    });

    it("should append the list of data sources from the response", () => {
      expect(initialState.list).not.toEqual(newState.list);
      expect(newState.list).toEqual([
        ...initialState.list,
        ...response.data.map(dataSource => ({ dataSource, tables: initialTablesState })),
      ]);
    });

    it("should disallow loading tables for DSs that are in the sync state", () => {
      const response2 = getPagedDataSourceResponseTemplate();
      const indexDsInSync = 1;
      response2.data[indexDsInSync].syncTask.status = "running";

      const newState2 = reducer(initialState, {
        type: "[Data sources] load success",
        response: response2,
      });
      const foundItems = newState2.list.filter(item => item.tables.canLoadMore === false);
      expect(foundItems.length).toEqual(1);
      expect(isDataSourceSynching(foundItems[0].dataSource)).toBeTruthy();
    });

    it("should update the field spec with the value from the response", () => {
      expect(initialState.fieldSpec).not.toEqual(newState.fieldSpec);
      expect(newState.fieldSpec).toEqual(response.fieldspec);
    });

    it("should set canLoadMore to true if there are more items to load", () => {
      const response2 = getPagedDataSourceResponseTemplate();
      response2.page.itemCount = response2.data.length + initialState.list.length + 1;
      const newState2 = reducer(initialState, {
        type: "[Data sources] load success",
        response: response2,
      });
      expect(newState2.canLoadMore).toBeTruthy();
    });

    it("should set canLoadMore to false if reached the end of the list", () => {
      const response2 = getPagedDataSourceResponseTemplate();
      response2.page.itemCount = response2.data.length + initialState.list.length;
      const newState2 = reducer(initialState, {
        type: "[Data sources] load success",
        response: response2,
      });
      expect(newState2.canLoadMore).toBeFalsy();
    });

    it("should update the current page value", () => {
      expect(initialState.currentPage).not.toEqual(newState.currentPage);
      expect(newState.currentPage).toEqual(response.page.current);
    });
  });

  describe("[Tables] load start", () => {
    const initialState: DataSourcesTreeState = {
      ...initialDataSourcesTreeState,
      list: [
        { dataSource: getDataSourceResponseTemplate(0), tables: { ...initialTablesState, error: "error0" } },
        { dataSource: getDataSourceResponseTemplate(1), tables: { ...initialTablesState, error: "error1" } },
        { dataSource: getDataSourceResponseTemplate(2), tables: { ...initialTablesState, error: "error2" } },
        { dataSource: getDataSourceResponseTemplate(3), tables: { ...initialTablesState, error: "error3" } },
        { dataSource: getDataSourceResponseTemplate(4), tables: { ...initialTablesState, error: "error4" } },
      ],
    };

    it("should enable the loading flag for the target DS", () => {
      const newState = reducer(initialState, {
        type: "[Tables] load start",
        dataSourceId: "id-0",
      });
      expect(initialState.list[0].tables.isLoading).not.toEqual(newState.list[0].tables.isLoading);
      expect(newState.list[0].tables.isLoading).toBeTruthy();

      const newState2 = reducer(initialState, {
        type: "[Tables] load start",
        dataSourceId: "id-4",
      });
      expect(initialState.list[4].tables.isLoading).not.toEqual(newState2.list[4].tables.isLoading);
      expect(newState2.list[4].tables.isLoading).toBeTruthy();

      const newState3 = reducer(initialState, {
        type: "[Tables] load start",
        dataSourceId: "id-4",
      });
      expect(initialState.list[4].tables.isLoading).not.toEqual(newState3.list[4].tables.isLoading);
      expect(newState3.list[4].tables.isLoading).toBeTruthy();
    });

    it("should disable canLoadMore flag for the target DS", () => {
      const newState = reducer(initialState, {
        type: "[Tables] load start",
        dataSourceId: "id-3",
      });
      expect(initialState.list[3].tables.canLoadMore).not.toEqual(newState.list[3].tables.canLoadMore);
      expect(newState.list[3].tables.canLoadMore).toBeFalsy();
    });

    it("should clear the error for the target DS tables", () => {
      const newState = reducer(initialState, {
        type: "[Tables] load start",
        dataSourceId: "id-1",
      });
      expect(initialState.list[1].tables.error).not.toEqual(newState.list[1].tables.error);
      expect(newState.list[1].tables.error).toBeUndefined();
    });
  });

  describe("[Tables] load error", () => {
    const baseState: DataSourcesTreeState = {
      ...initialDataSourcesTreeState,
      list: [
        { dataSource: getDataSourceResponseTemplate(0), tables: { ...initialTablesState } },
        { dataSource: getDataSourceResponseTemplate(1), tables: { ...initialTablesState } },
        { dataSource: getDataSourceResponseTemplate(2), tables: { ...initialTablesState } },
        { dataSource: getDataSourceResponseTemplate(3), tables: { ...initialTablesState } },
        { dataSource: getDataSourceResponseTemplate(4), tables: { ...initialTablesState } },
      ],
    };

    it("should disable loading flag for the target DS tables", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-0" });
      const newState = reducer(initialState, {
        type: "[Tables] load error",
        dataSourceId: "id-0",
      });

      expect(initialState.list[0].tables.isLoading).not.toEqual(newState.list[0].tables.isLoading);
      expect(newState.list[0].tables.isLoading).toBeFalsy();
    });

    it("should enable canLoadMore flag for the target DS tables", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-4" });
      const newState = reducer(initialState, {
        type: "[Tables] load error",
        dataSourceId: "id-4",
      });

      expect(initialState.list[4].tables.canLoadMore).not.toEqual(newState.list[4].tables.canLoadMore);
      expect(newState.list[4].tables.canLoadMore).toBeTruthy();
    });

    it("should set the error message for the target DS tables", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-2" });
      const newState = reducer(initialState, {
        type: "[Tables] load error",
        dataSourceId: "id-2",
      });

      expect(initialState.list[2].tables.error).not.toEqual(newState.list[2].tables.error);
      expect(newState.list[2].tables.error).not.toBeUndefined();
    });
  });

  describe("[Tables] load success", () => {
    const baseState: DataSourcesTreeState = {
      ...initialDataSourcesTreeState,
      list: [
        { dataSource: getDataSourceResponseTemplate(0), tables: { ...initialTablesState } },
        { dataSource: getDataSourceResponseTemplate(1), tables: { ...initialTablesState } },
        { dataSource: getDataSourceResponseTemplate(2), tables: { ...initialTablesState } },
        { dataSource: getDataSourceResponseTemplate(3), tables: { ...initialTablesState } },
        {
          dataSource: getDataSourceResponseTemplate(4),
          tables: {
            ...initialTablesState,
            list: [getTableResponseTemplate(-3), getTableResponseTemplate(-2), getTableResponseTemplate(-1)],
          },
        },
      ],
    };

    it("should disable loading flag for the target DS tables", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-0" });
      const response = getPagedTableResponseTemplate();
      const newState = reducer(initialState, {
        type: "[Tables] load success",
        dataSourceId: "id-0",
        response,
      });

      expect(initialState.list[0].tables.isLoading).not.toEqual(newState.list[0].tables.isLoading);
      expect(newState.list[0].tables.isLoading).toBeFalsy();
    });

    it("should disable loading flag for the target DS tables", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-0" });
      const response = getPagedTableResponseTemplate();
      const newState = reducer(initialState, {
        type: "[Tables] load success",
        dataSourceId: "id-0",
        response,
      });

      expect(initialState.list[0].tables.isLoading).not.toEqual(newState.list[0].tables.isLoading);
      expect(newState.list[0].tables.isLoading).toBeFalsy();
    });

    it("should append the list of tables from the response", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-4" });
      const response = getPagedTableResponseTemplate();
      const newState = reducer(initialState, {
        type: "[Tables] load success",
        dataSourceId: "id-4",
        response,
      });

      expect(initialState.list[4].tables.list).not.toEqual(newState.list[4].tables.list);
      expect(newState.list[4].tables.list).toEqual([...initialState.list[4].tables.list, ...response.data]);
    });

    it("should update the field spec with the value from the response", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-2" });
      const response = getPagedTableResponseTemplate();
      const newState = reducer(initialState, {
        type: "[Tables] load success",
        dataSourceId: "id-2",
        response,
      });

      expect(initialState.list[2].tables.fieldSpec).not.toEqual(newState.list[2].tables.fieldSpec);
      expect(newState.list[2].tables.fieldSpec).toEqual(response.fieldspec);
    });

    it("should set canLoadMore to true if there are more tables to load", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-4" });
      const response = getPagedTableResponseTemplate();
      response.page.itemCount = 9;
      const newState = reducer(initialState, {
        type: "[Tables] load success",
        dataSourceId: "id-4",
        response,
      });

      expect(newState.list[4].tables.canLoadMore).toBeTruthy();
    });

    it("should set canLoadMore to false if reached the end of the tables list", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-4" });
      const response = getPagedTableResponseTemplate();
      response.page.itemCount = 8;
      const newState = reducer(initialState, {
        type: "[Tables] load success",
        dataSourceId: "id-4",
        response,
      });

      expect(newState.list[4].tables.canLoadMore).toBeFalsy();
    });

    it("should update the current page value", () => {
      const initialState = reducer(baseState, { type: "[Tables] load start", dataSourceId: "id-0" });
      const response = getPagedTableResponseTemplate();
      response.page.current = 21;
      const newState = reducer(initialState, {
        type: "[Tables] load success",
        dataSourceId: "id-0",
        response,
      });

      expect(initialState.list[0].tables.currentPage).not.toEqual(newState.list[0].tables.currentPage);
      expect(newState.list[0].tables.currentPage).toEqual(response.page.current);
    });
  });
});
