import { useCallback, useEffect, useReducer } from "react";
import { getConfig } from "../../../products/Pantheon/components/Workbench/Config";
import { DataSourceType } from "../queries/hub/hub";
import { isHubErrorResponse } from "../queries/hub/utils";
import { listDataSources } from "./listDataSources";
import { listTables } from "./listTables";
import { reducer } from "./reducer";
import { initialDataSourcesTreeState } from "./state";

export const useGetDataSourcesTree = (projectId: string, type: DataSourceType) => {
  const [state, dispatch] = useReducer(reducer, initialDataSourcesTreeState);

  const loadMoreDataSources = useCallback(() => {
    dispatch({ type: "[Data sources] load start" });
    listDataSources(projectId, {
      isQueryable: true,
      filter: `{"filters":[{"operation":"in","property":"type","value":["${type}"]}]}`,
      pageSize: getConfig("dataSourceTreePageSize"),
      page: state.currentPage + 1,
    })
      .then(response => {
        if (isHubErrorResponse(response)) {
          dispatch({ type: "[Data sources] load error" });
          return;
        }
        dispatch({ type: "[Data sources] load success", response });
      })
      .catch(() => dispatch({ type: "[Data sources] load error" }));
  }, [projectId, type, state.currentPage, dispatch]);

  const loadMoreTables = useCallback(
    (dataSourceId: string, page: number) => {
      dispatch({ type: "[Tables] load start", dataSourceId });
      listTables(projectId, dataSourceId, { page, pageSize: getConfig("dataSourceTableTreePageSize") })
        .then(response => {
          if (isHubErrorResponse(response)) {
            dispatch({ type: "[Tables] load error", dataSourceId });
            return;
          }
          dispatch({ type: "[Tables] load success", dataSourceId, response });
        })
        .catch(() => dispatch({ type: "[Tables] load error", dataSourceId }));
    },
    [projectId, dispatch],
  );

  useEffect(() => {
    loadMoreDataSources(); // Kickstart initial data fetching
  }, []);

  return {
    data: state,
    loadMoreDataSources: state.canLoadMore ? loadMoreDataSources : undefined,
    loadMoreTables,
  };
};
