import { PagedDataSourceResponse, PagedTableResponse } from "../queries/hub/hub";
import { isDataSourceSynching } from "../queries/hub/utils";
import { DataSourcesTreeState, initialTablesState } from "./state";

type Action =
  | { type: "[Data sources] load start" }
  | { type: "[Data sources] load error" }
  | { type: "[Data sources] load success"; response: PagedDataSourceResponse }
  | { type: "[Tables] load start"; dataSourceId: string }
  | { type: "[Tables] load error"; dataSourceId: string }
  | { type: "[Tables] load success"; dataSourceId: string; response: PagedTableResponse };

export const reducer = (state: DataSourcesTreeState, action: Action): DataSourcesTreeState => {
  switch (action.type) {
    case "[Data sources] load start": {
      return {
        ...state,
        isLoading: true,
        canLoadMore: false,
        error: undefined,
      };
    }

    case "[Data sources] load error": {
      return {
        ...state,
        isLoading: false,
        canLoadMore: true, // Give an opportunity to retry after a failure
        error: "Error",
      };
    }

    case "[Data sources] load success": {
      const list = [
        ...state.list,
        ...action.response.data.map(dataSource => ({
          dataSource,
          tables: {
            ...initialTablesState,
            canLoadMore: isDataSourceSynching(dataSource) ? false : initialTablesState.canLoadMore,
          },
        })),
      ];
      return {
        ...state,
        list,
        fieldSpec: action.response.fieldspec,
        isLoading: false,
        canLoadMore: list.length < action.response.page.itemCount,
        currentPage: action.response.page.current,
      };
    }

    case "[Tables] load start": {
      const targetDataSourceIndex = state.list.findIndex(ds => ds.dataSource.id === action.dataSourceId);
      if (targetDataSourceIndex < 0) {
        return state;
      }

      const targetDataSource = state.list[targetDataSourceIndex];
      return {
        ...state,
        list: [
          ...state.list.slice(0, targetDataSourceIndex),
          {
            ...targetDataSource,
            tables: { ...targetDataSource.tables, isLoading: true, canLoadMore: false, error: undefined },
          },
          ...state.list.slice(targetDataSourceIndex + 1),
        ],
      };
    }

    case "[Tables] load error": {
      const targetDataSourceIndex = state.list.findIndex(ds => ds.dataSource.id === action.dataSourceId);
      if (targetDataSourceIndex < 0) {
        return state;
      }

      const targetDataSource = state.list[targetDataSourceIndex];
      return {
        ...state,
        list: [
          ...state.list.slice(0, targetDataSourceIndex),
          {
            ...targetDataSource,
            tables: {
              ...targetDataSource.tables,
              error: "Error",
              isLoading: false,
              canLoadMore: true, // Give an opportunity to retry after a failure
            },
          },
          ...state.list.slice(targetDataSourceIndex + 1),
        ],
      };
    }

    case "[Tables] load success": {
      const targetDataSourceIndex = state.list.findIndex(ds => ds.dataSource.id === action.dataSourceId);
      if (targetDataSourceIndex < 0) {
        return state;
      }

      const targetDataSource = state.list[targetDataSourceIndex];
      const list = [...targetDataSource.tables.list, ...action.response.data];
      return {
        ...state,
        list: [
          ...state.list.slice(0, targetDataSourceIndex),
          {
            ...targetDataSource,
            tables: {
              ...targetDataSource.tables,
              list,
              fieldSpec: action.response.fieldspec,
              isLoading: false,
              canLoadMore: action.response.page.itemCount > list.length,
              currentPage: action.response.page.current,
            },
          },
          ...state.list.slice(targetDataSourceIndex + 1),
        ],
      };
    }
  }
};
