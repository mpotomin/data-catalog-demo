type Thunk = <Action, Props, State>(dispatch: React.Dispatch<Action>, props: Props, state: State) => void;

const isThunk = <Action>(input: Thunk | Action): input is Thunk => input instanceof Function;

const enhanceDispatch = <Action, Props, State>(dispatch: React.Dispatch<Action>, props: Props, state: State) => (
  input: ((dispatch: React.Dispatch<Action>, props: Props, state: State) => void) | Action,
) => {
  if (isThunk(input)) {
    input(dispatch, props, state);
    return;
  }

  dispatch(input as Action);
};

export default enhanceDispatch;
