import {
  getColumnMappingStatus,
  getPreviewCellErrorsCount,
  getSchemaColumnHeaderErrorsCount,
} from "../../queries/hub/utils";

import { Message } from "@operational/components";
import React, { useMemo } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { ColumnMapping, FilePreviewResponse } from "../../queries/hub/hub";
import { tableColumnNameRestriction } from "../../queries/hub/inputRestrictions";
import { ErrorResponse } from "../../util/errors";

interface Props {
  showInfo: boolean;
  error: ErrorResponse | null;
  showSchemaColumnHeaderErrors?: boolean;
  preview?: FilePreviewResponse["preview"];
  columnMapping?: ColumnMapping;
}

const TableInfo = injectIntl(
  ({ showInfo, preview, columnMapping, error, showSchemaColumnHeaderErrors, intl }: Props & InjectedIntlProps) => {
    const schemaColumnErrorCount = useMemo(
      () => (showSchemaColumnHeaderErrors ? getSchemaColumnHeaderErrorsCount(error) : 0),
      [error, showSchemaColumnHeaderErrors],
    );
    const schemaColumnErrorMessage = useMemo(
      () =>
        intl.formatMessage(
          {
            id: "dataStore.tables.createWizard.columnHeaderErrors",
            defaultMessage: "Column headers with errors: {errorCount}",
          },
          { errorCount: schemaColumnErrorCount },
        ),
      [schemaColumnErrorCount],
    );

    const hoverSuggestion = useMemo(
      () =>
        intl.formatMessage({
          id: "dataStore.tables.createWizard.hoverSuggestion",
          defaultMessage: "Hover over the cell to see the error message.",
        }),
      [],
    );

    const columnMappingStatus = useMemo(() => (columnMapping ? getColumnMappingStatus(columnMapping) : undefined), [
      columnMapping,
    ]);
    const columnMappingErrorMessage = useMemo(() => {
      if (columnMappingStatus === "all-columns-dropped") {
        return intl.formatMessage({
          id: "dataStore.table.uploadDataWizard.step2.mappingError.allDrop",
          defaultMessage: "All source columns are set to be dropped",
        });
      }
      if (columnMappingStatus === "non-unique-target-columns") {
        return intl.formatMessage({
          id: "dataStore.table.uploadDataWizard.step2.mappingError.duplicateTarget",
          defaultMessage: "Duplicate target columns are not permitted",
        });
      }
      return "";
    }, [columnMappingStatus]);

    const cellErrorsCount = useMemo(() => (preview ? getPreviewCellErrorsCount(preview, columnMapping) : 0), [
      preview,
      columnMapping,
    ]);
    const cellsErrorMessage = useMemo(
      () =>
        intl.formatMessage(
          {
            id: "dataStore.tables.createWizard.cellWarnings",
            defaultMessage: "Table cells with warnings: {warningCount}",
          },
          { warningCount: cellErrorsCount },
        ),
      [cellErrorsCount],
    );

    const columnNameRestrictionMessage = useMemo(
      () =>
        intl.formatMessage(
          {
            id: "restriction.tableColumn",
            defaultMessage:
              "Column header must be between {minLength} and {maxLength} characters long. It must start with an alphabetic character and may only include alphanumeric characters and underscores '_'.",
          },
          { minLength: tableColumnNameRestriction.minLength, maxLength: tableColumnNameRestriction.maxLength },
        ),
      [],
    );

    return (
      <>
        {showInfo && schemaColumnErrorCount < 1 && <Message type="info" body={columnNameRestrictionMessage} />}
        {columnMappingStatus !== undefined && columnMappingStatus !== "valid" && (
          <Message type="error" body={columnMappingErrorMessage} />
        )}
        {(columnMappingStatus === undefined || columnMappingStatus === "valid") && schemaColumnErrorCount > 0 && (
          <Message
            type="error"
            title={schemaColumnErrorMessage}
            body={`${columnNameRestrictionMessage} ${hoverSuggestion}`}
          />
        )}
        {cellErrorsCount > 0 && <Message type="warning" title={cellsErrorMessage} body={hoverSuggestion} />}
      </>
    );
  },
);

export default TableInfo;
