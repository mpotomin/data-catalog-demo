export const dataTableCellHeight = 35;
const tableHeaderRowCount = 2;
const minHeightWithColumnTypeSelect = 380;

export const getTableHeight = (rows: number) => {
  const calculatedTableHeight = (rows + tableHeaderRowCount) * dataTableCellHeight;
  const maxTableHeight = Math.min(calculatedTableHeight, 600);
  return Math.max(minHeightWithColumnTypeSelect, maxTableHeight);
};

export const getTableBody = (rows: number, cellAmount: number) =>
  Array.from({ length: rows }, () => Array.from({ length: cellAmount }, () => ""));
