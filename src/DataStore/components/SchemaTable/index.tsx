import { DataTable, styled } from "@operational/components";
import React, { useCallback } from "react";
import { ManagedTableSchema } from "../../../queries/hub/hub";
import { getTableBody, getTableHeight } from "./tableBodyUtils";

interface SchemaTableProps {
  schema: ManagedTableSchema;
  children?: never;
}

const DataTableWrapper = styled("div")`
  overflow: auto;
`;

const SchemaTable = ({ schema }: SchemaTableProps) => {
  const getTableRows = useCallback((data: ManagedTableSchema) => {
    const { columns } = data;
    const { length } = columns;

    return {
      columns: columns.map(({ name, type }) => [name, type]),
      rows: getTableBody(2, length),
    };
  }, []);

  return (
    <DataTableWrapper>
      <DataTable height={getTableHeight(3)} columns={getTableRows(schema).columns} rows={getTableRows(schema).rows} />
    </DataTableWrapper>
  );
};

export default SchemaTable;
