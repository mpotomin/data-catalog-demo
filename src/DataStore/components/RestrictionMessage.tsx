import { FinePrint, InfoIcon, styled } from "@operational/components";
import React from "react";

interface Props {
  message: string;
  type: "info" | "error";
}

const Notification = styled(FinePrint)<{ type: "info" | "error" }>`
  width: 360px;
  text-align: justify;
  margin: ${({ theme }) => theme.space.small}px 0;
  display: grid;
  grid-template-columns: ${({ theme }) => theme.space.medium}px auto;
  grid-column-gap: ${({ theme }) => theme.space.base}px;
  color: ${({ type, theme }) => (type === "error" ? theme.color.error : theme.color.border)};
`;

const IconCell = styled("div")`
  padding-top: 2px;
`;

const RestrictionMessage = ({ message, type, ...rest }: Props) => (
  <Notification type={type} {...rest}>
    <IconCell>
      <InfoIcon size={12} color={type === "error" ? "error" : "info"} />
    </IconCell>
    {message}
  </Notification>
);

export default RestrictionMessage;
