import { PlusIcon, styled } from "@operational/components";
import { inputFocus } from "@operational/components/lib/utils";
import React from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";

interface AddDescriptionButtonProps {
  disabled: boolean;
  onClick: () => void;
}

const Container = styled.button`
  display: flex;
  height: fit-content;
  width: fit-content;
  align-items: center;
  justify-content: center;
  border: 0;
  background-color: transparent;
  font: inherit;
  color: ${({ theme }) => theme.color.primary};
  padding: ${({ theme }) => theme.space.medium}px;
  font-weight: ${({ theme }) => theme.font.weight.bold};
  cursor: pointer;

  &:hover {
    ${({ theme }) => inputFocus({ theme })};
  }

  &:focus {
    ${({ theme }) => inputFocus({ theme })};
  }
`;

const AddDescriptionButton = injectIntl(({ intl, ...rest }: AddDescriptionButtonProps & InjectedIntlProps) => (
  <Container type="button" {...rest}>
    {intl.formatMessage({
      id: "dataStore.addDescription",
      defaultMessage: "Add description",
    })}
    {<PlusIcon size={10} right color="primary" />}
  </Container>
));

export default AddDescriptionButton;
