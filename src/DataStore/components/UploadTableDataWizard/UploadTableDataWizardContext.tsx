import {
  FileDiscoveryRequest,
  FileDiscoveryResponse,
  FileIngestRequest,
  FilePreviewRequest,
  FilePreviewResponse,
  TaskResponse,
} from "../../../queries/hub/hub";

import { IMessage } from "@operational/components/lib/OperationalContext/OperationalContext";
import React from "react";
import { MutateMethod } from "restful-react";
import { Maybe } from "../../../types";
import { CreateTableWizardProps } from "../CreateTableWizard";
import { UploadTableDataWizardState } from "./state";
import { EnhancedDispatch } from "./state/enhanceDispatch";

const initial = Object.freeze({
  projectId: "",
  dataSourceId: "",
  tableId: "",
  state: undefined as Maybe<UploadTableDataWizardState>,
  enhancedDispatch: undefined as Maybe<EnhancedDispatch>,
  closeWizard: undefined as Maybe<() => void>,
  discoverFile: undefined as Maybe<MutateMethod<FileDiscoveryResponse, FileDiscoveryRequest>>,
  previewFile: undefined as Maybe<MutateMethod<FilePreviewResponse, FilePreviewRequest>>,
  createIngestionTask: undefined as Maybe<MutateMethod<TaskResponse, FileIngestRequest>>,
  showIngestionTask: undefined as Maybe<CreateTableWizardProps["showIngestionTask"]>,
  pushMessage: undefined as Maybe<(message: IMessage) => void>,
});

const UploadTableDataWizardContext = React.createContext(initial);

export default UploadTableDataWizardContext;
