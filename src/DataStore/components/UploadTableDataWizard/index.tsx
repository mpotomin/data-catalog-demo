import {
  CreateIngestionTask,
  DiscoverFileInManagedDataSource,
  PreviewFileInManagedDataSource,
  TaskResponse,
} from "../../../queries/hub/hub";

import { OperationalContext } from "@operational/components";
import React, { useEffect, useReducer, useState } from "react";
import { initialState } from "./state";
import enhanceUploadTableDataWizardDispatch from "./state/enhanceDispatch";
import reducer from "./state/reducer";
import UploadTableDataWizardConnected from "./UploadTableDataWizardConnected";
import UploadTableDataWizardContext from "./UploadTableDataWizardContext";

export interface UploadTableDataWizardProps {
  projectId: string;
  dataSourceId: string;
  tableId: string;
  closeWizard: () => void;
  showIngestionTask: (task: TaskResponse) => void;
}

export interface UploadTableDataAction {
  type: "upload-table-data";
  dataSourceId: string;
  dataSourceName: string;
  tableId: string;
}

const UploadTableDataWizard = (props: UploadTableDataWizardProps) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const enhancedDispatch = enhanceUploadTableDataWizardDispatch(dispatch, props, state);
  const { projectId, dataSourceId, tableId, closeWizard, showIngestionTask } = props;

  const [fileId, setFileId] = useState("no-fileId-defined");
  useEffect(() => {
    if (state.upload.uploadResponse) {
      setFileId(state.upload.uploadResponse.fileId);
    }
  }, [state.upload.uploadResponse]);

  return (
    <OperationalContext>
      {({ pushMessage }) => (
        <DiscoverFileInManagedDataSource projectId={projectId} dataSourceId={dataSourceId} fileId={fileId}>
          {discoverFile => (
            <PreviewFileInManagedDataSource
              projectId={projectId}
              dataSourceId={dataSourceId}
              fileId={fileId}
              localErrorOnly
            >
              {previewFile => (
                <CreateIngestionTask projectId={projectId} dataSourceId={dataSourceId}>
                  {createIngestionTask => (
                    <UploadTableDataWizardContext.Provider
                      value={{
                        projectId,
                        dataSourceId,
                        tableId,
                        state,
                        enhancedDispatch,
                        closeWizard,
                        discoverFile,
                        previewFile,
                        createIngestionTask,
                        showIngestionTask,
                        pushMessage,
                      }}
                    >
                      <UploadTableDataWizardConnected />
                    </UploadTableDataWizardContext.Provider>
                  )}
                </CreateIngestionTask>
              )}
            </PreviewFileInManagedDataSource>
          )}
        </DiscoverFileInManagedDataSource>
      )}
    </OperationalContext>
  );
};

export default UploadTableDataWizard;
