import {
  createBestGuessColumnMapping,
  getTargetColumnOptions,
} from "../../DiscoverDataStructure/MapSchemaView/columnMappingUtils";

import {
  getAlternativeSchemaTemplate,
  getFileOptionsTemplate,
  getSchemaTemplate,
  getTestErrorTemplate,
} from "../../../../queries/hub/testUtils";

import { initialState, UploadTableDataWizardState } from ".";
import { UploadResponse } from "../../../../queries/hub/hub";
import reducer from "./reducer";

describe("UploadTableDataWizard reducer", () => {
  describe("[data store uploader] file-upload-success", () => {
    const state: UploadTableDataWizardState = {
      ...initialState,
    };

    const uploadResponse: UploadResponse = {
      uploadedBytes: 1234,
      fileId: "string",
      collectionId: "string",
      schema: getSchemaTemplate(),
      targetSchema: getAlternativeSchemaTemplate(),
      options: getFileOptionsTemplate(),
      schemaErrors: getTestErrorTemplate(),
      preview: [],
    };

    const newState = reducer(state, { type: "[data store uploader] file-upload-success", uploadResponse });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should advance the flow step", () => {
      expect(newState.flowStep).toEqual("confirm-data-structure");
    });

    it("should store the upload response in the discovery slice of the state", () => {
      expect(newState.discovery.fileDiscoveryResponse).not.toEqual(state.discovery.fileDiscoveryResponse);
      expect(newState.discovery.fileDiscoveryResponse).toEqual(uploadResponse);
    });

    it("should set the preview property from the action payload", () => {
      expect(newState.discovery.preview).not.toEqual(state.discovery.preview);
      expect(newState.discovery.preview).toEqual(uploadResponse.preview);
    });

    it("should set the editable schema from the action payload", () => {
      expect(newState.discovery.editableSchema).toEqual(uploadResponse.schema);
    });

    it("should set the editable file options from the action payload", () => {
      expect(newState.discovery.editableFileOptions).toEqual(uploadResponse.options);
    });

    it("should set the target schema from the action payload", () => {
      expect(newState.discovery.targetSchema).not.toEqual(state.discovery.targetSchema);
      expect(newState.discovery.targetSchema).toEqual(uploadResponse.targetSchema);
    });

    it("should calculate target column options", () => {
      expect(newState.discovery.targetColumnOptions).not.toEqual(state.discovery.targetColumnOptions);
      expect(newState.discovery.targetColumnOptions).not.toEqual(getTargetColumnOptions(uploadResponse.schema));
      expect(newState.discovery.targetColumnOptions).toEqual(getTargetColumnOptions(uploadResponse.targetSchema));
    });

    it("should calculate initial column mapping", () => {
      expect(newState.discovery.columnMapping).not.toEqual(state.discovery.columnMapping);
      expect(newState.discovery.columnMapping).not.toEqual(
        createBestGuessColumnMapping(uploadResponse.targetSchema, uploadResponse.schema),
      );
      expect(newState.discovery.columnMapping).toEqual(
        createBestGuessColumnMapping(uploadResponse.schema, uploadResponse.targetSchema),
      );
    });

    it("should set the field errors of the discovery state slice from the action payload", () => {
      expect(newState.discovery.error).toEqual(uploadResponse.schemaErrors);
    });
  });

  describe("[upload table data wizard] ingestion-request-started", () => {
    const state: UploadTableDataWizardState = {
      ...initialState,
      isTaskCreationRunning: false,
    };

    const newState = reducer(state, { type: "[upload table data wizard] ingestion-request-started" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should enable task creation flag", () => {
      expect(newState.isTaskCreationRunning).toBeTruthy();
    });
  });

  describe("[upload table data wizard] ingestion-request-error", () => {
    const state: UploadTableDataWizardState = {
      ...initialState,
      isTaskCreationRunning: true,
    };

    const newState = reducer(state, { type: "[upload table data wizard] ingestion-request-error" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should disable task creation flag", () => {
      expect(newState.isTaskCreationRunning).toBeFalsy();
    });
  });
});
