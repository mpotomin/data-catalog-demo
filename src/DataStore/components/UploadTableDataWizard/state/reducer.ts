import {
  createBestGuessColumnMapping,
  getEmptyTableSchema,
  getTargetColumnOptions,
} from "../../DiscoverDataStructure/MapSchemaView/columnMappingUtils";

import { UploadTableDataWizardState } from "../state";
import { DataStoreUploaderAction, isDataStoreUploaderAction } from "../../DataStoreUploader/state/actions";
import dataStoreUploaderReducer from "../../DataStoreUploader/state/reducer";
import { DiscoverDataStructureAction, isDiscoverDataStructureAction } from "../../DiscoverDataStructure/state/actions";
import discoverDataStructureReducer from "../../DiscoverDataStructure/state/reducer";
import { UploadTableDataWizardAction } from "./actions";

function rootReducer(
  state: UploadTableDataWizardState,
  action: UploadTableDataWizardAction | DataStoreUploaderAction,
): UploadTableDataWizardState {
  switch (action.type) {
    case "[data store uploader] file-upload-success":
      return {
        ...state,
        flowStep: "confirm-data-structure",
        discovery: {
          ...state.discovery,
          fileDiscoveryResponse: action.uploadResponse,
          preview: action.uploadResponse.preview,
          editableSchema: { ...action.uploadResponse.schema },
          editableFileOptions: { ...action.uploadResponse.options },
          targetSchema: action.uploadResponse.targetSchema,
          targetColumnOptions: getTargetColumnOptions(
            action.uploadResponse.targetSchema ? action.uploadResponse.targetSchema : getEmptyTableSchema(),
          ),
          columnMapping: createBestGuessColumnMapping(
            action.uploadResponse.schema,
            action.uploadResponse.targetSchema ? action.uploadResponse.targetSchema : getEmptyTableSchema(),
          ),
          error: action.uploadResponse.schemaErrors,
        },
      };
    case "[upload table data wizard] ingestion-request-started":
      return { ...state, isTaskCreationRunning: true };
    case "[upload table data wizard] ingestion-request-error":
      return { ...state, isTaskCreationRunning: false };
    default:
      // Passing through the DiscoverDataStructureAction and DataStoreUploaderAction types
      return state;
  }
}

export default function reducer(
  state: UploadTableDataWizardState,
  action: UploadTableDataWizardAction | DiscoverDataStructureAction,
): UploadTableDataWizardState {
  const rootReducerResult = rootReducer(state, action);
  return {
    ...rootReducerResult,
    upload: isDataStoreUploaderAction(action)
      ? dataStoreUploaderReducer(rootReducerResult.upload, action)
      : rootReducerResult.upload,
    discovery: isDiscoverDataStructureAction(action)
      ? discoverDataStructureReducer(rootReducerResult.discovery, action)
      : rootReducerResult.discovery,
  };
}
