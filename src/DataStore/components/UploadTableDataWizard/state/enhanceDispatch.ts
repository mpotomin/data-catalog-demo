import { UploadTableDataWizardState } from "../state";
import { UploadTableDataWizardProps } from "../../UploadTableDataWizard";
import enhanceDispatch from "../../../state/enhanceDispatch";
import { DataStoreUploaderAction } from "../../DataStoreUploader/state/actions";
import { UploadTableDataWizardAction } from "./actions";

export type EnhancedDispatch = ReturnType<typeof enhanceUploadTableDataWizardDispatch>;

const enhanceUploadTableDataWizardDispatch = (
  dispatch: React.Dispatch<UploadTableDataWizardAction | UploadTableDataWizardAction | DataStoreUploaderAction>,
  props: UploadTableDataWizardProps,
  state: UploadTableDataWizardState,
) => enhanceDispatch(dispatch, props, state);

export default enhanceUploadTableDataWizardDispatch;
