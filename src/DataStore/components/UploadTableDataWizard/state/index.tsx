import { initialFileUploaderState } from "../../DataStoreUploader/state";
import { initialState as initialDiscoverState } from "../../DiscoverDataStructure/state";

export type UploadTableDataWizardState = typeof initialState;

export type FlowStep = "upload-file" | "confirm-data-structure";

export const initialState = Object.freeze({
  flowStep: "upload-file" as FlowStep,
  upload: initialFileUploaderState,
  discovery: initialDiscoverState,
  isTaskCreationRunning: false,
});
