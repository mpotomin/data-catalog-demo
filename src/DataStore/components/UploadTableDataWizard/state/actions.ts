import { DataStoreUploaderAction } from "../../DataStoreUploader/state/actions";
import { DiscoverDataStructureAction } from "../../DiscoverDataStructure/state/actions";

export type UploadTableDataWizardAction =
  | DiscoverDataStructureAction
  | DataStoreUploaderAction
  | { type: "[upload table data wizard] ingestion-request-started" }
  | { type: "[upload table data wizard] ingestion-request-error" };
