import { IMessage } from "@operational/components/lib/OperationalContext/OperationalContext";
import { InjectedIntl } from "react-intl";
import { MutateMethod } from "restful-react";
import { UploadTableDataWizardState } from "../state";
import { UploadTableDataWizardProps } from "../../UploadTableDataWizard";
import { FileIngestRequest, TaskResponse } from "../../../../queries/hub/hub";
import { dropColumnOptionValue } from "../../DiscoverDataStructure/MapSchemaView/columnMappingUtils";
import { UploadTableDataWizardAction } from "./actions";

export const ingestFile = (
  createIngestionTask: MutateMethod<TaskResponse, FileIngestRequest>,
  pushMessage: (message: IMessage) => void,
  showIngestionTask: UploadTableDataWizardProps["showIngestionTask"],
  successCallback: () => void,
  intl: InjectedIntl,
) => (
  dispatch: React.Dispatch<UploadTableDataWizardAction>,
  props: UploadTableDataWizardProps,
  state: UploadTableDataWizardState,
) => {
  const { tableId } = props;
  const { upload } = state;
  const { uploadResponse } = upload;
  const { discovery } = state;
  const { editableSchema, editableFileOptions, columnMapping } = discovery;

  const filteredColumnMapping = columnMapping
    ? columnMapping.filter(item => item.target !== dropColumnOptionValue)
    : [];

  if (uploadResponse && editableSchema && editableFileOptions) {
    dispatch({ type: "[upload table data wizard] ingestion-request-started" });
    createIngestionTask({
      tableId,
      columnMapping: filteredColumnMapping,
      fileId: uploadResponse.fileId,
      schema: editableSchema,
      options: editableFileOptions,
      insertMode: "upsert",
    })
      .then(ingestionTaskResponse => {
        const body = intl.formatMessage(
          {
            id: "dataStore.tasks.ingestionTaskCreated",
            defaultMessage: "Ingestion of {fileName} enqueued",
          },
          { fileName: ingestionTaskResponse.fileName },
        );
        pushMessage({
          body,
          type: "success",
          onClick: () => showIngestionTask(ingestionTaskResponse),
        });
        successCallback();
      })
      .catch(() => {
        dispatch({ type: "[upload table data wizard] ingestion-request-error" });
      });
  }
};
