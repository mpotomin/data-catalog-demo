import React, { useCallback, useContext, useMemo } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { getColumnMappingStatus } from "../../../queries/hub/utils";
import DiscoverDataStructureContext from "../DiscoverDataStructure/DiscoverDataStructureContext";
import { DiscoverDataStructureAction } from "../DiscoverDataStructure/state/actions";
import { discoverFileThunk, previewFileThunk } from "../DiscoverDataStructure/state/thunks";
import { ingestFile } from "./state/thunks";
import UploadTableDataWizardContext from "./UploadTableDataWizardContext";
import UploadTableDataWizardPresentational from "./UploadTableDataWizardPresentational";

const UploadTableDataWizardConnected = injectIntl(({ intl }: InjectedIntlProps) => {
  const {
    state,
    enhancedDispatch,
    closeWizard,
    discoverFile,
    previewFile,
    createIngestionTask,
    pushMessage,
    showIngestionTask,
  } = useContext(UploadTableDataWizardContext);

  if (!state) {
    return null;
  }

  const { flowStep, discovery, isTaskCreationRunning } = state;
  const { isFileDiscoveryRunning, columnMapping } = discovery;

  const discoverDispatch = useCallback(
    (action: DiscoverDataStructureAction) => {
      if (enhancedDispatch && discoverFile && pushMessage) {
        enhancedDispatch(discoverFileThunk(action, discovery, discoverFile, pushMessage));
      }
    },
    [enhancedDispatch, discovery, discoverFile, pushMessage],
  );

  const previewDispatch = useCallback(
    (action: DiscoverDataStructureAction) => {
      if (enhancedDispatch && previewFile && pushMessage) {
        enhancedDispatch(previewFileThunk(action, discovery, previewFile, pushMessage));
      }
    },
    [enhancedDispatch, discovery, discoverFile, pushMessage],
  );

  const createIngestionTaskHandler = useCallback(() => {
    if (enhancedDispatch && createIngestionTask && columnMapping && pushMessage && showIngestionTask && closeWizard) {
      enhancedDispatch(ingestFile(createIngestionTask, pushMessage, showIngestionTask, closeWizard, intl));
    }
  }, [enhancedDispatch, createIngestionTask, pushMessage, closeWizard]);

  const isColumnMappingValid = useMemo(
    () => (columnMapping ? getColumnMappingStatus(columnMapping) === "valid" : false),
    [columnMapping],
  );

  return (
    <DiscoverDataStructureContext.Provider
      value={{
        isDisabled: isFileDiscoveryRunning || isTaskCreationRunning,
        state: discovery,
        discoverDispatch,
        previewDispatch,
      }}
    >
      <UploadTableDataWizardPresentational
        flowStep={flowStep}
        isColumnMappingValid={isColumnMappingValid}
        isDiscoveryRunning={isFileDiscoveryRunning}
        isTaskCreationRunning={isTaskCreationRunning}
        createIngestionTask={createIngestionTaskHandler}
        closeWizard={closeWizard}
      />
    </DiscoverDataStructureContext.Provider>
  );
});

export default UploadTableDataWizardConnected;
