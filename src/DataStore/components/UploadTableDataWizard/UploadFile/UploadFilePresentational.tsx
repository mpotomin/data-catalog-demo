import { CardColumn, CardItem } from "@operational/components";
import React from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import DataStoreUploader from "../../DataStoreUploader";
import { FileUploadState } from "../../DataStoreUploader/state";

interface UploadFilePresentationalProps {
  fileName: string;
  uploadState: FileUploadState;
  uploadProgress: number;
  uploadFile: (file: File) => void;
  abortUpload: () => void;
  restartUpload: () => void;
}

const UploadFilePresentational = injectIntl(
  ({
    fileName,
    uploadState,
    uploadProgress,
    uploadFile,
    abortUpload,
    restartUpload,
    intl,
  }: UploadFilePresentationalProps & InjectedIntlProps) => {
    return (
      <CardColumn
        title={intl.formatMessage({
          id: "dataStore.table.uploadDataWizard.step1.subhead",
          defaultMessage: "Select file to upload",
        })}
      >
        <CardItem
          title={intl.formatMessage({
            id: "dataStore.table.uploadDataWizard.step1.upload.heading",
            defaultMessage: "Upload a file you want to import",
          })}
          value={
            <DataStoreUploader
              fileName={fileName}
              uploadState={uploadState}
              uploadProgress={uploadProgress}
              uploadFile={uploadFile}
              abort={abortUpload}
              restart={restartUpload}
            />
          }
        />
      </CardColumn>
    );
  },
);

export default UploadFilePresentational;
