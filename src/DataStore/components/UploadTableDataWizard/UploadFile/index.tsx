import React, { useCallback, useContext } from "react";
import { uploadFile } from "../../DataStoreUploader/state/thunks";
import UploadTableDataWizardContext from "../UploadTableDataWizardContext";
import UploadFilePresentational from "./UploadFilePresentational";

const UploadFile = () => {
  const { projectId, dataSourceId, tableId, state, enhancedDispatch, pushMessage } = useContext(
    UploadTableDataWizardContext,
  );

  const abortUpload = useCallback(() => {
    if (state && state.upload && state.upload.request) {
      state.upload.request.abort();
    }
  }, [state]);

  const restartUploadHandler = useCallback(() => {
    if (enhancedDispatch) {
      enhancedDispatch({
        type: "[data store uploader] file-upload-restart",
      });
    }
  }, [enhancedDispatch]);

  const uploadFileHandler = useCallback(
    (file: File) => {
      if (enhancedDispatch && pushMessage) {
        enhancedDispatch(uploadFile(file, projectId, dataSourceId, pushMessage, tableId));
      }
    },
    [projectId, dataSourceId, enhancedDispatch],
  );

  return (
    <UploadFilePresentational
      fileName={state ? state.upload.nameOfFile : ""}
      uploadState={state ? state.upload.uploadState : "initial"}
      uploadProgress={state ? state.upload.uploadProgress : 0}
      uploadFile={uploadFileHandler}
      abortUpload={abortUpload}
      restartUpload={restartUploadHandler}
    />
  );
};

export default UploadFile;
