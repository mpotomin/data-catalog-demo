import { Button, Modal, Stepper, styled } from "@operational/components";
import React, { useCallback, useMemo } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { Maybe } from "../../../types";
import DiscoverDataStructure from "../DiscoverDataStructure";
import { FlowStep } from "./state";
import UploadFile from "./UploadFile";

interface UploadTableDataWizardPresentationalProps {
  flowStep: FlowStep;
  isColumnMappingValid: boolean;
  isDiscoveryRunning: boolean;
  isTaskCreationRunning: boolean;
  createIngestionTask: () => void;
  closeWizard: Maybe<() => void>;
}

const getIndex = (flowStep: FlowStep) => {
  switch (flowStep) {
    case "upload-file":
      return 0;
    case "confirm-data-structure":
      return 1;
  }
};

const ModalContent = styled.div`
  min-width: 800px;
  min-height: 400px;
  padding: 2px; /* padding so the focus border of the controls doesn't get cut off */
`;

const StepperContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const Steps = styled(Stepper)`
  width: 450px;
`;

const UploadTableDataWizardPresentational = injectIntl(
  ({
    flowStep,
    isColumnMappingValid,
    isDiscoveryRunning,
    isTaskCreationRunning,
    createIngestionTask,
    closeWizard,
    intl,
  }: UploadTableDataWizardPresentationalProps & InjectedIntlProps) => {
    const step1 = intl.formatMessage({
      id: "dataStore.table.uploadDataWizard.step1.heading",
      defaultMessage: "Select file",
    });
    const step2 = intl.formatMessage({
      id: "dataStore.table.uploadDataWizard.step2.heading",
      defaultMessage: "Confirm data",
    });

    const closeWizardHandler = useCallback(() => {
      if (closeWizard) {
        closeWizard();
      }
    }, [closeWizard]);

    const actions = useMemo(() => {
      switch (flowStep) {
        case "upload-file":
          return [
            <Button condensed onClick={closeWizardHandler}>
              {intl.formatMessage({
                id: "dataStore.table.uploadDataWizard.actions.cancel",
                defaultMessage: "Cancel",
              })}
            </Button>,
          ];
        case "confirm-data-structure":
          return [
            <Button
              condensed
              loading={isDiscoveryRunning || isTaskCreationRunning}
              disabled={!isColumnMappingValid || isDiscoveryRunning || isTaskCreationRunning}
              color="primary"
              onClick={createIngestionTask}
            >
              {intl.formatMessage({
                id: "dataStore.table.uploadDataWizard.actions.upload",
                defaultMessage: "Upload",
              })}
            </Button>,
            <Button condensed disabled={isDiscoveryRunning || isTaskCreationRunning} onClick={closeWizardHandler}>
              {intl.formatMessage({
                id: "dataStore.table.uploadDataWizard.actions.cancel",
                defaultMessage: "Cancel",
              })}
            </Button>,
          ];
      }
    }, [
      flowStep,
      closeWizardHandler,
      isDiscoveryRunning,
      isTaskCreationRunning,
      isColumnMappingValid,
      createIngestionTask,
    ]);

    return (
      <Modal
        title={intl.formatMessage({ id: "dataStore.table.uploadDataWizard.heading", defaultMessage: "Upload Data" })}
        width="max-content"
        height="auto"
        isOpen
        actions={actions}
      >
        <ModalContent>
          <StepperContainer>
            <Steps
              steps={[
                {
                  title: step1,
                  content: null,
                },
                {
                  title: step2,
                  content: null,
                },
              ]}
              activeSlideIndex={getIndex(flowStep)}
            />
          </StepperContainer>
          {flowStep === "upload-file" && <UploadFile />}
          {flowStep === "confirm-data-structure" && <DiscoverDataStructure mode="map-to-existing-target-schema" />}
        </ModalContent>
      </Modal>
    );
  },
);

export default UploadTableDataWizardPresentational;
