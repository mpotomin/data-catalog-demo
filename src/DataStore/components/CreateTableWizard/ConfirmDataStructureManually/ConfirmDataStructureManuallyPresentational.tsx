import { AddIcon, CardColumn, DataTable, DataTableSelect, NoIcon, styled } from "@operational/components";
import { inputFocus } from "@operational/components/lib/utils";
import React, { useMemo } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { ManagedTableColumnType, ManagedTableSchema } from "../../../../queries/hub/hub";
import { tableColumnNameRestriction } from "../../../../queries/hub/inputRestrictions";
import { managedTableColumnTypeOptions } from "../../../../queries/hub/selectOptions";
import { getColumnHeaderError } from "../../../../queries/hub/utils";
import { ErrorResponse } from "../../../../util/errors";
import { dataTableCellHeight, getTableBody, getTableHeight } from "../../SchemaTable/tableBodyUtils";
import StatefulDataTableInput from "../../StatefulDataTableInput";
import TableInfo from "../../TableInfo";

export interface ConfirmDataStructureManuallyPresentationalProps {
  schema: ManagedTableSchema;
  onAddColumn: () => void;
  onRemoveColumn: (index: number) => void;
  onColumnNameChange: (columnIndex: number, columnName: string) => void;
  onColumnTypeChange: (columnIndex: number, columnType: ManagedTableColumnType) => void;
  localValidationStatuses: boolean[];
  error: ErrorResponse;
}

const DeleteButtonContainer = styled.button`
  background-color: transparent; /* Fixes the grey bg of the button on Chrome Linux */
  width: ${dataTableCellHeight}px;
  height: ${dataTableCellHeight}px;
  margin: 0;
  padding: 0;
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
  text-align: center;
  appearance: none;
  border: 0;
  cursor: pointer;

  > * {
    pointer-events: none;
  }

  &:focus {
    ${({ theme }) => inputFocus({ theme })};
  }
`;

const Container = styled.div`
  display: grid;
  grid-template-columns: minmax(auto, min-content) 20px;
  grid-gap: ${({ theme }) => theme.space.element}px;
`;

const DataTableWrapper = styled.div`
  overflow: auto;
`;

const AddButtonContainer = styled.button`
  background-color: transparent; /* fixes the grey bg of the button on Chrome Linux */
  display: flex;
  align-items: center;
  justify-content: center;
  appearance: none;
  border: 1px solid ${({ theme }) => theme.color.primary};
  padding: ${({ theme }) => theme.space.base}px;
  cursor: pointer;

  &:focus {
    ${({ theme }) => inputFocus({ theme })};
  }
`;

const getColumnNameError = (
  index: number,
  columnNameValidity: boolean[],
  errorResponse: ErrorResponse,
  columnNameRestrictionMessage: string,
): string => {
  if (columnNameValidity[index] === false) {
    // ignore if undefined (was not validated yet)
    return columnNameRestrictionMessage;
  }

  return getColumnHeaderError(errorResponse, index);
};

const ConfirmDataStructureManuallyPresentational = injectIntl(
  ({
    schema,
    onAddColumn,
    onRemoveColumn,
    onColumnNameChange,
    onColumnTypeChange,
    localValidationStatuses,
    error,
    intl,
  }: ConfirmDataStructureManuallyPresentationalProps & InjectedIntlProps) => {
    const placeholderText = useMemo(
      () =>
        intl.formatMessage({
          id: "dataStore.tables.createWizard.confirmDataStructure.field.placeholder",
          defaultMessage: "Fill here",
        }),
      [],
    );

    const columnNameRestrictionMessage = useMemo(
      () =>
        intl.formatMessage(
          {
            id: "restriction.tableColumn",
            defaultMessage:
              "Column header must be between {minLength} and {maxLength} characters long. It must start with an alphabetic character and may only include alphanumeric characters and underscores '_'.",
          },
          { minLength: tableColumnNameRestriction.minLength, maxLength: tableColumnNameRestriction.maxLength },
        ),
      [tableColumnNameRestriction.minLength, tableColumnNameRestriction.maxLength],
    );

    const tableData = useMemo(() => {
      const { columns } = schema;
      return {
        columns: columns.map((column, index) => {
          const withDeleteButton = index > 0 || columns.length > 1;
          return [
            <>
              <StatefulDataTableInput
                index={index}
                placeholderText={placeholderText}
                value={column.name}
                setGlobalState={onColumnNameChange}
                withDeleteButton={withDeleteButton}
                error={getColumnNameError(index, localValidationStatuses, error, columnNameRestrictionMessage)}
              />
              {withDeleteButton && (
                <DeleteButtonContainer role="button" aria-label="Remove column" onClick={() => onRemoveColumn(index)}>
                  <NoIcon color="primary" />
                </DeleteButtonContainer>
              )}
            </>,
            <DataTableSelect
              options={managedTableColumnTypeOptions}
              value={column.type}
              onChange={newValue => {
                onColumnTypeChange(index, newValue as ManagedTableColumnType);
              }}
            />,
          ];
        }),
        rows: getTableBody(2, columns.length),
      };
    }, [schema, localValidationStatuses, error, columnNameRestrictionMessage]);

    return (
      <CardColumn
        title={intl.formatMessage({
          id: "dataStore.tables.createWizard.confirmDataStructure.manual.heading",
          defaultMessage: "Create table manually",
        })}
      >
        <TableInfo showInfo showSchemaColumnHeaderErrors error={error} />
        <Container>
          <DataTableWrapper>
            <DataTable
              height={getTableHeight(tableData.rows.length)}
              columns={tableData.columns}
              rows={tableData.rows}
            />
          </DataTableWrapper>
          <div>
            <AddButtonContainer onClick={onAddColumn}>
              <AddIcon size={12} color="primary" />
            </AddButtonContainer>
          </div>
        </Container>
      </CardColumn>
    );
  },
);

export default ConfirmDataStructureManuallyPresentational;
