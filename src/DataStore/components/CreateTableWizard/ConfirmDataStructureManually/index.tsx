import React from "react";
import { ManagedTableColumnType } from "../../../../queries/hub/hub";
import { getEmptyErrorTemplate } from "../../../../queries/hub/utils";
import CreateTableWizardContext from "../CreateTableWizardContext";
import { initialState } from "../state";
import ConfirmDataStructureManuallyPresentational from "./ConfirmDataStructureManuallyPresentational";

const ConfirmDataStructureManually = () => (
  <CreateTableWizardContext.Consumer>
    {({ state, enhancedDispatch }) => (
      <ConfirmDataStructureManuallyPresentational
        schema={state ? state.manualFlow.schema : initialState.manualFlow.schema}
        onAddColumn={() => {
          if (enhancedDispatch) {
            enhancedDispatch({
              type: "[create table wizard] manual-flow-add-column",
            });
          }
        }}
        onRemoveColumn={(index: number) => {
          if (enhancedDispatch) {
            enhancedDispatch({
              type: "[create table wizard] manual-flow-remove-column",
              index,
            });
          }
        }}
        onColumnNameChange={(index: number, name: string) => {
          if (enhancedDispatch) {
            enhancedDispatch({
              type: "[create table wizard] manual-flow-column-name-changed",
              index,
              name,
            });
          }
        }}
        onColumnTypeChange={(index: number, columnType: ManagedTableColumnType) => {
          if (enhancedDispatch && columnType !== null) {
            enhancedDispatch({
              type: "[create table wizard] manual-flow-column-type-changed",
              index,
              columnType,
            });
          }
        }}
        localValidationStatuses={state ? state.manualFlow.localValidationStatuses : []}
        error={state ? state.error : getEmptyErrorTemplate()}
      />
    )}
  </CreateTableWizardContext.Consumer>
);

export default ConfirmDataStructureManually;
