import React, { useCallback, useContext, useEffect, useState } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { useGetTable } from "../../../../queries/hub/hub";
import { tableNameRestriction } from "../../../../queries/hub/inputRestrictions";
import { getEmptyErrorTemplate } from "../../../../queries/hub/utils";
import { ErrorResponse } from "../../../../util/errors";
import { removeLeadingSpace } from "../../../utils";
import { canCreateWithName, getNameStatus, nameInitialState } from "../../../utils/nameValidation";
import CreateTableWizardContext from "../CreateTableWizardContext";
import { createTableAndIngestFileThunk, createTableThunk } from "../state/thunks";
import getSuggestedFileName, { nameMatcher } from "./getSuggestedFileName";
import ImportDataPresentational from "./ImportDataPresentational";

const ImportData = injectIntl(({ intl }: InjectedIntlProps) => {
  const {
    projectId,
    dataSource,
    state,
    createTable,
    createIngestionTask,
    enhancedDispatch,
    pushMessage,
    onTableCreated,
    showIngestionTask,
  } = useContext(CreateTableWizardContext);

  const [nameState, setNameState] = useState<typeof nameInitialState>({
    ...nameInitialState,
    name: state ? getSuggestedFileName(state.upload.nameOfFile) : "",
  });

  const [description, setDescription] = useState("");
  const [error, setError] = useState<ErrorResponse>(getEmptyErrorTemplate());

  useEffect(() => {
    setError(state ? state.error : getEmptyErrorTemplate());
  }, [state]);

  const { loading: isTableLoading, error: getTableError } = useGetTable({
    projectId,
    dataSourceId: dataSource ? dataSource.id : "",
    tableId: nameState.name,
    debounce: 250,
    localErrorOnly: true,
    lazy:
      nameState.name.length < tableNameRestriction.minLength || nameState.name.length > tableNameRestriction.maxLength,
  });

  useEffect(() => {
    setNameState({
      ...nameState,
      status: getNameStatus(nameState.name, tableNameRestriction, nameMatcher, isTableLoading, getTableError),
    });
  }, [isTableLoading, getTableError]);

  const setName = useCallback(
    (updatedName: string) => {
      setError(getEmptyErrorTemplate());
      setNameState({
        name: updatedName,
        status: getNameStatus(updatedName, tableNameRestriction, nameMatcher, true, null),
      });
    },
    [setNameState, setError],
  );

  const setProcessedDescription = useCallback(
    (input: string) => {
      setDescription(removeLeadingSpace(input));
    },
    [setDescription],
  );

  const manualFlowCreateTableHandler = useCallback(() => {
    if (!canCreateWithName(nameState)) {
      return;
    }
    if (state && enhancedDispatch && createTable && pushMessage && onTableCreated) {
      const schema = state.manualFlow.schema;
      enhancedDispatch(
        createTableThunk(nameState.name, description, createTable, schema, pushMessage, onTableCreated, intl),
      );
    }
  }, [nameState, description, state, enhancedDispatch, createTable, pushMessage, onTableCreated]);

  const discoveryFlowCreateTableHandler = useCallback(() => {
    if (!canCreateWithName(nameState)) {
      return;
    }
    if (state && enhancedDispatch && createTable && pushMessage && onTableCreated) {
      const schema = state.discoveryFlow.editableSchema;
      if (schema) {
        enhancedDispatch(
          createTableThunk(nameState.name, description, createTable, schema, pushMessage, onTableCreated, intl),
        );
      }
    }
  }, [nameState, description, state, enhancedDispatch, createTable, pushMessage, onTableCreated]);

  const discoveryFlowCreateTableAndImportDataHandler = useCallback(() => {
    if (!canCreateWithName(nameState)) {
      return;
    }
    if (enhancedDispatch && createTable && createIngestionTask && pushMessage && onTableCreated && showIngestionTask) {
      enhancedDispatch(
        createTableAndIngestFileThunk(
          nameState.name,
          description,
          createTable,
          createIngestionTask,
          pushMessage,
          onTableCreated,
          showIngestionTask,
          intl,
        ),
      );
    }
  }, [
    nameState,
    description,
    enhancedDispatch,
    createTable,
    createIngestionTask,
    pushMessage,
    onTableCreated,
    showIngestionTask,
  ]);

  return (
    <ImportDataPresentational
      nameState={nameState}
      setName={setName}
      description={description}
      setDescription={setProcessedDescription}
      canCreateWithName={canCreateWithName(nameState)}
      isCreateTableRequestRunning={state ? state.isCreateTableRequestRunning : false}
      manualFlowCreateTableHandler={manualFlowCreateTableHandler}
      discoveryFlowCreateTableHandler={discoveryFlowCreateTableHandler}
      discoveryFlowCreateTableAndImportDataHandler={discoveryFlowCreateTableAndImportDataHandler}
      error={error}
      canImportData={state ? state.flowStep === "import-data-discovery" : false}
    />
  );
});

export default ImportData;
