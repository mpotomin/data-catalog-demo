import getSuggestedFileName from "./getSuggestedFileName";

describe("getSuggestedFileName util", () => {
  it("should return an empty string empty string was given", () => {
    expect(getSuggestedFileName("")).toEqual("");
  });

  it("should an empty string if name does not fit the allowed pattern", () => {
    expect(getSuggestedFileName("123pollution_2016.csv")).toEqual("");
    expect(getSuggestedFileName("pollution_2016*.csv")).toEqual("");
    expect(getSuggestedFileName("мой_файл.csv")).toEqual("");
  });

  it("should return the filename if fits the pattern", () => {
    expect(getSuggestedFileName("crimeRate2018.csv")).toEqual("crime_rate_2018");
    expect(getSuggestedFileName("lowSavings_2018.csv")).toEqual("low_savings_2018");
    expect(getSuggestedFileName("life_expectancy_2016.csv")).toEqual("life_expectancy_2016");
  });
});
