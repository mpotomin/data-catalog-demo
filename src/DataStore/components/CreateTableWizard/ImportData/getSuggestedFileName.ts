import { snake } from "case";
import { tableNameRestriction } from "../../../../queries/hub/inputRestrictions";

export const nameMatcher = RegExp(tableNameRestriction.pattern);

const getSuggestedFileName = (nameOfFile: string) => {
  const processedName = snake(nameOfFile.split(".")[0]);
  return nameMatcher.test(processedName) ? processedName : "";
};

export default getSuggestedFileName;
