import { Button, CardColumn, CardItem, Input, Textarea } from "@operational/components";
import React, { useCallback, useMemo, useState } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { tableNameRestriction } from "../../../../queries/hub/inputRestrictions";
import { ErrorResponse, getFieldError } from "../../../../util/errors";
import { getInputRestrictionType, getInputStatusIcon, NameState } from "../../../utils/nameValidation";
import AddDescriptionButton from "../../AddDescriptionButton";
import NameDescriptionBox from "../../NameDescriptionBox";
import RestrictionMessage from "../../RestrictionMessage";

export interface ImportDataPresentationalProps {
  nameState: NameState;
  setName: (name: string) => void;
  description: string;
  setDescription: (description: string) => void;
  canImportData: boolean;
  canCreateWithName: boolean;
  isCreateTableRequestRunning: boolean;
  manualFlowCreateTableHandler: () => void;
  discoveryFlowCreateTableHandler: () => void;
  discoveryFlowCreateTableAndImportDataHandler: () => void;
  error: ErrorResponse;
}

const ImportDataPresentational = injectIntl(
  ({
    nameState,
    setName,
    description,
    setDescription,
    canImportData,
    canCreateWithName,
    isCreateTableRequestRunning,
    manualFlowCreateTableHandler,
    discoveryFlowCreateTableHandler,
    discoveryFlowCreateTableAndImportDataHandler,
    error,
    intl,
  }: ImportDataPresentationalProps & InjectedIntlProps) => {
    const [isDescriptionDrawerOpen, setDescriptionDrawerOpen] = useState(false);

    const openDescriptionCallback = useCallback(() => {
      setDescriptionDrawerOpen(true);
    }, [setDescriptionDrawerOpen]);

    const manualFlowCreateTableButton = useMemo(
      () => (
        <Button
          condensed
          color="primary"
          loading={isCreateTableRequestRunning}
          disabled={!canCreateWithName || isCreateTableRequestRunning}
          onClick={manualFlowCreateTableHandler}
        >
          {intl.formatMessage({
            id: "dataStore.tables.createWizard.modalActions.createTable",
            defaultMessage: "Create Table",
          })}
        </Button>
      ),
      [canCreateWithName, isCreateTableRequestRunning, manualFlowCreateTableHandler],
    );

    const discoveryFlowCreateButton = useMemo(
      () => (
        <Button
          condensed
          disabled={!canCreateWithName || isCreateTableRequestRunning}
          onClick={discoveryFlowCreateTableHandler}
        >
          {intl.formatMessage({
            id: "dataStore.tables.createWizard.modalActions.createTableWithoutImportingData",
            defaultMessage: "No, just create a Table",
          })}
        </Button>
      ),
      [canCreateWithName, isCreateTableRequestRunning, discoveryFlowCreateTableHandler],
    );

    const discoveryFlowCreateAndImportButton = useMemo(
      () => (
        <Button
          condensed
          color="primary"
          disabled={!canCreateWithName || isCreateTableRequestRunning}
          loading={isCreateTableRequestRunning}
          onClick={discoveryFlowCreateTableAndImportDataHandler}
        >
          {intl.formatMessage({
            id: "dataStore.tables.createWizard.modalActions.createTableAndImportData",
            defaultMessage: "Yes, create a Table and import data I uploaded",
          })}
        </Button>
      ),
      [isCreateTableRequestRunning, discoveryFlowCreateTableHandler],
    );

    const nameRestrictionMessage = useMemo(() => {
      if (nameState.status === "in-use") {
        return intl.formatMessage(
          {
            id: "restriction.table.inUse",
            defaultMessage:
              "Name is already in use. Name must be between {minLength} and {maxLength} characters long. It must start with an alphabetic character and may only include alphanumeric characters and underscores '_'.",
          },
          { minLength: tableNameRestriction.minLength, maxLength: tableNameRestriction.maxLength },
        );
      }
      return intl.formatMessage(
        {
          id: "restriction.table",
          defaultMessage:
            "Name must be between {minLength} and {maxLength} characters long. It must start with an alphabetic character and may only include alphanumeric characters and underscores '_'.",
        },
        { minLength: tableNameRestriction.minLength, maxLength: tableNameRestriction.maxLength },
      );
    }, [nameState.status]);

    return (
      <>
        <CardColumn
          title={intl.formatMessage({
            id: "dataStore.tables.createWizard.importData.heading.general",
            defaultMessage: "General information",
          })}
        >
          <NameDescriptionBox onSubmit={e => e.preventDefault()}>
            <CardItem
              title={intl.formatMessage({
                id: "dataStore.tables.createWizard.importData.name",
                defaultMessage: "Name",
              })}
              value={
                <>
                  <Input
                    isUniqueId
                    autoFocus
                    statusIcon={getInputStatusIcon(nameState.status)}
                    value={nameState.name}
                    onChange={setName}
                    error={getFieldError(error, "name")}
                  />
                  <RestrictionMessage
                    type={getInputRestrictionType(nameState.status)}
                    message={nameRestrictionMessage}
                  />
                </>
              }
            />
            {!isDescriptionDrawerOpen ? (
              <AddDescriptionButton disabled={isCreateTableRequestRunning} onClick={openDescriptionCallback} />
            ) : (
              <CardItem
                title={intl.formatMessage({
                  id: "dataStore.tables.createWizard.importData.description",
                  defaultMessage: "Description",
                })}
                value={
                  <Textarea value={description} onChange={setDescription} error={getFieldError(error, "description")} />
                }
              />
            )}
          </NameDescriptionBox>
        </CardColumn>
        {canImportData ? (
          <CardColumn
            title={intl.formatMessage({
              id: "dataStore.tables.createWizard.importData.discovery.heading",
              defaultMessage: "Your table structure has been defined. Do you want to import your data now?",
            })}
          >
            <>
              {discoveryFlowCreateAndImportButton}
              {discoveryFlowCreateButton}
            </>
          </CardColumn>
        ) : (
          <CardColumn
            title={intl.formatMessage({
              id: "dataStore.tables.createWizard.importData.manual.heading",
              defaultMessage: "Your table structure has been defined. You can add data later to it",
            })}
          >
            {manualFlowCreateTableButton}
          </CardColumn>
        )}
      </>
    );
  },
);

export default ImportDataPresentational;
