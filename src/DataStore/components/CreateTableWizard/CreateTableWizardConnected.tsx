import React, { useCallback, useContext, useMemo } from "react";
import { ManagedTableSchema } from "../../../queries/hub/hub";
import { tableColumnNameRestriction } from "../../../queries/hub/inputRestrictions";
import { getSchemaColumnHeaderErrorsCount } from "../../../queries/hub/utils";
import { isNameValid, tableColumnNameMatcher } from "../../utils/nameValidation";
import DiscoverDataStructureContext from "../DiscoverDataStructure/DiscoverDataStructureContext";
import { DiscoverDataStructureAction } from "../DiscoverDataStructure/state/actions";
import { discoverFileThunk, previewFileThunk } from "../DiscoverDataStructure/state/thunks";
import CreateTableWizardContext from "./CreateTableWizardContext";
import CreateTableWizardPresentational from "./CreateTableWizardPresentational";

const isSchemaValid = (schema: ManagedTableSchema): boolean => {
  const { columns } = schema;
  if (!columns.length) {
    return false;
  }

  return columns.every(column => isNameValid(column.name, tableColumnNameRestriction, tableColumnNameMatcher));
};

const CreateTableWizardConnected = () => {
  const { dataSource, state, enhancedDispatch, discoverFile, previewFile, closeWizard, pushMessage } = useContext(
    CreateTableWizardContext,
  );

  if (!state) {
    return null;
  }

  const { flowStep, discoveryFlow, isCreateTableRequestRunning } = state;
  const { isFileDiscoveryRunning } = discoveryFlow;

  const discoverDispatch = useCallback(
    (action: DiscoverDataStructureAction) => {
      if (enhancedDispatch && discoverFile && pushMessage) {
        enhancedDispatch(discoverFileThunk(action, discoveryFlow, discoverFile, pushMessage));
      }
    },
    [enhancedDispatch, discoverFile, pushMessage, discoveryFlow],
  );

  const previewDispatch = useCallback(
    (action: DiscoverDataStructureAction) => {
      if (enhancedDispatch && previewFile && pushMessage) {
        enhancedDispatch(previewFileThunk(action, discoveryFlow, previewFile, pushMessage));
      }
    },
    [enhancedDispatch, previewFile, pushMessage, discoveryFlow],
  );

  const closeWizardHandler = useCallback(() => {
    if (closeWizard) {
      closeWizard();
    }
  }, [closeWizard]);

  const manualFlowGoBackHandler = useCallback(() => {
    if (enhancedDispatch) {
      enhancedDispatch({ type: "[create table wizard] manual-flow-go-back" });
    }
  }, [enhancedDispatch]);

  const manualFlowContinueHandler = useCallback(() => {
    if (enhancedDispatch) {
      enhancedDispatch({ type: "[create table wizard] manual-flow-continue" });
    }
  }, [enhancedDispatch]);

  const discoveryFlowGoBackHandler = useCallback(() => {
    if (enhancedDispatch) {
      enhancedDispatch({ type: "[create table wizard] discovery-flow-go-back" });
    }
  }, [enhancedDispatch]);

  const discoveryFlowContinueHandler = useCallback(() => {
    if (enhancedDispatch) {
      enhancedDispatch({ type: "[create table wizard] discovery-flow-continue" });
    }
  }, []);

  const errorsInManualFlowColumnHeaders = useMemo(() => getSchemaColumnHeaderErrorsCount(state.error), [state.error]);

  const errorsInDiscoveryColumnHeaders = useMemo(() => getSchemaColumnHeaderErrorsCount(state.discoveryFlow.error), [
    state.discoveryFlow.error,
  ]);

  return (
    <DiscoverDataStructureContext.Provider
      value={{
        isDisabled: isFileDiscoveryRunning || isCreateTableRequestRunning,
        state: discoveryFlow,
        discoverDispatch,
        previewDispatch,
      }}
    >
      <CreateTableWizardPresentational
        dataSourceName={dataSource ? dataSource.name : ""}
        flowStep={flowStep}
        isManualFlowSchemaValid={isSchemaValid(state.manualFlow.schema) && errorsInManualFlowColumnHeaders < 1}
        isDiscoveryFlowSchemaValid={
          state.discoveryFlow.editableSchema
            ? isSchemaValid(state.discoveryFlow.editableSchema) && errorsInDiscoveryColumnHeaders < 1
            : false
        }
        isFileDiscoveryRunning={state ? state.discoveryFlow.isFileDiscoveryRunning : false}
        isCreateTableRequestRunning={state ? state.isCreateTableRequestRunning : false}
        closeWizardHandler={closeWizardHandler}
        manualFlowGoBackHandler={manualFlowGoBackHandler}
        manualFlowContinueHandler={manualFlowContinueHandler}
        discoveryFlowGoBackHandler={discoveryFlowGoBackHandler}
        discoveryFlowContinueHandler={discoveryFlowContinueHandler}
      />
    </DiscoverDataStructureContext.Provider>
  );
};

export default CreateTableWizardConnected;
