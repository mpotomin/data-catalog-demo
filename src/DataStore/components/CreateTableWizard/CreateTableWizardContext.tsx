import {
  CreateManagedTableRequest,
  DataSourceResponse,
  FileDiscoveryRequest,
  FileDiscoveryResponse,
  FileIngestRequest,
  FilePreviewRequest,
  FilePreviewResponse,
  TableInstanceResponse,
  TaskResponse,
  UploadResponse,
} from "../../../queries/hub/hub";

import { IMessage } from "@operational/components/lib/OperationalContext/OperationalContext";
import React from "react";
import { MutateMethod } from "restful-react";
import { CreateTableWizardProps } from "../CreateTableWizard";
import { Maybe } from "../../../types";
import { CreateTableWizardState } from "./state";
import { EnhancedDispatch } from "./state/enhanceDispatch";

const initial = Object.freeze({
  projectId: "",
  dataSource: undefined as Maybe<DataSourceResponse>,
  state: undefined as Maybe<CreateTableWizardState>,
  enhancedDispatch: undefined as Maybe<EnhancedDispatch>,
  uploadFile: undefined as Maybe<MutateMethod<UploadResponse, void>>,
  discoverFile: undefined as Maybe<MutateMethod<FileDiscoveryResponse, FileDiscoveryRequest>>,
  previewFile: undefined as Maybe<MutateMethod<FilePreviewResponse, FilePreviewRequest>>,
  createTable: undefined as Maybe<MutateMethod<TableInstanceResponse, CreateManagedTableRequest>>,
  createIngestionTask: undefined as Maybe<MutateMethod<TaskResponse, FileIngestRequest>>,
  closeWizard: undefined as Maybe<() => void>,
  onTableCreated: undefined as Maybe<CreateTableWizardProps["onTableCreated"]>,
  showIngestionTask: undefined as Maybe<CreateTableWizardProps["showIngestionTask"]>,
  pushMessage: undefined as Maybe<(message: IMessage) => void>,
});

const CreateTableWizardContext = React.createContext(initial);

export default CreateTableWizardContext;
