import React, { useCallback, useContext } from "react";
import { getEmptyErrorTemplate } from "../../../../queries/hub/utils";
import { uploadFile } from "../../DataStoreUploader/state/thunks";
import CreateTableWizardContext from "../CreateTableWizardContext";
import CreateTablePresentational from "./CreateTablePresentational";

const CreateTable = () => {
  const { projectId, dataSource, state, enhancedDispatch, closeWizard, pushMessage } = useContext(
    CreateTableWizardContext,
  );

  const uploadFileHandler = useCallback(
    (file: File) => {
      if (dataSource && enhancedDispatch && pushMessage) {
        enhancedDispatch(uploadFile(file, projectId, dataSource.id, pushMessage));
      }
    },
    [enhancedDispatch],
  );

  const abortUpload = useCallback(() => {
    if (state && state.upload && state.upload.request) {
      state.upload.request.abort();
    }
  }, [state]);

  const createManuallyHandler = useCallback(() => {
    if (enhancedDispatch) {
      enhancedDispatch({
        type: "[create table wizard] start-manual-flow",
      });
    }
  }, [enhancedDispatch]);

  const restartUploadHandler = useCallback(() => {
    if (enhancedDispatch) {
      enhancedDispatch({
        type: "[data store uploader] file-upload-restart",
      });
    }
  }, [enhancedDispatch]);

  return (
    <CreateTablePresentational
      fileName={state ? state.upload.nameOfFile : ""}
      uploadState={state ? state.upload.uploadState : "initial"}
      uploadProgress={state ? state.upload.uploadProgress : 0}
      closeWizard={closeWizard}
      uploadFile={uploadFileHandler}
      createManually={createManuallyHandler}
      restartUpload={restartUploadHandler}
      abortUpload={abortUpload}
      error={state ? state.error : getEmptyErrorTemplate()}
    />
  );
};

export default CreateTable;
