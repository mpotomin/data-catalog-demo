import { Button, CardColumn, CardItem, Form, styled, useHotkey } from "@operational/components";
import React, { useCallback, useRef } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { Maybe } from "../../../../types";
import { ErrorResponse } from "../../../../util/errors";
import DataStoreUploader from "../../DataStoreUploader";
import { FileUploadState } from "../../DataStoreUploader/state";

export interface CreateTablePresentationalProps {
  fileName: string;
  uploadState: FileUploadState;
  uploadProgress: number;
  closeWizard: Maybe<() => void>;
  uploadFile: (file: File) => void;
  createManually: () => void;
  abortUpload: () => void;
  restartUpload: () => void;
  error: ErrorResponse;
}

const Container = styled.div`
  display: grid;
  grid-column-gap: ${({ theme }) => theme.space.element}px;
  grid-auto-flow: column;
  justify-content: start;
  align-items: center;
`;

const Text = styled.span`
  color: ${({ theme }) => theme.color.border.default};
  font-weight: ${({ theme }) => theme.font.weight.bold};
  text-transform: uppercase;
`;

const ManualFlowControl = styled(Button)`
  white-space: nowrap;
`;

const CreateTablePresentational = injectIntl(
  ({
    fileName,
    uploadState,
    uploadProgress,
    closeWizard,
    uploadFile,
    abortUpload,
    restartUpload,
    createManually,
    intl,
  }: CreateTablePresentationalProps & InjectedIntlProps) => {
    const closeWizardHandler = useCallback(() => {
      if (closeWizard) {
        closeWizard();
      }
    }, [closeWizard]);
    const hotkeyScope = useRef(null);
    useHotkey(hotkeyScope, { key: "escape" }, closeWizardHandler);

    return (
      <Form
        onSubmit={e => {
          e.preventDefault();
        }}
      >
        <div ref={hotkeyScope}>
          <CardColumn
            title={intl.formatMessage({
              id: "dataStore.tables.createWizard.createTable.heading.how",
              defaultMessage: "How to create your table",
            })}
          >
            <CardItem
              title={intl.formatMessage({
                id: "dataStore.tables.createWizard.upload.heading",
                defaultMessage: "Upload a file you want to import",
              })}
              value={
                <Container>
                  <DataStoreUploader
                    fileName={fileName}
                    uploadState={uploadState}
                    uploadProgress={uploadProgress}
                    uploadFile={uploadFile}
                    abort={abortUpload}
                    restart={restartUpload}
                  />
                  <Text>
                    {intl.formatMessage({
                      id: "dataStore.tables.createWizard.createTable.or",
                      defaultMessage: "or",
                    })}
                  </Text>
                  <ManualFlowControl disabled={uploadState !== "initial"} onClick={createManually}>
                    {intl.formatMessage({
                      id: "dataStore.tables.createWizard.createTable.manualFlowButton",
                      defaultMessage: "Create structure manually",
                    })}
                  </ManualFlowControl>
                </Container>
              }
            />
          </CardColumn>
        </div>
      </Form>
    );
  },
);

export default CreateTablePresentational;
