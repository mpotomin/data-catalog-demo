import { FlowStep, initialState } from "../state";
import { defaultSchemaColumn, getEmptyErrorTemplate, schemaColumnPrefix } from "../../../../queries/hub/utils";
import { Error, ErrorResponse } from "../../../../util/errors";
import { getColumnNameLocalValidationStatuses } from "../../../utils/nameValidation";
import { initialFileUploaderState } from "../../DataStoreUploader/state";
import { DataStoreUploaderAction, isDataStoreUploaderAction } from "../../DataStoreUploader/state/actions";
import dataStoreUploaderReducer from "../../DataStoreUploader/state/reducer";
import { DiscoverDataStructureAction, isDiscoverDataStructureAction } from "../../DiscoverDataStructure/state/actions";
import discoverDataStructureReducer from "../../DiscoverDataStructure/state/reducer";
import { CreateTableWizardState } from "../state";
import { CreateTableWizardAction } from "./actions";

const hasSchemaError = (errorResponse: ErrorResponse) => {
  if (!errorResponse.errors) {
    return;
  }

  return errorResponse.errors.some(
    (error: Error) => error.type === "FieldError" && error.key.startsWith(schemaColumnPrefix),
  );
};

const getPostErrorFlowStep = (currentFlowStep: FlowStep, errorResponse: ErrorResponse): FlowStep => {
  if (hasSchemaError(errorResponse) && currentFlowStep === "import-data-manual") {
    return "confirm-data-structure-manual";
  }

  if (hasSchemaError(errorResponse) && currentFlowStep === "import-data-discovery") {
    return "confirm-data-structure-discovery";
  }

  return currentFlowStep;
};

const previousFlowStepDictionary: { [key in FlowStep]: FlowStep } = {
  "create-table": "create-table",
  "confirm-data-structure-manual": "create-table",
  "confirm-data-structure-discovery": "create-table",
  "import-data-manual": "confirm-data-structure-manual",
  "import-data-discovery": "confirm-data-structure-discovery",
};

function rootReducer(
  state: CreateTableWizardState,
  action: CreateTableWizardAction | DiscoverDataStructureAction | DataStoreUploaderAction,
): CreateTableWizardState {
  switch (action.type) {
    case "[data store uploader] file-upload-success":
      return {
        ...state,
        flowStep: "confirm-data-structure-discovery",
        discoveryFlow: {
          ...state.discoveryFlow,
          fileDiscoveryResponse: action.uploadResponse,
          preview: action.uploadResponse.preview,
          editableSchema: { ...action.uploadResponse.schema },
          editableFileOptions: { ...action.uploadResponse.options },
          error: action.uploadResponse.schemaErrors,
        },
      };
    case "[create table wizard] start-manual-flow":
      return { ...initialState, flowStep: "confirm-data-structure-manual" };
    case "[create table wizard] manual-flow-add-column":
      return {
        ...state,
        manualFlow: {
          ...state.manualFlow,
          schema: { ...state.manualFlow.schema, columns: [...state.manualFlow.schema.columns, defaultSchemaColumn] },
        },
      };
    case "[create table wizard] manual-flow-remove-column":
      const columns = [
        ...state.manualFlow.schema.columns.slice(0, action.index),
        ...state.manualFlow.schema.columns.slice(action.index + 1),
      ];
      const localValidationStatusesOnRemove = getColumnNameLocalValidationStatuses({
        ...state.manualFlow.schema,
        columns,
      });
      return {
        ...state,
        manualFlow: {
          ...state.manualFlow,
          schema: {
            ...state.manualFlow.schema,
            columns,
          },
          localValidationStatuses: localValidationStatusesOnRemove,
        },
        error: getEmptyErrorTemplate(),
      };
    case "[create table wizard] manual-flow-column-name-changed":
      const columnsNameChanged = [...state.manualFlow.schema.columns];
      columnsNameChanged[action.index] = { ...state.manualFlow.schema.columns[action.index], name: action.name };
      const localValidationStatusesNameChanged = getColumnNameLocalValidationStatuses({
        ...state.manualFlow.schema,
        columns: [...columnsNameChanged],
      });
      return {
        ...state,
        manualFlow: {
          ...state.manualFlow,
          schema: { ...state.manualFlow.schema, columns: [...columnsNameChanged] },
          localValidationStatuses: localValidationStatusesNameChanged,
        },
        error: getEmptyErrorTemplate(),
      };
    case "[create table wizard] manual-flow-column-type-changed":
      const columnsManualTypeChanged = [...state.manualFlow.schema.columns];
      columnsManualTypeChanged[action.index] = {
        ...state.manualFlow.schema.columns[action.index],
        type: action.columnType,
      };
      const localValidationStatusesColumnTypeChanged = getColumnNameLocalValidationStatuses({
        ...state.manualFlow.schema,
        columns: [...columnsManualTypeChanged],
      });
      return {
        ...state,
        manualFlow: {
          ...state.manualFlow,
          schema: { ...state.manualFlow.schema, columns: [...columnsManualTypeChanged] },
          localValidationStatuses: localValidationStatusesColumnTypeChanged,
        },
      };
    case "[create table wizard] manual-flow-go-back":
      return {
        ...state,
        flowStep: previousFlowStepDictionary[state.flowStep],
      };
    case "[create table wizard] manual-flow-continue":
      return { ...state, flowStep: "import-data-manual" };
    case "[create table wizard] discovery-flow-go-back":
      return {
        ...state,
        flowStep: previousFlowStepDictionary[state.flowStep],
        upload: {
          ...(previousFlowStepDictionary[state.flowStep] === "create-table" ? initialFileUploaderState : state.upload),
        },
      };
    case "[create table wizard] discovery-flow-continue":
      return { ...state, flowStep: "import-data-discovery" };
    case "[create table wizard] create-table-request-started":
      return {
        ...state,
        isCreateTableRequestRunning: true,
        error: getEmptyErrorTemplate(),
      };
    case "[create table wizard] create-table-request-success":
      return {
        ...state,
        isCreateTableRequestRunning: false,
      };
    case "[create table wizard] create-table-request-error":
      return {
        ...state,
        isCreateTableRequestRunning: false,
        flowStep: getPostErrorFlowStep(state.flowStep, action.error),
        error: action.error,
        discoveryFlow: {
          ...state.discoveryFlow,
          error: action.error,
        },
      };
    default:
      // Passing through the DiscoverDataStructureAction and DataStoreUploaderAction types
      return state;
  }
}

export default function reducer(
  state: CreateTableWizardState,
  action: CreateTableWizardAction | DiscoverDataStructureAction | DataStoreUploaderAction,
): CreateTableWizardState {
  const rootReducerResult = rootReducer(state, action);
  return {
    ...rootReducerResult,
    upload: isDataStoreUploaderAction(action)
      ? dataStoreUploaderReducer(rootReducerResult.upload, action)
      : rootReducerResult.upload,
    discoveryFlow: isDiscoverDataStructureAction(action)
      ? discoverDataStructureReducer(rootReducerResult.discoveryFlow, action)
      : rootReducerResult.discoveryFlow,
  };
}
