import { ManagedTableColumnType, TableInstanceResponse } from "../../../../queries/hub/hub";
import { ErrorResponse } from "../../../../util/errors";
import { DataStoreUploaderAction } from "../../DataStoreUploader/state/actions";
import { DiscoverDataStructureAction } from "../../DiscoverDataStructure/state/actions";

export type CreateTableWizardAction =
  | DiscoverDataStructureAction
  | DataStoreUploaderAction
  | { type: "[create table wizard] start-manual-flow" }
  | { type: "[create table wizard] manual-flow-add-column" }
  | { type: "[create table wizard] manual-flow-remove-column"; index: number }
  | { type: "[create table wizard] manual-flow-column-name-changed"; index: number; name: string }
  | {
      type: "[create table wizard] manual-flow-column-type-changed";
      index: number;
      columnType: ManagedTableColumnType;
    }
  | { type: "[create table wizard] manual-flow-go-back" }
  | { type: "[create table wizard] manual-flow-continue" }
  | { type: "[create table wizard] discovery-flow-go-back" }
  | { type: "[create table wizard] discovery-flow-continue" }
  | { type: "[create table wizard] create-table-request-started" }
  | { type: "[create table wizard] create-table-request-success"; response: TableInstanceResponse }
  | { type: "[create table wizard] create-table-request-error"; error: ErrorResponse };
