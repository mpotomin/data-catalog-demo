import { CreateTableWizardProps } from "../../CreateTableWizard";
import enhanceDispatch from "../../../state/enhanceDispatch";
import { DiscoverDataStructureAction } from "../../DiscoverDataStructure/state/actions";
import { CreateTableWizardState } from "../state";
import { DataStoreUploaderAction } from "../../DataStoreUploader/state/actions";
import { CreateTableWizardAction } from "./actions";

export type EnhancedDispatch = ReturnType<typeof enhanceCreateTableWizardDispatch>;

const enhanceCreateTableWizardDispatch = (
  dispatch: React.Dispatch<CreateTableWizardAction | DiscoverDataStructureAction | DataStoreUploaderAction>,
  props: CreateTableWizardProps,
  state: CreateTableWizardState,
) => enhanceDispatch(dispatch, props, state);

export default enhanceCreateTableWizardDispatch;
