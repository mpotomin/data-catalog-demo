import { defaultSchemaColumn, getEmptyErrorTemplate } from "../../../../queries/hub/utils";
import { initialFileUploaderState } from "../../DataStoreUploader/state";
import { initialState as initialDiscoverState } from "../../DiscoverDataStructure/state";

export type CreateTableWizardState = typeof initialState;

export type FlowStep =
  | "create-table"
  | "confirm-data-structure-manual"
  | "confirm-data-structure-discovery"
  | "import-data-manual"
  | "import-data-discovery";

export const defaultManualFlowState = Object.freeze({
  schema: { columns: [defaultSchemaColumn], indexes: [] },
  localValidationStatuses: [] as boolean[],
});

export const initialState = Object.freeze({
  flowStep: "create-table" as FlowStep,
  manualFlow: defaultManualFlowState,
  upload: initialFileUploaderState,
  discoveryFlow: initialDiscoverState,
  isCreateTableRequestRunning: false,
  error: getEmptyErrorTemplate(),
});
