import { CreateTableWizardState, initialState } from ".";
import { UploadResponse } from "../../../../queries/hub/hub";
import { defaultSchemaColumn, getEmptyErrorTemplate } from "../../../../queries/hub/utils";
import { initialFileUploaderState } from "../../DataStoreUploader/state";
import reducer from "./reducer";

import {
  getFileOptionsTemplate,
  getSchemaColumnErrorTemplate,
  getSchemaTemplate,
  getTableInstanceResponseTemplate,
  getTestErrorTemplate,
} from "../../../../queries/hub/testUtils";

describe("CreateTableWizard reducer", () => {
  describe("[create table wizard] file-upload-success", () => {
    const state: CreateTableWizardState = {
      ...initialState,
      flowStep: "create-table",
      discoveryFlow: {
        ...initialState.discoveryFlow,
        fileDiscoveryResponse: undefined,
        editableSchema: undefined,
        editableFileOptions: undefined,
      },
    };

    const uploadResponse: UploadResponse = {
      uploadedBytes: 123,
      fileId: "qwe",
      collectionId: "345",
      schema: getSchemaTemplate(),
      targetSchema: null,
      options: getFileOptionsTemplate(),
      preview: [[{ name: "preview-name", value: "preview-value", error: "preview-error" }]],
      schemaErrors: getTestErrorTemplate(),
    };

    const newState = reducer(state, {
      type: "[data store uploader] file-upload-success",
      uploadResponse,
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should advance the flow step value", () => {
      expect(newState.flowStep).toEqual("confirm-data-structure-discovery");
    });

    it("should set the uploadResponse value to the fileDiscoveryResponse prop of the discoveryFlow slice of the state from the action payload", () => {
      expect(newState.discoveryFlow.fileDiscoveryResponse).not.toEqual(state.discoveryFlow.fileDiscoveryResponse);
      expect(newState.discoveryFlow.fileDiscoveryResponse).toEqual(uploadResponse);
    });

    it("should set the preview property from the action payload", () => {
      expect(newState.discoveryFlow.preview).not.toEqual(state.discoveryFlow.preview);
      expect(newState.discoveryFlow.preview).toEqual(uploadResponse.preview);
    });

    it("should set the editable schema from the action payload", () => {
      expect(newState.discoveryFlow.editableSchema).toEqual(uploadResponse.schema);
    });

    it("should set the editable file options from the action payload", () => {
      expect(newState.discoveryFlow.editableFileOptions).toEqual(uploadResponse.options);
    });

    it("should set fieldErrors value of the discovery flow state slice from the action payload", () => {
      expect(newState.discoveryFlow.error).toEqual(uploadResponse.schemaErrors);
    });
  });

  describe("[create table wizard] create-structure-manually", () => {
    const state: CreateTableWizardState = {
      ...initialState,
      upload: {
        ...initialFileUploaderState,
        uploadResponse: {
          uploadedBytes: 123,
          fileId: "qwe",
          collectionId: "345",
          schema: getSchemaTemplate(),
          targetSchema: null,
          schemaErrors: { errors: [] },
          options: getFileOptionsTemplate(),
          preview: [[{ name: "", value: "", error: "" }]],
        },
      },
      error: getTestErrorTemplate(),
    };

    const newState = reducer(state, { type: "[create table wizard] start-manual-flow" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should reset the state of the Wizard and advance the flow step", () => {
      const reference = { ...initialState, flowStep: "confirm-data-structure-manual" };
      expect(newState).toEqual(reference);
    });
  });

  describe("[create table wizard] manual-flow-add-column", () => {
    const state: CreateTableWizardState = {
      ...initialState,
    };

    const newState = reducer(state, { type: "[create table wizard] manual-flow-add-column" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should add a default column to the schema structure", () => {
      expect(newState.manualFlow.schema.columns.length).toEqual(2);
      expect(newState.manualFlow.schema.columns[1]).toEqual(defaultSchemaColumn);
    });
  });

  describe("[create table wizard] manual-flow-remove-column", () => {
    function getInitialState(): CreateTableWizardState {
      return {
        ...initialState,
        manualFlow: {
          ...initialState.manualFlow,
          schema: {
            ...getSchemaTemplate(),
            columns: [
              { ...defaultSchemaColumn, name: "col0" },
              { ...defaultSchemaColumn, name: "col1" },
              { ...defaultSchemaColumn, name: "col2" },
            ],
          },
        },
        error: getEmptyErrorTemplate(),
      };
    }

    it("should return a new object", () => {
      const state = getInitialState();
      const newState = reducer(state, {
        type: "[create table wizard] manual-flow-remove-column",
        index: 0,
      });

      expect(state === newState).toBeFalsy();
    });

    it("should correctly remove the first column", () => {
      const newState = reducer(getInitialState(), {
        type: "[create table wizard] manual-flow-remove-column",
        index: 0,
      });

      expect(newState.manualFlow.schema.columns.length).toEqual(2);
      expect(newState.manualFlow.schema.columns[0].name).toEqual(getInitialState().manualFlow.schema.columns[1].name);
      expect(newState.manualFlow.schema.columns[1].name).toEqual(getInitialState().manualFlow.schema.columns[2].name);
    });

    it("should correctly remove the last column", () => {
      const newState = reducer(getInitialState(), {
        type: "[create table wizard] manual-flow-remove-column",
        index: 2,
      });

      expect(newState.manualFlow.schema.columns.length).toEqual(2);
      expect(newState.manualFlow.schema.columns[0].name).toEqual(getInitialState().manualFlow.schema.columns[0].name);
      expect(newState.manualFlow.schema.columns[1].name).toEqual(getInitialState().manualFlow.schema.columns[1].name);
    });

    it("should correctly remove an arbitrary column", () => {
      const newState = reducer(getInitialState(), {
        type: "[create table wizard] manual-flow-remove-column",
        index: 1,
      });

      expect(newState.manualFlow.schema.columns.length).toEqual(2);
      expect(newState.manualFlow.schema.columns[0].name).toEqual(getInitialState().manualFlow.schema.columns[0].name);
      expect(newState.manualFlow.schema.columns[1].name).toEqual(getInitialState().manualFlow.schema.columns[2].name);
    });

    it("should validate the column names", () => {
      const state = getInitialState();
      const newState = reducer(state, {
        type: "[create table wizard] manual-flow-remove-column",
        index: 0,
      });

      expect(newState.manualFlow.localValidationStatuses).not.toEqual(state.manualFlow.localValidationStatuses);
      expect(newState.manualFlow.localValidationStatuses).toEqual([true, true]);
    });

    it("should clean up the field errors object", () => {
      const state = getInitialState();
      const newState = reducer(state, {
        type: "[create table wizard] manual-flow-remove-column",
        index: 0,
      });

      expect(newState.error).toEqual(getEmptyErrorTemplate());
    });
  });

  describe("[create table wizard] manual-flow-column-name-changed", () => {
    function getInitialState(): CreateTableWizardState {
      return {
        ...initialState,
        manualFlow: {
          ...initialState.manualFlow,
          schema: {
            ...getSchemaTemplate(),
            columns: [
              { ...defaultSchemaColumn, name: "col0" },
              { ...defaultSchemaColumn, name: "col1" },
              { ...defaultSchemaColumn, name: "col2" },
            ],
          },
        },
        error: getTestErrorTemplate(),
      };
    }

    it("should return a new object", () => {
      const state = getInitialState();
      const newState = reducer(state, {
        type: "[create table wizard] manual-flow-column-name-changed",
        name: "col-new-name",
        index: 1,
      });

      expect(state === newState).toBeFalsy();
    });

    it("should update the name of the column in state", () => {
      const newState = reducer(getInitialState(), {
        type: "[create table wizard] manual-flow-column-name-changed",
        name: "col-new-name",
        index: 1,
      });

      expect(newState.manualFlow.schema.columns[1].name).toEqual("col-new-name");
    });

    it("should validate the column names", () => {
      const newState = reducer(getInitialState(), {
        type: "[create table wizard] manual-flow-column-name-changed",
        name: "",
        index: 1,
      });

      expect(newState.manualFlow.localValidationStatuses).not.toEqual(
        getInitialState().manualFlow.localValidationStatuses,
      );
      expect(newState.manualFlow.localValidationStatuses).toEqual([true, false, true]);
    });

    it("should remove the field errors", () => {
      const newState = reducer(getInitialState(), {
        type: "[create table wizard] manual-flow-column-name-changed",
        name: "col-new-name",
        index: 1,
      });

      expect(newState.error).toEqual(getEmptyErrorTemplate());
    });
  });

  describe("[create table wizard] manual-flow-column-type-changed", () => {
    function getInitialState(): CreateTableWizardState {
      return {
        ...initialState,
        manualFlow: {
          ...initialState.manualFlow,
          schema: {
            ...getSchemaTemplate(),
            columns: [
              { ...defaultSchemaColumn, name: "col0", type: "bool" },
              { ...defaultSchemaColumn, name: "col1", type: "decimal" },
              { ...defaultSchemaColumn, name: "col2", type: "integer" },
            ],
          },
        },
      };
    }

    it("should return a new object", () => {
      const state = getInitialState();
      const newState = reducer(state, {
        type: "[create table wizard] manual-flow-column-type-changed",
        index: 1,
        columnType: "datetime",
      });

      expect(state === newState).toBeFalsy();
    });

    it("should update the type of the column in state", () => {
      const newState = reducer(getInitialState(), {
        type: "[create table wizard] manual-flow-column-type-changed",
        index: 1,
        columnType: "datetime",
      });

      expect(newState.manualFlow.schema.columns[1].type).not.toEqual(
        getInitialState().manualFlow.schema.columns[1].type,
      );
      expect(newState.manualFlow.schema.columns[1].type).toEqual("datetime");
    });

    it("should validate the column names", () => {
      const newState = reducer(getInitialState(), {
        type: "[create table wizard] manual-flow-column-type-changed",
        index: 1,
        columnType: "datetime",
      });

      expect(newState.manualFlow.localValidationStatuses).not.toEqual(
        getInitialState().manualFlow.localValidationStatuses,
      );
      expect(newState.manualFlow.localValidationStatuses).toEqual([true, true, true]);
    });
  });

  describe("[create table wizard] manual-flow-go-back", () => {
    const state: CreateTableWizardState = {
      ...initialState,
      flowStep: "confirm-data-structure-manual",
    };

    const newState = reducer(state, { type: "[create table wizard] manual-flow-go-back" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should not change the step of the flow for step1", () => {
      const newState1 = reducer(
        {
          ...initialState,
          flowStep: "create-table",
        },
        { type: "[create table wizard] manual-flow-go-back" },
      );
      expect(newState1.flowStep).toEqual("create-table");
    });

    it("should change the step of the flow for step2", () => {
      const newState1 = reducer(
        {
          ...initialState,
          flowStep: "confirm-data-structure-manual",
        },
        { type: "[create table wizard] manual-flow-go-back" },
      );
      expect(newState1.flowStep).toEqual("create-table");
    });

    it("should change the step of the flow for step3 and keep field errors intact", () => {
      const newState1 = reducer(
        {
          ...initialState,
          flowStep: "import-data-manual",
          error: getTestErrorTemplate(),
        },
        { type: "[create table wizard] manual-flow-go-back" },
      );
      expect(newState1.flowStep).toEqual("confirm-data-structure-manual");
      expect(newState1.error).toEqual(getTestErrorTemplate());
    });
  });

  describe("[create table wizard] manual-flow-continue", () => {
    const state: CreateTableWizardState = {
      ...initialState,
      flowStep: "confirm-data-structure-manual",
    };

    const newState = reducer(state, { type: "[create table wizard] manual-flow-continue" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should move ahead one step of the flow", () => {
      expect(newState.flowStep).toEqual("import-data-manual");
    });
  });

  describe("[create table wizard] discovery-flow-go-back", () => {
    const state: CreateTableWizardState = {
      ...initialState,
      flowStep: "confirm-data-structure-discovery",
      upload: {
        ...initialFileUploaderState,
        uploadState: "completed",
        uploadProgress: 0.95,
      },
    };

    const newState = reducer(state, { type: "[create table wizard] discovery-flow-go-back" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should reset the upload progress and state if backing up from `confirm-data-structure-discovery` step", () => {
      expect(newState.upload.uploadState).toEqual("initial");
      expect(newState.upload.uploadProgress).toEqual(0);
    });

    it("should NOT reset the upload progress and state if backing up from `import-data-discovery` step", () => {
      const state0: CreateTableWizardState = {
        ...initialState,
        flowStep: "import-data-discovery",
        upload: {
          ...initialFileUploaderState,
          nameOfFile: "file-name",
          uploadState: "completed",
          uploadProgress: 0.95,
        },
      };
      const newState0 = reducer(state0, { type: "[create table wizard] discovery-flow-go-back" });

      expect(newState0.upload).toEqual(state0.upload);
      expect(newState0.upload).not.toEqual(newState.upload);
    });

    it("should not change the step of the flow for step1", () => {
      const newState1 = reducer(
        {
          ...initialState,
          flowStep: "create-table",
        },
        { type: "[create table wizard] discovery-flow-go-back" },
      );
      expect(newState1.flowStep).toEqual("create-table");
    });

    it("should change the step of the flow for step2", () => {
      const newState1 = reducer(
        {
          ...initialState,
          flowStep: "confirm-data-structure-discovery",
        },
        { type: "[create table wizard] discovery-flow-go-back" },
      );
      expect(newState1.flowStep).toEqual("create-table");
    });

    it("should change the step of the flow for step3 and keep field errors intact", () => {
      const newState1 = reducer(
        {
          ...initialState,
          flowStep: "import-data-discovery",
          error: getTestErrorTemplate(),
        },
        { type: "[create table wizard] discovery-flow-go-back" },
      );
      expect(newState1.flowStep).toEqual("confirm-data-structure-discovery");
      expect(newState1.error).toEqual(getTestErrorTemplate());
    });
  });

  describe("[create table wizard] discovery-flow-continue", () => {
    const state: CreateTableWizardState = {
      ...initialState,
      flowStep: "confirm-data-structure-discovery",
    };

    const newState = reducer(state, { type: "[create table wizard] discovery-flow-continue" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should move ahead one step of the flow", () => {
      expect(newState.flowStep).toEqual("import-data-discovery");
    });
  });

  describe("[create table wizard] manual-flow-create-table-request-started", () => {
    const state: CreateTableWizardState = {
      ...initialState,
      error: getTestErrorTemplate(),
    };

    const newState = reducer(state, { type: "[create table wizard] create-table-request-started" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should set the loading flag enabled", () => {
      expect(newState.isCreateTableRequestRunning).not.toEqual(state.isCreateTableRequestRunning);
      expect(newState.isCreateTableRequestRunning).toBeTruthy();
    });

    it("should clear up the error state", () => {
      expect(newState.error).not.toEqual(state.error);
      expect(newState.error).toEqual(getEmptyErrorTemplate());
    });
  });

  describe("[create table wizard] manual-flow-create-table-request-success", () => {
    const state: CreateTableWizardState = {
      ...initialState,
    };

    const response = getTableInstanceResponseTemplate();

    const newState = reducer(state, {
      type: "[create table wizard] create-table-request-success",
      response,
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should set the loading flag disabled", () => {
      expect(newState.isCreateTableRequestRunning).toBeFalsy();
    });
  });

  describe("[create table wizard] create-table-request-error", () => {
    const state: CreateTableWizardState = {
      ...initialState,
      flowStep: "confirm-data-structure-manual",
    };

    const newState = reducer(state, {
      type: "[create table wizard] create-table-request-error",
      error: getTestErrorTemplate(),
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should set the loading flag disabled", () => {
      expect(newState.isCreateTableRequestRunning).toBeFalsy();
    });

    it("should populate the state with the fieldErrors from the response", () => {
      expect(newState.error).toEqual(getTestErrorTemplate());
    });

    it("should populate the discoveryFlow slice of the state with the fieldErrors from the response", () => {
      expect(newState.discoveryFlow.error).toEqual(getTestErrorTemplate());
    });

    it("should not modify the flow step for an arbitrary error value", () => {
      expect(newState.flowStep).toEqual("confirm-data-structure-manual");
    });

    it("If the error includes the Schema field error and we are using the manual flow - move back to the manual schema definition step", () => {
      const state2: CreateTableWizardState = {
        ...initialState,
        flowStep: "import-data-manual",
      };

      const newState2 = reducer(state2, {
        type: "[create table wizard] create-table-request-error",
        error: getSchemaColumnErrorTemplate(),
      });

      expect(newState2.flowStep).toEqual("confirm-data-structure-manual");
    });

    it("If the error includes the Schema field error and we are using the discovery flow - move back to the discovery schema definition step", () => {
      const state2: CreateTableWizardState = {
        ...initialState,
        flowStep: "import-data-discovery",
      };

      const newState2 = reducer(state2, {
        type: "[create table wizard] create-table-request-error",
        error: getSchemaColumnErrorTemplate(),
      });

      expect(newState2.flowStep).toEqual("confirm-data-structure-discovery");
    });
  });
});
