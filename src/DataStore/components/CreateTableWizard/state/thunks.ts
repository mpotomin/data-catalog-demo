import {
  CreateManagedTableRequest,
  FileIngestRequest,
  ManagedTableSchema,
  TableInstanceResponse,
  TaskResponse,
} from "../../../../queries/hub/hub";

import { IMessage } from "@operational/components/lib/OperationalContext/OperationalContext";
import { InjectedIntl } from "react-intl";
import { MutateMethod } from "restful-react";
import { CreateTableWizardState } from "../state";
import { CreateTableWizardProps } from "../../CreateTableWizard";
import { getErrorData } from "../../../../util/errors";
import { CreateTableWizardAction } from "./actions";

export const createTableThunk = (
  name: string,
  description: string,
  create: MutateMethod<TableInstanceResponse, CreateManagedTableRequest>,
  schema: ManagedTableSchema,
  pushMessage: (message: IMessage) => void,
  onTableCreated: CreateTableWizardProps["onTableCreated"],
  intl: InjectedIntl,
) => (dispatch: React.Dispatch<CreateTableWizardAction>) => {
  dispatch({ type: "[create table wizard] create-table-request-started" });
  create({ name, description, expiresAt: null, defaultInsertMode: "upsert", schema })
    .then((response: TableInstanceResponse) => {
      dispatch({ type: "[create table wizard] create-table-request-success", response });
      const body = intl.formatMessage(
        {
          id: "dataStore.tables.createWizard.success",
          defaultMessage: "Table {tableName} created",
        },
        { tableName: response.data.name },
      );
      pushMessage({ body, type: "success" });
      onTableCreated(response);
    })
    .catch(error => {
      dispatch({
        type: "[create table wizard] create-table-request-error",
        error: getErrorData(error),
      });
    });
};

export const createTableAndIngestFileThunk = (
  name: string,
  description: string,
  createTable: MutateMethod<TableInstanceResponse, CreateManagedTableRequest>,
  createIngestionTask: MutateMethod<TaskResponse, FileIngestRequest>,
  pushMessage: (message: IMessage) => void,
  onTableCreated: CreateTableWizardProps["onTableCreated"],
  showIngestionTask: CreateTableWizardProps["showIngestionTask"],
  intl: InjectedIntl,
) => (dispatch: React.Dispatch<CreateTableWizardAction>, _: CreateTableWizardProps, state: CreateTableWizardState) => {
  dispatch({ type: "[create table wizard] create-table-request-started" });
  const schema = state.discoveryFlow.editableSchema;
  const options = state.discoveryFlow.editableFileOptions;
  const uploadResponse = state.upload.uploadResponse;
  if (!schema || !options || !uploadResponse) {
    pushMessage({ body: "No table structure defined", type: "error" });
    return;
  }
  const defaultInsertMode = "upsert";
  createTable({ name, description, expiresAt: null, defaultInsertMode, schema })
    .then((tableResponse: TableInstanceResponse) => {
      return createIngestionTask({
        tableId: tableResponse.data.id,
        fileId: uploadResponse.fileId,
        schema,
        options,
        insertMode: defaultInsertMode,
      }).then(ingestionTaskResponse => {
        const body = intl.formatMessage(
          {
            id: "dataStore.tasks.ingestionTaskCreated",
            defaultMessage: "Ingestion of {fileName} enqueued",
          },
          { fileName: ingestionTaskResponse.fileName },
        );
        pushMessage({
          body,
          type: "success",
          onClick: () => showIngestionTask(ingestionTaskResponse),
        });
        onTableCreated(tableResponse);
      });
    })
    .catch(error => {
      dispatch({
        type: "[create table wizard] create-table-request-error",
        error: getErrorData(error),
      });
    });
};
