import {
  CreateIngestionTask,
  DataSourceResponse,
  DiscoverFileInManagedDataSource,
  PreviewFileInManagedDataSource,
  TableInstanceResponse,
  TaskResponse,
  UploadManagedDataSourceFile,
  useCreateManagedTable,
} from "../../../queries/hub/hub";

import { OperationalContext } from "@operational/components";
import React, { useEffect, useReducer, useState } from "react";
import CreateTableWizardConnected from "./CreateTableWizardConnected";
import CreateTableWizardContext from "./CreateTableWizardContext";
import { initialState } from "./state";
import enhanceCreateTableWizardDispatch from "./state/enhanceDispatch";
import reducer from "./state/reducer";

export interface CreateTableWizardProps {
  projectId: string;
  dataSource: DataSourceResponse;
  closeWizard: () => void;
  onTableCreated: (table: TableInstanceResponse) => void;
  showIngestionTask: (task: TaskResponse) => void;
}

export interface CreateTableAction {
  type: "add-table";
  dataSource: DataSourceResponse;
}

const CreateTableWizard = (props: CreateTableWizardProps) => {
  const { projectId, dataSource, closeWizard, onTableCreated, showIngestionTask } = props;
  const [state, dispatch] = useReducer(reducer, initialState);

  const [fileId, setFileId] = useState("no-fileId-defined");
  useEffect(() => {
    if (state.upload.uploadResponse) {
      setFileId(state.upload.uploadResponse.fileId);
    }
  }, [state.upload.uploadResponse]);

  const enhancedDispatch = enhanceCreateTableWizardDispatch(dispatch, props, state);

  const { mutate: createTable } = useCreateManagedTable({ projectId, dataSourceId: dataSource.id });

  return (
    <OperationalContext>
      {({ pushMessage }) => (
        <UploadManagedDataSourceFile projectId={projectId} dataSourceId={dataSource.id}>
          {uploadFile => (
            <DiscoverFileInManagedDataSource projectId={projectId} dataSourceId={dataSource.id} fileId={fileId}>
              {discoverFile => (
                <PreviewFileInManagedDataSource
                  projectId={projectId}
                  dataSourceId={dataSource.id}
                  fileId={fileId}
                  localErrorOnly
                >
                  {previewFile => (
                    <CreateIngestionTask projectId={projectId} dataSourceId={dataSource.id}>
                      {createIngestionTask => (
                        <CreateTableWizardContext.Provider
                          value={{
                            projectId,
                            dataSource,
                            state,
                            enhancedDispatch,
                            uploadFile,
                            discoverFile,
                            previewFile,
                            createTable,
                            createIngestionTask,
                            closeWizard,
                            onTableCreated,
                            showIngestionTask,
                            pushMessage,
                          }}
                        >
                          <CreateTableWizardConnected />
                        </CreateTableWizardContext.Provider>
                      )}
                    </CreateIngestionTask>
                  )}
                </PreviewFileInManagedDataSource>
              )}
            </DiscoverFileInManagedDataSource>
          )}
        </UploadManagedDataSourceFile>
      )}
    </OperationalContext>
  );
};

export default CreateTableWizard;
