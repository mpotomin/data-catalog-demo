import { Button, Modal, Stepper, styled } from "@operational/components";
import React, { useMemo } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import DiscoverDataStructure from "../DiscoverDataStructure";
import ConfirmDataStructureManually from "./ConfirmDataStructureManually";
import CreateTable from "./CreateTable";
import ImportData from "./ImportData";
import { FlowStep } from "./state";

export interface CreateTableWizardPresentationalProps {
  dataSourceName: string;
  flowStep: FlowStep;
  isManualFlowSchemaValid: boolean;
  isDiscoveryFlowSchemaValid: boolean;
  isFileDiscoveryRunning: boolean;
  isCreateTableRequestRunning: boolean;
  closeWizardHandler: () => void;
  manualFlowGoBackHandler: () => void;
  manualFlowContinueHandler: () => void;
  discoveryFlowContinueHandler: () => void;
  discoveryFlowGoBackHandler: () => void;
}

const getStepperIndex = (flowStep: FlowStep) => {
  switch (flowStep) {
    case "create-table":
      return 0;
    case "confirm-data-structure-manual":
      return 1;
    case "confirm-data-structure-discovery":
      return 1;
    case "import-data-manual":
      return 2;
    case "import-data-discovery":
      return 2;
  }
};

const StepperContainer = styled.div`
  display: flex;
  justify-content: center;
`;

const ModalContent = styled.div`
  min-width: 800px;
  min-height: 400px;
  padding: 2px; /* padding so the focus border of the controls doesn't get cut off */
`;

const Steps = styled(Stepper)`
  width: 550px;
`;

const getStepTitles = (flowStep: FlowStep, intl: ReactIntl.InjectedIntl) => {
  if (flowStep === "confirm-data-structure-manual" || flowStep === "import-data-manual") {
    return [
      {
        title: intl.formatMessage({
          id: "dataStore.tables.createWizard.stepper.step1",
          defaultMessage: "How to create your table",
        }),
        content: null,
      },
      {
        title: intl.formatMessage({
          id: "dataStore.tables.createWizard.stepper.step2.manual",
          defaultMessage: "Create data structure",
        }),
        content: null,
      },
      {
        title: intl.formatMessage({
          id: "dataStore.tables.createWizard.stepper.step3.manual",
          defaultMessage: "Create table",
        }),
        content: null,
      },
    ];
  }

  return [
    {
      title: intl.formatMessage({
        id: "dataStore.tables.createWizard.stepper.step1",
        defaultMessage: "How to create your table",
      }),
      content: null,
    },
    {
      title: intl.formatMessage({
        id: "dataStore.tables.createWizard.stepper.step2",
        defaultMessage: "Confirm data structure",
      }),
      content: null,
    },
    {
      title: intl.formatMessage({
        id: "dataStore.tables.createWizard.stepper.step3",
        defaultMessage: "Import data",
      }),
      content: null,
    },
  ];
};

const CreateTableWizardPresentational = injectIntl(
  ({
    dataSourceName,
    flowStep,
    isManualFlowSchemaValid,
    isDiscoveryFlowSchemaValid,
    isFileDiscoveryRunning,
    isCreateTableRequestRunning,
    closeWizardHandler,
    manualFlowGoBackHandler,
    manualFlowContinueHandler,
    discoveryFlowGoBackHandler,
    discoveryFlowContinueHandler,
    intl,
  }: CreateTableWizardPresentationalProps & InjectedIntlProps) => {
    const closeWizardButton = (
      <Button condensed disabled={isCreateTableRequestRunning} onClick={closeWizardHandler}>
        {intl.formatMessage({
          id: "dataStore.tables.createWizard.modalActions.cancel",
          defaultMessage: "Cancel",
        })}
      </Button>
    );

    const manualFlowGoBackButton = (
      <Button condensed disabled={isCreateTableRequestRunning} onClick={manualFlowGoBackHandler}>
        {intl.formatMessage({
          id: "dataStore.tables.createWizard.modalActions.back",
          defaultMessage: "Back",
        })}
      </Button>
    );

    const manualFlowContinueButton = (
      <Button condensed color="primary" disabled={!isManualFlowSchemaValid} onClick={manualFlowContinueHandler}>
        {intl.formatMessage({
          id: "dataStore.tables.createWizard.modalActions.continue",
          defaultMessage: "Continue",
        })}
      </Button>
    );

    const discoveryFlowGoBackButton = (
      <Button
        condensed
        disabled={isFileDiscoveryRunning || isCreateTableRequestRunning}
        onClick={discoveryFlowGoBackHandler}
      >
        {intl.formatMessage({
          id: "dataStore.tables.createWizard.modalActions.back",
          defaultMessage: "Back",
        })}
      </Button>
    );

    const discoveryFlowContinueButton = (
      <Button
        condensed
        color="primary"
        loading={isFileDiscoveryRunning}
        disabled={isFileDiscoveryRunning || !isDiscoveryFlowSchemaValid}
        onClick={discoveryFlowContinueHandler}
      >
        {intl.formatMessage({
          id: "dataStore.tables.createWizard.modalActions.continue",
          defaultMessage: "Continue",
        })}
      </Button>
    );

    const actions = useMemo(() => {
      switch (flowStep) {
        case "create-table":
          return [closeWizardButton];
        case "confirm-data-structure-manual":
          return [manualFlowContinueButton, manualFlowGoBackButton];
        case "confirm-data-structure-discovery":
          return [discoveryFlowContinueButton, discoveryFlowGoBackButton];
        case "import-data-manual":
          return [closeWizardButton, manualFlowGoBackButton];
        case "import-data-discovery":
          return [closeWizardButton, discoveryFlowGoBackButton];
      }
    }, [
      flowStep,
      manualFlowGoBackButton,
      manualFlowContinueButton,
      discoveryFlowGoBackButton,
      discoveryFlowContinueButton,
      closeWizardButton,
    ]);

    return (
      <Modal title={`Add Table to ${dataSourceName}`} width="max-content" height="auto" isOpen actions={actions}>
        <ModalContent>
          <StepperContainer>
            <Steps steps={getStepTitles(flowStep, intl)} activeSlideIndex={getStepperIndex(flowStep)} />
          </StepperContainer>
          {flowStep === "create-table" && <CreateTable />}
          {flowStep === "confirm-data-structure-manual" && <ConfirmDataStructureManually />}
          {flowStep === "confirm-data-structure-discovery" && <DiscoverDataStructure mode="modify-schema" />}
          {(flowStep === "import-data-manual" || flowStep === "import-data-discovery") && <ImportData />}
        </ModalContent>
      </Modal>
    );
  },
);

export default CreateTableWizardPresentational;
