import { DataTableInput, styled, Tooltip } from "@operational/components";
import React, { useCallback, useState } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";

interface Props {
  index: number;
  placeholderText: string;
  value: string;
  setGlobalState: (index: number, state: string) => void;
  error: string;
  withDeleteButton?: boolean;
  disabled?: boolean;
}

const StyledDataTableInput = styled(DataTableInput)<Pick<Props, "withDeleteButton">>`
  padding: 8px ${({ withDeleteButton }) => (withDeleteButton ? 26 : 12)}px 8px 12px;
`;

const StatefulDataTableInput = injectIntl(
  ({ index, placeholderText, value, disabled, setGlobalState, withDeleteButton, error }: Props & InjectedIntlProps) => {
    const [state, setState] = useState(value);
    const [isHovered, setIsHovered] = useState(false);

    const onMouseOver = useCallback(() => {
      setIsHovered(true);
    }, [setIsHovered]);

    const onMouseOut = useCallback(() => {
      setIsHovered(false);
    }, [setIsHovered]);

    const errorComponentFactory = useCallback(() => (isHovered ? <Tooltip position="bottom">{error}</Tooltip> : null), [
      isHovered,
      error,
    ]);

    return (
      <StyledDataTableInput
        placeholder={placeholderText}
        onBlur={() => {
          setGlobalState(index, state);
        }}
        value={state}
        disabled={disabled}
        onChange={setState}
        withDeleteButton={withDeleteButton}
        error={error}
        onMouseOver={onMouseOver}
        onMouseOut={onMouseOut}
        errorComponent={errorComponentFactory}
      />
    );
  },
);

export default StatefulDataTableInput;
