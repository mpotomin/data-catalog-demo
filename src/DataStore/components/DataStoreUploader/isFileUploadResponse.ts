import { UploadResponse } from "../../../queries/hub/hub";

export default function isFileUploadResponse(response: UploadResponse | any): response is UploadResponse {
  if (response === undefined || response === null) {
    return false;
  }

  return (
    response.fileId !== undefined &&
    response.fileId !== null &&
    typeof response.fileId === "string" &&
    response.fileId.length > 0
  );
}
