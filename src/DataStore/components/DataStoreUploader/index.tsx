import { InfoIcon, NoIcon, Progress, Spinner, styled, Uploader } from "@operational/components";
import { inputFocus } from "@operational/components/lib/utils";
import React, { createRef, useRef } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { sliceTextIfNeeded } from "../../../util/sliceTextIfNeeded";
import acceptedUploadFileFormat from "../../utils/acceptedUploadFileFormat";
import { FileUploadState } from "./state";

interface DataStoreUploaderProps {
  fileName: string;
  uploadState: FileUploadState;
  uploadProgress: number;
  uploadFile: (file: File) => void;
  abort: () => void;
  restart: () => void;
}

const ActionContainer = styled.button`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border: 0;
  background-color: transparent;
  font: inherit;
  padding: ${({ theme }) => theme.space.medium}px;
  cursor: pointer;
  outline: 0;

  &:hover {
    ${({ theme }) => inputFocus({ theme })};
  }

  &:focus {
    ${({ theme }) => inputFocus({ theme })};
  }

  & input {
    display: none;
  }
`;

const ActiveMessage = styled.span`
  color: ${({ theme }) => theme.color.primary};
  font-weight: ${({ theme }) => theme.font.weight.bold};
`;

const InfoContainer = styled.div`
  display: flex;
  align-items: center;
`;

const InfoText = styled.p`
  color: ${({ theme }) => theme.color.text.lighter};
`;

const ErrorContainer = styled.div`
  color: ${({ theme }) => theme.color.error};
`;

const ProgressContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const AbortUpload = styled.button`
  width: ${({ theme }) => theme.space.big}px;
  height: ${({ theme }) => theme.space.big}px;
  margin-left: ${({ theme }) => theme.space.small}px;
  display: flex;
  color: ${({ theme }) => theme.color.border};
  align-items: center;
  justify-content: center;
  appearance: none;
  background-color: transparent; /* fixes the grey bg of the button on Chrome Linux */
  border: 1px solid ${({ theme }) => theme.color.border};
  border-radius: ${({ theme }) => Math.round(theme.space.big / 2)}px;
  cursor: pointer;

  &:hover {
    ${({ theme }) => inputFocus({ theme })};
  }

  &:focus {
    ${({ theme }) => inputFocus({ theme })};
  }
`;

const DataStoreUploader = injectIntl(
  ({
    fileName,
    uploadState,
    uploadProgress,
    uploadFile,
    abort,
    restart,
    intl,
  }: DataStoreUploaderProps & InjectedIntlProps) => {
    const [isDragging, setIsDragging] = React.useState(false);
    const cachedTarget = useRef<EventTarget | null>(null);

    const fileInputRef = createRef<HTMLInputElement>();

    React.useEffect(() => {
      if (uploadState !== "initial") {
        return;
      }

      const dragEnterListener = (event: DragEvent) => {
        cachedTarget.current = event.target;
      };

      const dragLeaveListener = (event: DragEvent) => {
        if (event.target === cachedTarget.current) {
          setIsDragging(false);
        }
      };

      const dragDropListener = (event: DragEvent) => {
        event.preventDefault();
        setIsDragging(false);
        if (event.dataTransfer !== null && event.dataTransfer.files.length > 0) {
          const file = event.dataTransfer.files[0];
          uploadFile(file);
        }
      };

      const dragOverListener = (event: DragEvent) => {
        event.preventDefault();
        setIsDragging(true);
      };

      document.addEventListener("dragenter", dragEnterListener);
      document.addEventListener("dragleave", dragLeaveListener);
      document.addEventListener("dragover", dragOverListener);
      document.addEventListener("drop", dragDropListener);

      return () => {
        document.removeEventListener("dragenter", dragEnterListener);
        document.removeEventListener("dragleave", dragLeaveListener);
        document.removeEventListener("dragover", dragOverListener);
        document.removeEventListener("drop", dragDropListener);
      };
    }, [uploadState]);

    return (
      <Uploader dragActive={uploadState === "initial" && isDragging}>
        {uploadState === "initial" && !isDragging && (
          <ActionContainer
            onClick={() => {
              if (fileInputRef.current !== null) {
                fileInputRef.current.click();
              }
            }}
          >
            {intl.formatMessage({
              id: "dataStore.uploader.dropHere",
              defaultMessage: "Drop your file here",
            })}
            <br />
            {intl.formatMessage({
              id: "dataStore.uploader.or",
              defaultMessage: "or",
            })}
            <br />
            <ActiveMessage>
              {intl.formatMessage({
                id: "dataStore.uploader.browseFiles",
                defaultMessage: "browse files",
              })}
            </ActiveMessage>
            <input
              ref={fileInputRef}
              disabled={uploadState !== "initial"}
              id="browse"
              type="file"
              accept={acceptedUploadFileFormat}
              onChange={event => {
                if (event.target.files && event.target.files.length) {
                  uploadFile(event.target.files[0]);
                }
              }}
            />
          </ActionContainer>
        )}
        {uploadState === "initial" &&
          isDragging &&
          intl.formatMessage({
            id: "dataStore.uploader.dropHere",
            defaultMessage: "Drop your file here",
          })}
        {uploadState === "uploading" && (
          <>
            <ProgressContainer>
              <Progress inline percentage={uploadProgress * 100} width={196} />
              <AbortUpload onClick={abort}>
                <NoIcon size={12} />
              </AbortUpload>
            </ProgressContainer>
            <InfoText>
              {sliceTextIfNeeded(
                intl.formatMessage(
                  {
                    id: "dataStore.uploader.uploading",
                    defaultMessage: "Uploading {fileName}",
                  },
                  { fileName },
                ),
                45,
              )}
            </InfoText>
          </>
        )}
        {uploadState === "aborted" && (
          <ErrorContainer>
            <InfoIcon left size={12} />
            {intl.formatMessage({
              id: "dataStore.uploader.aborted",
              defaultMessage: "Upload cancelled",
            })}
          </ErrorContainer>
        )}
        {uploadState === "processing" && (
          <InfoContainer>
            <Spinner left color="primary" />
            {intl.formatMessage({
              id: "dataStore.uploader.processing",
              defaultMessage: "Processing...",
            })}
          </InfoContainer>
        )}

        {uploadState === "completed" && (
          <ActiveMessage>
            {intl.formatMessage({
              id: "dataStore.uploader.completed",
              defaultMessage: "Completed",
            })}
          </ActiveMessage>
        )}
        {uploadState === "error" && (
          <ActionContainer onClick={restart}>
            <ErrorContainer>
              <InfoIcon left size={12} />
              {intl.formatMessage({
                id: "dataStore.uploader.failed",
                defaultMessage: "Upload failed",
              })}
            </ErrorContainer>
            <br />
            <ActiveMessage>
              {intl.formatMessage({
                id: "dataStore.uploader.tryAgain",
                defaultMessage: "Try again",
              })}
            </ActiveMessage>
          </ActionContainer>
        )}
      </Uploader>
    );
  },
);

export default DataStoreUploader;
