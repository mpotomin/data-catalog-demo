import { UploadResponse } from "../../../../queries/hub/hub";
import { Maybe } from "../../../../types";

export type FileUploadState = "initial" | "uploading" | "aborted" | "processing" | "error" | "completed";

export type DataStoreUploadState = typeof initialFileUploaderState;

export const initialFileUploaderState = Object.freeze({
  nameOfFile: "",
  request: undefined as Maybe<XMLHttpRequest>,
  uploadState: "initial" as FileUploadState,
  uploadProgress: 0,
  uploadResponse: undefined as Maybe<UploadResponse>,
});
