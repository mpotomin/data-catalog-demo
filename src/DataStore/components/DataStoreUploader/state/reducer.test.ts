import { UploadResponse } from "../../../../queries/hub/hub";
import { getFileOptionsTemplate, getSchemaTemplate } from "../../../../queries/hub/testUtils";
import { DataStoreUploadState, initialFileUploaderState } from "./index";
import reducer from "./reducer";

describe("DataStoreUpload reducer", () => {
  describe("[data store uploader] file-upload-started", () => {
    const state: DataStoreUploadState = {
      ...initialFileUploaderState,
      uploadResponse: {
        uploadedBytes: 123,
        fileId: "qwe",
        collectionId: "345",
        schema: getSchemaTemplate(),
        targetSchema: null,
        options: getFileOptionsTemplate(),
        preview: [[{ name: "", value: "", error: "" }]],
        schemaErrors: null,
      },
      uploadState: "error",
      uploadProgress: 0.75,
    };

    const nameOfFile = "some-file_name";
    const request = new XMLHttpRequest();
    const newState = reducer(state, { type: "[data store uploader] file-upload-started", nameOfFile, request });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should set the name of the file from the action payload", () => {
      expect(state.nameOfFile).not.toEqual(nameOfFile);
      expect(newState.nameOfFile).toEqual(nameOfFile);
    });

    it("should set the request of the file upload from the action payload", () => {
      expect(newState.request).not.toEqual(state.request);
      expect(newState.request).toEqual(request);
    });

    it("should reset the state of the Wizard and enable file uploading flag", () => {
      const reference = {
        ...initialFileUploaderState,
        nameOfFile,
        uploadState: "uploading",
        uploadProgress: 0,
        request,
      };
      expect(newState).toEqual(reference);
    });
  });

  describe("[data store uploader] file-upload-aborted", () => {
    const state: DataStoreUploadState = {
      ...initialFileUploaderState,
      uploadProgress: 0.1,
      uploadState: "uploading",
    };

    const newState = reducer(state, { type: "[data store uploader] file-upload-aborted" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should update the state to signify the aborted upload state", () => {
      expect(newState.uploadState).toEqual("aborted");
    });
  });

  describe("[data store uploader] file-upload-progress", () => {
    const state: DataStoreUploadState = {
      ...initialFileUploaderState,
      uploadProgress: 0.1,
    };

    const progress = 0.725;
    const newState = reducer(state, { type: "[data store uploader] file-upload-progress", progress });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should set the progress value from the action payload", () => {
      expect(newState.uploadProgress).not.toEqual(state.uploadProgress);
      expect(newState.uploadProgress).toEqual(progress);
    });
  });

  describe("[data store uploader] processing-started", () => {
    const state: DataStoreUploadState = {
      ...initialFileUploaderState,
      uploadProgress: 0.1,
      uploadState: "uploading",
    };

    const newState = reducer(state, { type: "[data store uploader] processing-started" });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should update the state to signify the processing of the file", () => {
      expect(newState.uploadProgress).toEqual(1.0);
      expect(newState.uploadState).toEqual("processing");
    });
  });

  describe("[data store uploader] file-upload-success", () => {
    const state: DataStoreUploadState = {
      ...initialFileUploaderState,
      uploadResponse: undefined,
      uploadState: "uploading",
    };

    const uploadResponse: UploadResponse = {
      uploadedBytes: 123,
      fileId: "qwe",
      collectionId: "345",
      schema: getSchemaTemplate(),
      targetSchema: null,
      options: getFileOptionsTemplate(),
      preview: [[{ name: "preview-name", value: "preview-value", error: "preview-error" }]],
      schemaErrors: null,
    };

    const newState = reducer(state, {
      type: "[data store uploader] file-upload-success",
      uploadResponse,
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should disable file uploading flag", () => {
      expect(newState.uploadState).toEqual("completed");
    });

    it("should set the upload response data from the action payload", () => {
      expect(newState.uploadResponse).toEqual(uploadResponse);
    });
  });

  describe("[data store uploader] file-upload-error", () => {
    const state: DataStoreUploadState = {
      ...initialFileUploaderState,
      uploadState: "uploading",
    };

    const newState = reducer(state, {
      type: "[data store uploader] file-upload-error",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should disable file uploading flag", () => {
      expect(newState.uploadState).toEqual("error");
    });
  });

  describe("[data store uploader] file-upload-restart", () => {
    const state: DataStoreUploadState = {
      ...initialFileUploaderState,
      uploadState: "error",
      uploadProgress: 0.67,
    };

    const newState = reducer(state, {
      type: "[data store uploader] file-upload-restart",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should disable file uploading flag", () => {
      expect(newState.uploadState).toEqual("initial");
      expect(newState.uploadProgress).toEqual(0);
    });
  });
});
