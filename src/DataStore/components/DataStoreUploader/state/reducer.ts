import { DataStoreUploadState, initialFileUploaderState } from "../state";
import { DataStoreUploaderAction } from "./actions";

function reducer(state: DataStoreUploadState, action: DataStoreUploaderAction): DataStoreUploadState {
  switch (action.type) {
    case "[data store uploader] file-upload-started":
      return {
        ...initialFileUploaderState,
        uploadState: "uploading",
        uploadProgress: 0,
        nameOfFile: action.nameOfFile,
        request: action.request,
      };
    case "[data store uploader] file-upload-aborted":
      return {
        ...state,
        uploadState: "aborted",
      };
    case "[data store uploader] file-upload-progress":
      return {
        ...state,
        uploadProgress: action.progress,
      };
    case "[data store uploader] processing-started":
      return {
        ...state,
        uploadProgress: 1.0,
        uploadState: "processing",
      };
    case "[data store uploader] file-upload-success":
      return {
        ...state,
        uploadResponse: { ...action.uploadResponse },
        uploadState: "completed",
      };
    case "[data store uploader] file-upload-error":
      return {
        ...state,
        uploadState: "error",
      };
    case "[data store uploader] file-upload-restart":
      return {
        ...state,
        uploadState: "initial",
        uploadProgress: 0,
      };
  }
}

export default reducer;
