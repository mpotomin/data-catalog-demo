import { UploadResponse } from "../../../../queries/hub/hub";

export const isDataStoreUploaderAction = (action: DataStoreUploaderAction | any): action is DataStoreUploaderAction =>
  action !== undefined &&
  action !== null &&
  action.type &&
  typeof action.type === "string" &&
  action.type.startsWith("[data store uploader]");

export type DataStoreUploaderAction =
  | { type: "[data store uploader] file-upload-started"; nameOfFile: string; request: XMLHttpRequest }
  | { type: "[data store uploader] file-upload-progress"; progress: number }
  | { type: "[data store uploader] file-upload-aborted" }
  | { type: "[data store uploader] processing-started" }
  | { type: "[data store uploader] file-upload-success"; uploadResponse: UploadResponse }
  | { type: "[data store uploader] file-upload-error" }
  | { type: "[data store uploader] file-upload-restart" };
