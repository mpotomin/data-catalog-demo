import { IMessage } from "@operational/components/lib/OperationalContext/OperationalContext";
import { UploadResponse } from "../../../../queries/hub/hub";
import getHubXMLHttpRequest from "../../../../../queries/hub/hubXMLHttpRequest";
import { getGeneralErrors, isErrorResponse } from "../../../../util/errors";
import isFileUploadResponse from "../isFileUploadResponse";
import { DataStoreUploaderAction } from "./actions";

export const uploadFile = (
  file: File,
  projectId: string,
  dataSourceId: string,
  pushMessage: (message: IMessage) => void,
  tableId?: string,
) => (dispatch: React.Dispatch<DataStoreUploaderAction>) => {
  const formData = new FormData();
  formData.append("file", file);

  if (tableId) {
    formData.append("tableId", tableId); // If provided, `UploadResponse` will include optional `targetSchema`
  }

  const request = getHubXMLHttpRequest(`/api/v1/${projectId}/datasources/${dataSourceId}/uploads`, "POST");
  request.upload.onprogress = progressHandler;
  request.onload = completeHandler;
  request.onerror = errorHandler;
  request.onabort = abortHandler;

  dispatch({ type: "[data store uploader] file-upload-started", nameOfFile: file.name, request });

  request.send(formData);

  function progressHandler(event: ProgressEvent) {
    const progress = event.loaded / event.total;
    if (progress < 1) {
      dispatch({ type: "[data store uploader] file-upload-progress", progress });
      return;
    }

    dispatch({ type: "[data store uploader] processing-started" });
  }

  function abortHandler() {
    dispatch({ type: "[data store uploader] file-upload-aborted" });

    setTimeout(() => {
      dispatch({ type: "[data store uploader] file-upload-restart" });
    }, 1500);
  }

  function completeHandler() {
    if (request.status !== 200) {
      dispatch({ type: "[data store uploader] file-upload-error" });
      pushMessage({ body: `Upload error ${request.statusText}`, type: "error" });
      return;
    }

    let uploadResponse: UploadResponse | null = null;
    try {
      uploadResponse = JSON.parse(request.responseText);
    } catch (error) {
      dispatch({ type: "[data store uploader] file-upload-error" });
      pushMessage({ body: "Upload error", type: "error" });
    }

    if (isFileUploadResponse(uploadResponse)) {
      dispatch({ type: "[data store uploader] file-upload-success", uploadResponse });
      return;
    }

    if (isErrorResponse(uploadResponse)) {
      const errorMessage = getGeneralErrors(uploadResponse);
      pushMessage({ body: errorMessage ? errorMessage : "Error", type: "error" });
      dispatch({ type: "[data store uploader] file-upload-error" });
      return;
    }

    dispatch({ type: "[data store uploader] file-upload-error" });
    pushMessage({ body: "Upload error", type: "error" });
  }

  function errorHandler() {
    dispatch({ type: "[data store uploader] file-upload-error" });
  }
};
