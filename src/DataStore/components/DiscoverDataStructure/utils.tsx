import React from "react";
import { ColumnMapping, SchemaPreviewCell } from "../../../queries/hub/hub";
import Cell from "./Cell";
import { dropColumnOptionValue } from "./MapSchemaView/columnMappingUtils";

export const getTableBody = (data: SchemaPreviewCell[][], mapping?: ColumnMapping) =>
  data.map(row =>
    row.map((cell, index) => {
      const isDisabled = mapping ? mapping[index].target === dropColumnOptionValue : false;
      return <Cell value={cell.value} disabled={isDisabled} error={cell.error} />;
    }),
  );

export const getEmptyTableBody = (columns: number, rows: number): SchemaPreviewCell[][] =>
  Array.from({ length: rows }, () =>
    Array.from(
      { length: columns },
      (): SchemaPreviewCell => ({
        name: "",
        value: "",
        error: "",
      }),
    ),
  );
