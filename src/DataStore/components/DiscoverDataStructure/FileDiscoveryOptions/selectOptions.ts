import { FileDecimalSeparator, FileDelimiter, FilePreferredDateFormat } from "../../../../queries/hub/hub";

export const fileDelimiterOptions = [
  { label: ", (comma)", value: "," as FileDelimiter },
  { label: "; (semicolon)", value: ";" as FileDelimiter },
  { label: "\\t (tab)", value: "\t" as FileDelimiter },
];

export const fileDecimalSeparatorOptions = [
  { label: ", (comma)", value: "numericComma" as FileDecimalSeparator },
  { label: ". (period)", value: "numericPeriod" as FileDecimalSeparator },
];

export const filePreferredDateFormatOptions = [
  { label: "DD/MM/YY", value: "dayFirst" as FilePreferredDateFormat },
  { label: "MM/DD/YY", value: "monthFirst" as FilePreferredDateFormat },
  { label: "YY/MM/DD", value: "yearFirst" as FilePreferredDateFormat },
];
