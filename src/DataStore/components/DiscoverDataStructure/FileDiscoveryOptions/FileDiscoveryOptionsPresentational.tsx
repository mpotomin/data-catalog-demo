import { CardItem, Checkbox, Select, styled } from "@operational/components";
import React from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { fileEncodingOptionOptions } from "../../../../queries/hub/selectOptions";
import { fileDecimalSeparatorOptions, fileDelimiterOptions, filePreferredDateFormatOptions } from "./selectOptions";
import StatefulSelect from "./StatefulSelect";

import {
  FileDecimalSeparator,
  FileDelimiter,
  FileEncodingOption,
  FileHasHeader,
  FileIgnoreHeaders,
  FileOptions,
  FilePreferredDateFormat,
} from "../../../../queries/hub/hub";

interface FileDiscoveryOptionsPresentationalProps {
  isDisabled: boolean;
  encodingOptions: typeof fileEncodingOptionOptions;
  fileOptions: FileOptions;
  onFileDelimiterChange: (delimiter: FileDelimiter) => void;
  onDateFormatChange: (dateFormat: FilePreferredDateFormat) => void;
  onEncodingChange: (encoding: FileEncodingOption) => void;
  onDecimalSeparatorChange: (decimalSeparator: FileDecimalSeparator) => void;
  onHasHeaderChange: (hasHeader: FileHasHeader) => void;
  onIgnoreHeadersChange: (ignoreHeaders: FileIgnoreHeaders) => void;
}

const SelectsGrid = styled.div`
  display: grid;
  max-width: ${({ theme }) => theme.pageSize.max}px; /* Prevent the row of controls from getting stretched */
  grid-template-columns: 90px repeat(5, auto);
  grid-gap: ${({ theme }) => theme.space.small}px;
`;

const FullWidthSelect = styled(Select)`
  width: 100%;
`;

const HeaderOptionsContainer = styled.div`
  display: grid;
  grid-gap: ${({ theme }) => theme.space.medium}px;
`;

const FileDiscoveryOptionsPresentational = injectIntl(
  ({
    isDisabled,
    encodingOptions,
    fileOptions,
    onFileDelimiterChange,
    onDateFormatChange,
    onEncodingChange,
    onDecimalSeparatorChange,
    onHasHeaderChange,
    onIgnoreHeadersChange,
    intl,
  }: FileDiscoveryOptionsPresentationalProps & InjectedIntlProps) => (
    <SelectsGrid>
      <CardItem
        title={intl.formatMessage({
          id: "dataStore.tables.createWizard.confirmDataStructure.item.fileType",
          defaultMessage: "File Type",
        })}
        value={
          <Select
            options={[{ value: "CSV" }, { value: "TSV" }]}
            value={fileOptions.delimiter === "\t" ? "TSV" : "CSV"}
            disabled={true}
          />
        }
      />
      <CardItem
        title={intl.formatMessage({
          id: "dataStore.tables.createWizard.confirmDataStructure.item.delimiter",
          defaultMessage: "Delimiter",
        })}
        value={
          <StatefulSelect
            options={fileDelimiterOptions}
            value={fileOptions.delimiter}
            disabled={isDisabled}
            onChange={newValue => {
              onFileDelimiterChange(newValue as FileDelimiter);
            }}
          />
        }
      />
      <CardItem
        title={intl.formatMessage({
          id: "dataStore.tables.createWizard.confirmDataStructure.item.dateFormat",
          defaultMessage: "Date Format",
        })}
        value={
          <FullWidthSelect
            options={filePreferredDateFormatOptions}
            value={fileOptions.preferredDateFormat}
            disabled={isDisabled}
            onChange={newValue => {
              onDateFormatChange(newValue as FilePreferredDateFormat);
            }}
          />
        }
      />
      <CardItem
        title={intl.formatMessage({
          id: "dataStore.tables.createWizard.confirmDataStructure.item.encoding",
          defaultMessage: "Encoding",
        })}
        value={
          <FullWidthSelect
            options={encodingOptions}
            value={fileOptions.encoding}
            disabled={isDisabled}
            onChange={newValue => {
              onEncodingChange(newValue as FileEncodingOption);
            }}
          />
        }
      />
      <CardItem
        title={intl.formatMessage({
          id: "dataStore.tables.createWizard.confirmDataStructure.item.decimalSeparator",
          defaultMessage: "Decimal separator",
        })}
        value={
          <FullWidthSelect
            options={fileDecimalSeparatorOptions}
            value={fileOptions.decimalSeparator}
            disabled={isDisabled}
            onChange={newValue => {
              onDecimalSeparatorChange(newValue as FileDecimalSeparator);
            }}
          />
        }
      />
      <CardItem
        title={intl.formatMessage({
          id: "dataStore.tables.createWizard.confirmDataStructure.item.options",
          defaultMessage: "Options",
        })}
        value={
          <HeaderOptionsContainer>
            <Checkbox
              condensed
              label={intl.formatMessage({
                id: "dataStore.tables.createWizard.confirmDataStructure.item.hasHeader",
                defaultMessage: "Has header",
              })}
              disabled={isDisabled}
              value={fileOptions.hasHeader}
              onChange={onHasHeaderChange}
            />
            <Checkbox
              condensed
              label={intl.formatMessage({
                id: "dataStore.tables.createWizard.confirmDataStructure.item.ignoreHeaders",
                defaultMessage: "Ignore first row",
              })}
              disabled={isDisabled || !fileOptions.hasHeader}
              value={fileOptions.ignoreHeader}
              onChange={onIgnoreHeadersChange}
            />
          </HeaderOptionsContainer>
        }
      />
    </SelectsGrid>
  ),
);

export default FileDiscoveryOptionsPresentational;
