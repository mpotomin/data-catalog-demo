import React, { useCallback, useContext } from "react";
import DiscoverDataStructureContext from "../DiscoverDataStructureContext";
import useEncodingOptions from "../useEncodingOptions";
import FileDiscoveryOptionsPresentational from "./FileDiscoveryOptionsPresentational";

import {
  FileDecimalSeparator,
  FileDelimiter,
  FileEncodingOption,
  FileHasHeader,
  FileIgnoreHeaders,
  FilePreferredDateFormat,
} from "../../../../queries/hub/hub";

const FileDiscoveryOptions = () => {
  const { isDisabled, state, discoverDispatch, previewDispatch } = useContext(DiscoverDataStructureContext);
  if (!state) {
    return null;
  }

  const encodingOptions = useEncodingOptions(state);

  const { editableFileOptions } = state;

  if (!editableFileOptions) {
    return null;
  }

  const onFileDelimiterChange = useCallback(
    (delimiter: FileDelimiter) => {
      if (discoverDispatch && delimiter !== null) {
        discoverDispatch({
          type: "[discover data structure] file-delimiter-changed",
          delimiter,
        });
      }
    },
    [discoverDispatch],
  );

  const onDateFormatChange = useCallback(
    (dateFormat: FilePreferredDateFormat) => {
      if (previewDispatch && dateFormat !== null) {
        previewDispatch({
          type: "[discover data structure] date-format-changed",
          dateFormat,
        });
      }
    },
    [discoverDispatch],
  );

  const onEncodingChange = useCallback(
    (encoding: FileEncodingOption) => {
      if (discoverDispatch && encoding !== null) {
        discoverDispatch({
          type: "[discover data structure] encoding-changed",
          encoding,
        });
      }
    },
    [discoverDispatch],
  );

  const onDecimalSeparatorChange = useCallback(
    (decimalSeparator: FileDecimalSeparator) => {
      if (previewDispatch && decimalSeparator !== null) {
        previewDispatch({
          type: "[discover data structure] decimal-separator-changed",
          decimalSeparator,
        });
      }
    },
    [discoverDispatch],
  );

  const onHasHeaderChange = useCallback(
    (hasHeader: FileHasHeader) => {
      if (discoverDispatch) {
        discoverDispatch({
          type: "[discover data structure] has-header-changed",
          hasHeader,
        });
      }
    },
    [discoverDispatch],
  );

  const onIgnoreHeadersChange = useCallback(
    (ignoreHeaders: FileIgnoreHeaders) => {
      if (discoverDispatch) {
        discoverDispatch({
          type: "[discover data structure] ignore-headers-changed",
          ignoreHeaders,
        });
      }
    },
    [discoverDispatch],
  );

  return (
    <FileDiscoveryOptionsPresentational
      isDisabled={isDisabled}
      fileOptions={editableFileOptions}
      encodingOptions={encodingOptions}
      onFileDelimiterChange={onFileDelimiterChange}
      onDateFormatChange={onDateFormatChange}
      onEncodingChange={onEncodingChange}
      onDecimalSeparatorChange={onDecimalSeparatorChange}
      onHasHeaderChange={onHasHeaderChange}
      onIgnoreHeadersChange={onIgnoreHeadersChange}
    />
  );
};

export default FileDiscoveryOptions;
