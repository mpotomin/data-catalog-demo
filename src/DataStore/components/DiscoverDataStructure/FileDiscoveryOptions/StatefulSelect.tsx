import { Select, SelectProps, styled } from "@operational/components";
import { Value } from "@operational/components/lib/Select/Select.types";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";

const InsideSelect = styled(Select)`
  width: 100%;
`;

const StatefulSelect = injectIntl(({ intl, value, onChange, options, ...props }: SelectProps & InjectedIntlProps) => {
  if (typeof value !== "string") {
    return null;
  }

  const customSelectLabel = useMemo(
    () =>
      intl.formatMessage({
        id: "dataStore.tables.createWizard.confirmDataStructure.customOption",
        defaultMessage: "Custom...",
      }),
    [],
  );

  const [state, setState] = useState<Value>(value);
  const [customInputValue, setCustomInputValue] = useState<string>("");

  useEffect(() => {
    if (value !== state) {
      setCustomInputValue("");
      setState(value);
    }
  }, [value]);

  const changeHandler = React.useCallback((newValue, incoming) => {
    if (options.map(o => o.value).includes(newValue)) {
      setState(newValue);
      setCustomInputValue("");
      if (onChange) {
        onChange(newValue);
      }
      return;
    }
    if (incoming.label === customSelectLabel) {
      setCustomInputValue(newValue ? newValue[0] : "");
      setState(newValue && newValue[0]);
      if (onChange && newValue) {
        onChange(newValue[0]);
      }
    } else {
      setState(newValue);
    }
  }, []);

  const onBlur = useCallback(
    (val: Value) => {
      if (val === "") {
        changeHandler(options[0].value, options[0].value);
      }
    },
    [options],
  );

  return (
    <InsideSelect
      {...props}
      options={options}
      value={state}
      onChange={changeHandler}
      customOption={{ label: customSelectLabel, value: customInputValue }}
      customInputSettings={{ onBlur }}
    />
  );
});

export default StatefulSelect;
