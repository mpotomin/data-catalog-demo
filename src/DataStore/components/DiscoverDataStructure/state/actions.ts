import {
  FileDecimalSeparator,
  FileDelimiter,
  FileDiscoveryResponse,
  FileEncodingOption,
  FileHasHeader,
  FileIgnoreHeaders,
  FilePreferredDateFormat,
  FilePreviewResponse,
  ManagedTableColumnType,
} from "../../../../queries/hub/hub";

import { ErrorResponse } from "../../../../util/errors";

export const isDiscoverDataStructureAction = (
  action: DiscoverDataStructureAction | any,
): action is DiscoverDataStructureAction =>
  action !== undefined &&
  action !== null &&
  action.type &&
  typeof action.type === "string" &&
  action.type.startsWith("[discover data structure]");

export type DiscoverDataStructureAction =
  | {
      type: "[discover data structure] column-name-changed";
      index: number;
      name: string;
    }
  | {
      type: "[discover data structure] column-type-changed";
      index: number;
      columnType: ManagedTableColumnType;
    }
  | {
      type: "[discover data structure] column-mapping-changed";
      index: number;
      targetColumnName: string;
    }
  | {
      type: "[discover data structure] file-delimiter-changed";
      delimiter: FileDelimiter;
    }
  | {
      type: "[discover data structure] date-format-changed";
      dateFormat: FilePreferredDateFormat;
    }
  | {
      type: "[discover data structure] encoding-changed";
      encoding: FileEncodingOption;
    }
  | {
      type: "[discover data structure] decimal-separator-changed";
      decimalSeparator: FileDecimalSeparator;
    }
  | {
      type: "[discover data structure] has-header-changed";
      hasHeader: FileHasHeader;
    }
  | {
      type: "[discover data structure] ignore-headers-changed";
      ignoreHeaders: FileIgnoreHeaders;
    }
  | {
      type: "[discover data structure] discovery-request-started";
    }
  | {
      type: "[discover data structure] discovery-request-success";
      response: FileDiscoveryResponse;
    }
  | {
      type: "[discover data structure] discovery-request-failed";
    }
  | {
      type: "[discover data structure] preview-request-started";
    }
  | {
      type: "[discover data structure] preview-request-success";
      response: FilePreviewResponse;
    }
  | {
      type: "[discover data structure] preview-request-failed";
    }
  | {
      type: "[discover data structure] preview-request-error-message";
      error: ErrorResponse;
    };
