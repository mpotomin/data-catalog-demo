import { DiscoverDataState } from "../state";
import { getEmptyErrorTemplate } from "../../../../queries/hub/utils";
import { DiscoverDataStructureAction } from "./actions";

import {
  createBestGuessColumnMapping,
  getEmptyTableSchema,
  getTargetColumnType,
} from "../MapSchemaView/columnMappingUtils";

const getEditableSchemaCopy = (state: DiscoverDataState) => {
  return state.editableSchema ? { ...state.editableSchema } : undefined;
};

const getEditableFileOptionsCopy = (state: DiscoverDataState) => {
  return state.editableFileOptions ? { ...state.editableFileOptions } : undefined;
};

const getColumnMappingCopy = (state: DiscoverDataState) => {
  return state.columnMapping ? [...state.columnMapping] : undefined;
};

function reducer(state: DiscoverDataState, action: DiscoverDataStructureAction): DiscoverDataState {
  switch (action.type) {
    case "[discover data structure] column-name-changed":
      if (!state.editableSchema) {
        throw new Error("Schema can't be undefined in this part of the reducer");
      }
      const columnsDiscoveryNameChanged = [...state.editableSchema.columns];
      columnsDiscoveryNameChanged[action.index] = {
        ...state.editableSchema.columns[action.index],
        name: action.name,
      };
      return {
        ...state,
        previousEditableSchema: getEditableSchemaCopy(state),
        previousEditableFileOptions: getEditableFileOptionsCopy(state),
        editableSchema: { ...state.editableSchema, columns: [...columnsDiscoveryNameChanged] },
      };
    case "[discover data structure] column-type-changed":
      if (!state.editableSchema) {
        throw new Error("Schema can't be undefined in this part of the reducer");
      }
      const columnsDiscoveryTypeChanged = [...state.editableSchema.columns];
      columnsDiscoveryTypeChanged[action.index] = {
        ...state.editableSchema.columns[action.index],
        type: action.columnType,
      };
      return {
        ...state,
        previousEditableSchema: getEditableSchemaCopy(state),
        previousEditableFileOptions: getEditableFileOptionsCopy(state),
        editableSchema: { ...state.editableSchema, columns: [...columnsDiscoveryTypeChanged] },
      };
    case "[discover data structure] column-mapping-changed":
      if (!state.columnMapping || !state.editableSchema || !state.targetSchema) {
        throw new Error("Column mapping can't be undefined in this part of the reducer");
      }
      const updatedMapping = [...state.columnMapping];
      updatedMapping[action.index] = {
        ...state.columnMapping[action.index],
        target: action.targetColumnName,
      };
      const columnsDiscoveryTypeUpdated = [...state.editableSchema.columns];
      columnsDiscoveryTypeUpdated[action.index] = {
        ...state.editableSchema.columns[action.index],
        type: getTargetColumnType(action.index, state.editableSchema, updatedMapping, state.targetSchema),
      };
      return {
        ...state,
        columnMapping: updatedMapping,
        previousColumnMapping: getColumnMappingCopy(state),
        previousEditableSchema: getEditableSchemaCopy(state),
        previousEditableFileOptions: getEditableFileOptionsCopy(state),
        editableSchema: { ...state.editableSchema, columns: [...columnsDiscoveryTypeUpdated] },
      };
    case "[discover data structure] file-delimiter-changed":
      if (!state.editableFileOptions) {
        throw new Error("File options can't be undefined in this part of the reducer");
      }
      return {
        ...state,
        previousEditableSchema: getEditableSchemaCopy(state),
        previousEditableFileOptions: getEditableFileOptionsCopy(state),
        editableFileOptions: { ...state.editableFileOptions, delimiter: action.delimiter },
      };
    case "[discover data structure] date-format-changed":
      if (!state.editableFileOptions) {
        throw new Error("File options can't be undefined in this part of the reducer");
      }
      return {
        ...state,
        previousEditableSchema: getEditableSchemaCopy(state),
        previousEditableFileOptions: getEditableFileOptionsCopy(state),
        editableFileOptions: { ...state.editableFileOptions, preferredDateFormat: action.dateFormat },
      };
    case "[discover data structure] encoding-changed":
      if (!state.editableFileOptions) {
        throw new Error("File options can't be undefined in this part of the reducer");
      }
      return {
        ...state,
        previousEditableSchema: getEditableSchemaCopy(state),
        previousEditableFileOptions: getEditableFileOptionsCopy(state),
        editableFileOptions: { ...state.editableFileOptions, encoding: action.encoding },
      };
    case "[discover data structure] decimal-separator-changed":
      if (!state.editableFileOptions) {
        throw new Error("File options can't be undefined in this part of the reducer");
      }
      return {
        ...state,
        previousEditableSchema: getEditableSchemaCopy(state),
        previousEditableFileOptions: getEditableFileOptionsCopy(state),
        editableFileOptions: {
          ...state.editableFileOptions,
          decimalSeparator: action.decimalSeparator,
        },
      };
    case "[discover data structure] has-header-changed":
      if (!state.editableFileOptions) {
        throw new Error("File options can't be undefined in this part of the reducer");
      }
      return {
        ...state,
        previousEditableSchema: getEditableSchemaCopy(state),
        previousEditableFileOptions: getEditableFileOptionsCopy(state),
        editableFileOptions: {
          ...state.editableFileOptions,
          hasHeader: action.hasHeader,
        },
      };
    case "[discover data structure] ignore-headers-changed":
      if (!state.editableFileOptions) {
        throw new Error("File options can't be undefined in this part of the reducer");
      }
      return {
        ...state,
        previousEditableSchema: getEditableSchemaCopy(state),
        previousEditableFileOptions: getEditableFileOptionsCopy(state),
        editableFileOptions: {
          ...state.editableFileOptions,
          ignoreHeader: action.ignoreHeaders,
        },
      };
    case "[discover data structure] discovery-request-started":
      return {
        ...state,
        isFileDiscoveryRunning: true,
      };
    case "[discover data structure] discovery-request-success":
      return {
        ...state,
        isFileDiscoveryRunning: false,
        preview: action.response.preview,
        editableSchema: { ...action.response.schema },
        columnMapping: createBestGuessColumnMapping(
          action.response.schema,
          state.targetSchema ? state.targetSchema : getEmptyTableSchema(),
        ),
        error: action.response.schemaErrors,
      };
    case "[discover data structure] discovery-request-failed":
      const editableSchemaDiscovery = state.previousEditableSchema ? { ...state.previousEditableSchema } : undefined;
      const editableFileOptionsDiscovery = state.previousEditableFileOptions
        ? { ...state.previousEditableFileOptions }
        : undefined;
      return {
        ...state,
        isFileDiscoveryRunning: false,
        editableSchema: editableSchemaDiscovery,
        editableFileOptions: editableFileOptionsDiscovery,
      };
    case "[discover data structure] preview-request-started":
      return {
        ...state,
        isFileDiscoveryRunning: true,
      };
    case "[discover data structure] preview-request-success":
      return {
        ...state,
        isFileDiscoveryRunning: false,
        preview: action.response.preview,
        error: getEmptyErrorTemplate(),
      };
    case "[discover data structure] preview-request-failed":
      const editableSchemaPreview = state.previousEditableSchema ? { ...state.previousEditableSchema } : undefined;
      const editableFileOptionsPreview = state.previousEditableFileOptions
        ? { ...state.previousEditableFileOptions }
        : undefined;
      const editableMappingPreview = state.previousColumnMapping ? [...state.previousColumnMapping] : undefined;
      return {
        ...state,
        isFileDiscoveryRunning: false,
        editableSchema: editableSchemaPreview,
        editableFileOptions: editableFileOptionsPreview,
        columnMapping: editableMappingPreview,
      };
    case "[discover data structure] preview-request-error-message":
      return {
        ...state,
        isFileDiscoveryRunning: false,
        error: action.error,
      };
  }
}

export default reducer;
