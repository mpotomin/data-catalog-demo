import {
  ColumnMapping,
  FileDiscoveryResponse,
  FileOptions,
  ManagedTableSchema,
  SchemaPreviewCell,
} from "../../../../queries/hub/hub";

import { IOption } from "@operational/components/lib/Select/Select.types";
import { Maybe } from "../../../../types";
import { ErrorResponse } from "../../../../util/errors";

export type DiscoverDataState = typeof initialState;

export const initialState = Object.freeze({
  isFileDiscoveryRunning: false,
  fileDiscoveryResponse: undefined as Maybe<FileDiscoveryResponse>,
  preview: undefined as Maybe<SchemaPreviewCell[][]>,
  editableSchema: undefined as Maybe<ManagedTableSchema>,
  editableFileOptions: undefined as Maybe<FileOptions>,
  previousEditableSchema: undefined as Maybe<ManagedTableSchema>,
  previousEditableFileOptions: undefined as Maybe<FileOptions>,
  targetSchema: null as ManagedTableSchema | null,
  targetColumnOptions: undefined as Maybe<IOption[]>,
  columnMapping: undefined as Maybe<ColumnMapping>,
  previousColumnMapping: undefined as Maybe<ColumnMapping>,
  error: null as ErrorResponse | null,
});
