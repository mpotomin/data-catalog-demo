import {
  getColumnMappingTemplate,
  getFileOptionsTemplate,
  getSchemaTemplate,
  getTestErrorTemplate,
} from "../../../../queries/hub/testUtils";

import { defaultSchemaColumn } from "../../../../queries/hub/utils";
import { DiscoverDataState, initialState } from ".";
import { ManagedTableSchema } from "../../../../queries/hub/hub";
import { createBestGuessColumnMapping, getEmptyTableSchema } from "../MapSchemaView/columnMappingUtils";
import reducer from "./reducer";

function getInitialState(): DiscoverDataState {
  return {
    ...initialState,
    editableSchema: {
      ...getSchemaTemplate(),
      columns: [
        { ...defaultSchemaColumn, name: "col0", type: "bool" },
        { ...defaultSchemaColumn, name: "col1", type: "decimal" },
        { ...defaultSchemaColumn, name: "col2", type: "integer" },
      ],
    },
    targetSchema: {
      ...getSchemaTemplate(),
      columns: [
        { ...defaultSchemaColumn, name: "col0", type: "bool" },
        { ...defaultSchemaColumn, name: "col1", type: "decimal" },
        { ...defaultSchemaColumn, name: "col2", type: "integer" },
      ],
    },
    previousEditableSchema: undefined,
  };
}

describe("DiscoverDataStructure reducer", () => {
  describe("[discover data structure] column-name-changed", () => {
    const state = getInitialState();
    const newState = reducer(state, {
      type: "[discover data structure] column-name-changed",
      index: 1,
      name: "new-col-name-value",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should update the name of the column in state", () => {
      expect(newState.editableSchema.columns[1].name).not.toEqual(getInitialState().editableSchema.columns[1].name);
      expect(newState.editableSchema.columns[1].name).toEqual("new-col-name-value");
    });

    it("should store previous schema state", () => {
      expect(newState.editableSchema).not.toEqual(newState.previousEditableSchema);
      expect(newState.previousEditableSchema).toEqual(getInitialState().editableSchema);
    });
  });

  describe("[discover data structure] column-type-changed", () => {
    it("should return a new object", () => {
      const state = getInitialState();
      const newState = reducer(state, {
        type: "[discover data structure] column-type-changed",
        index: 0,
        columnType: "time",
      });

      expect(state === newState).toBeFalsy();
    });

    it("should update the type of the column in state", () => {
      const newState = reducer(getInitialState(), {
        type: "[discover data structure] column-type-changed",
        index: 1,
        columnType: "time",
      });

      expect(newState.editableSchema.columns[1].type).not.toEqual(getInitialState().editableSchema.columns[1].type);
      expect(newState.editableSchema.columns[1].type).toEqual("time");
    });

    it("should store previous schema state", () => {
      const newState = reducer(getInitialState(), {
        type: "[discover data structure] column-type-changed",
        index: 1,
        columnType: "time",
      });

      expect(newState.editableSchema).not.toEqual(newState.previousEditableSchema);
      expect(newState.previousEditableSchema).toEqual(getInitialState().editableSchema);
    });
  });

  describe("[discover data structure] column-mapping-changed", () => {
    it("should return a new object", () => {
      const state = { ...getInitialState(), columnMapping: [{ source: "", target: "" }] };
      const newState = reducer(state, {
        type: "[discover data structure] column-mapping-changed",
        index: 0,
        targetColumnName: "new-name",
      });

      expect(state === newState).toBeFalsy();
    });

    it("should update the target of the column in mapping state", () => {
      const initialState1 = {
        ...getInitialState(),
        columnMapping: getColumnMappingTemplate(),
      };
      const newState = reducer(initialState1, {
        type: "[discover data structure] column-mapping-changed",
        index: 1,
        targetColumnName: "new-target-name",
      });

      expect(newState.columnMapping[1].target).not.toEqual(initialState1.columnMapping[1].target);
      expect(newState.columnMapping[1].target).toEqual("new-target-name");
    });

    it("should update the type of the source column in state", () => {
      const initialState2: DiscoverDataState = {
        ...getInitialState(),
        columnMapping: getColumnMappingTemplate(),
        editableSchema: {
          ...getSchemaTemplate(),
          columns: [
            { ...defaultSchemaColumn, name: "source-1", type: "bool" },
            { ...defaultSchemaColumn, name: "source-2", type: "decimal" },
            { ...defaultSchemaColumn, name: "source-3", type: "integer" },
          ],
        },
        targetSchema: {
          ...getSchemaTemplate(),
          columns: [
            { ...defaultSchemaColumn, name: "target-1", type: "bool" },
            { ...defaultSchemaColumn, name: "target-2", type: "decimal" },
            { ...defaultSchemaColumn, name: "target-3", type: "integer" },
          ],
        },
      };

      const newState = reducer(initialState2, {
        type: "[discover data structure] column-mapping-changed",
        index: 1,
        targetColumnName: "target-3",
      });

      expect(newState.editableSchema.columns[1].type).not.toEqual(getInitialState().editableSchema.columns[1].type);
      expect(newState.editableSchema.columns[1].type).toEqual("integer");
    });

    it("should store previous state of the mapping in the state", () => {
      const initialState3 = {
        ...getInitialState(),
        columnMapping: getColumnMappingTemplate(),
      };
      const newState = reducer(initialState3, {
        type: "[discover data structure] column-mapping-changed",
        index: 1,
        targetColumnName: "new-target-name",
      });

      expect(newState.previousColumnMapping).not.toEqual(newState.columnMapping);
      expect(newState.previousColumnMapping).toEqual(initialState3.columnMapping);
    });
  });

  describe("[discover data structure] file-delimiter-changed", () => {
    const state: DiscoverDataState = {
      ...initialState,
      editableFileOptions: { ...getFileOptionsTemplate(), delimiter: "comma" },
      previousEditableFileOptions: undefined,
    };

    const newState = reducer(state, {
      type: "[discover data structure] file-delimiter-changed",
      delimiter: "tab",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should update delimiter option in state", () => {
      expect(state.editableFileOptions.delimiter).not.toEqual(newState.editableFileOptions.delimiter);
      expect(newState.editableFileOptions.delimiter).toEqual("tab");
    });

    it("should store previous file options state", () => {
      expect(newState.editableFileOptions).not.toEqual(newState.previousEditableFileOptions);
      expect(newState.previousEditableFileOptions).toEqual(state.editableFileOptions);
    });
  });

  describe("[discover data structure] date-format-changed", () => {
    const state: DiscoverDataState = {
      ...initialState,
      editableFileOptions: { ...getFileOptionsTemplate(), preferredDateFormat: "dayFirst" },
      previousEditableFileOptions: undefined,
    };

    const newState = reducer(state, {
      type: "[discover data structure] date-format-changed",
      dateFormat: "monthFirst",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should update date format option in state", () => {
      expect(state.editableFileOptions.preferredDateFormat).not.toEqual(
        newState.editableFileOptions.preferredDateFormat,
      );
      expect(newState.editableFileOptions.preferredDateFormat).toEqual("monthFirst");
    });

    it("should store previous file options state", () => {
      expect(newState.editableFileOptions).not.toEqual(newState.previousEditableFileOptions);
      expect(newState.previousEditableFileOptions).toEqual(state.editableFileOptions);
    });
  });

  describe("[discover data structure] encoding-changed", () => {
    const state: DiscoverDataState = {
      ...initialState,
      editableFileOptions: { ...getFileOptionsTemplate(), encoding: "UTF-8" },
      previousEditableFileOptions: undefined,
    };

    const newState = reducer(state, {
      type: "[discover data structure] encoding-changed",
      encoding: "KOI8-R",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should update encoding option in state", () => {
      expect(state.editableFileOptions.encoding).not.toEqual(newState.editableFileOptions.encoding);
      expect(newState.editableFileOptions.encoding).toEqual("KOI8-R");
    });

    it("should store previous file options state", () => {
      expect(newState.editableFileOptions).not.toEqual(newState.previousEditableFileOptions);
      expect(newState.previousEditableFileOptions).toEqual(state.editableFileOptions);
    });
  });

  describe("[discover data structure] decimal-separator-changed", () => {
    const state: DiscoverDataState = {
      ...initialState,
      editableFileOptions: { ...getFileOptionsTemplate(), decimalSeparator: "numericComma" },
      previousEditableFileOptions: undefined,
    };

    const newState = reducer(state, {
      type: "[discover data structure] decimal-separator-changed",
      decimalSeparator: "numericPeriod",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should update decimal separator option in state", () => {
      expect(state.editableFileOptions.decimalSeparator).not.toEqual(newState.editableFileOptions.decimalSeparator);
      expect(newState.editableFileOptions.decimalSeparator).toEqual("numericPeriod");
    });

    it("should store previous file options state", () => {
      expect(newState.editableFileOptions).not.toEqual(newState.previousEditableFileOptions);
      expect(newState.previousEditableFileOptions).toEqual(state.editableFileOptions);
    });
  });

  describe("[discover data structure] has-header-changed", () => {
    const state: DiscoverDataState = {
      ...initialState,
      editableFileOptions: { ...getFileOptionsTemplate(), hasHeader: false },
      previousEditableFileOptions: undefined,
    };

    const newState = reducer(state, {
      type: "[discover data structure] has-header-changed",
      hasHeader: true,
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should update hasHeader option in state", () => {
      expect(state.editableFileOptions.hasHeader).not.toEqual(newState.editableFileOptions.hasHeader);
      expect(newState.editableFileOptions.hasHeader).toBeTruthy();
    });

    it("should store previous file options state", () => {
      expect(newState.editableFileOptions).not.toEqual(newState.previousEditableFileOptions);
      expect(newState.previousEditableFileOptions).toEqual(state.editableFileOptions);
    });
  });

  describe("[discover data structure] ignore-headers-changed", () => {
    const state: DiscoverDataState = {
      ...initialState,
      editableFileOptions: { ...getFileOptionsTemplate(), ignoreHeader: false },
      previousEditableFileOptions: undefined,
    };

    const newState = reducer(state, {
      type: "[discover data structure] ignore-headers-changed",
      ignoreHeaders: true,
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should update ignoreHeaders option in state", () => {
      expect(state.editableFileOptions.ignoreHeader).not.toEqual(newState.editableFileOptions.ignoreHeader);
      expect(newState.editableFileOptions.ignoreHeader).toBeTruthy();
    });

    it("should store previous file options state", () => {
      expect(newState.editableFileOptions).not.toEqual(newState.previousEditableFileOptions);
      expect(newState.previousEditableFileOptions).toEqual(state.editableFileOptions);
    });
  });

  describe("[discover data structure] discovery-request-started", () => {
    const state: DiscoverDataState = {
      ...initialState,
      isFileDiscoveryRunning: false,
    };

    const newState = reducer(state, {
      type: "[discover data structure] discovery-request-started",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should enable discovery running flag", () => {
      expect(newState.isFileDiscoveryRunning).toBeTruthy();
    });
  });

  describe("[discover data structure] discovery-request-success", () => {
    const state: DiscoverDataState = {
      ...initialState,
      columnMapping: getColumnMappingTemplate(),
      isFileDiscoveryRunning: true,
    };

    const response = {
      schema: getSchemaTemplate(),
      targetSchema: null,
      schemaErrors: getTestErrorTemplate(),
      options: getFileOptionsTemplate(),
      preview: [[{ name: "name1", value: "val1", error: "err1" }]],
    };

    const newState = reducer(state, {
      type: "[discover data structure] discovery-request-success",
      response,
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should disable discovery running flag", () => {
      expect(newState.isFileDiscoveryRunning).toBeFalsy();
    });

    it("should overwrite the preview property with the value from the response", () => {
      expect(newState.preview).not.toEqual(state.preview);
      expect(newState.preview).toEqual(response.preview);
    });

    it("should overwrite the editable Schema with the value from the response", () => {
      expect(newState.editableSchema).not.toEqual(state.editableSchema);
      expect(newState.editableSchema).toEqual(response.schema);
    });

    it("should generate new best guess columnMapping", () => {
      expect(newState.columnMapping).not.toEqual(state.columnMapping);
      expect(newState.columnMapping).toEqual(
        createBestGuessColumnMapping(response.schema, state.targetSchema ? state.targetSchema : getEmptyTableSchema()),
      );
    });

    it("should set the fieldErrors value from the action payload", () => {
      expect(newState.error).not.toEqual(state.error);
      expect(newState.error).toEqual(response.schemaErrors);
    });
  });

  describe("[discover data structure] discovery-request-failed", () => {
    const editedSchema: ManagedTableSchema = { ...getSchemaTemplate() };
    editedSchema.columns[0].name = "Edited schema column name";

    const state: DiscoverDataState = {
      ...initialState,
      isFileDiscoveryRunning: true,
      editableSchema: editedSchema,
      editableFileOptions: {
        ...getFileOptionsTemplate(),
        delimiter: "tab",
      },
      previousEditableSchema: getSchemaTemplate(),
      previousEditableFileOptions: getFileOptionsTemplate(),
    };

    const newState = reducer(state, {
      type: "[discover data structure] discovery-request-failed",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should disable discovery running flag", () => {
      expect(newState.isFileDiscoveryRunning).toBeFalsy();
    });

    it("should overwrite current editable schema with the stored previous value", () => {
      expect(newState.editableSchema).not.toEqual(state.editableSchema);
      expect(newState.editableSchema).toEqual(state.previousEditableSchema);
    });

    it("should overwrite current editable file options with the stored previous value", () => {
      expect(newState.editableFileOptions).not.toEqual(state.editableFileOptions);
      expect(newState.editableFileOptions).toEqual(state.previousEditableFileOptions);
    });
  });

  describe("[discover data structure] preview-request-started", () => {
    const state: DiscoverDataState = {
      ...initialState,
      isFileDiscoveryRunning: false,
    };

    const newState = reducer(state, {
      type: "[discover data structure] preview-request-started",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should enable discovery running flag", () => {
      expect(newState.isFileDiscoveryRunning).toBeTruthy();
    });
  });

  describe("[discover data structure] preview-request-success", () => {
    const state: DiscoverDataState = {
      ...initialState,
      isFileDiscoveryRunning: true,
    };

    const response = {
      schema: getSchemaTemplate(),
      options: getFileOptionsTemplate(),
      preview: [[{ name: "name1", value: "val1", error: "err1" }]],
    };

    const newState = reducer(state, {
      type: "[discover data structure] preview-request-success",
      response,
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should disable discovery running flag", () => {
      expect(newState.isFileDiscoveryRunning).toBeFalsy();
    });

    it("should overwrite the preview property with the value from the response", () => {
      expect(newState.preview).not.toEqual(state.preview);
      expect(newState.preview).toEqual(response.preview);
    });
  });

  describe("[discover data structure] preview-request-failed", () => {
    const editedSchema: ManagedTableSchema = { ...getSchemaTemplate() };
    editedSchema.columns[0].name = "Edited schema column name";

    const state: DiscoverDataState = {
      ...initialState,
      isFileDiscoveryRunning: true,
      editableSchema: editedSchema,
      editableFileOptions: {
        ...getFileOptionsTemplate(),
        delimiter: "tab",
      },
      columnMapping: getColumnMappingTemplate(),
      previousEditableSchema: getSchemaTemplate(),
      previousEditableFileOptions: getFileOptionsTemplate(),
      previousColumnMapping: [...getColumnMappingTemplate(), { source: "new-source", target: "new-target" }],
    };

    const newState = reducer(state, {
      type: "[discover data structure] preview-request-failed",
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should disable discovery running flag", () => {
      expect(newState.isFileDiscoveryRunning).toBeFalsy();
    });

    it("should overwrite current editable schema with the stored previous value", () => {
      expect(newState.editableSchema).not.toEqual(state.editableSchema);
      expect(newState.editableSchema).toEqual(state.previousEditableSchema);
    });

    it("should overwrite current editable file options with the stored previous value", () => {
      expect(newState.editableFileOptions).not.toEqual(state.editableFileOptions);
      expect(newState.editableFileOptions).toEqual(state.previousEditableFileOptions);
    });

    it("should overwrite current editable mapping with the stored previous value", () => {
      expect(newState.columnMapping).not.toEqual(state.columnMapping);
      expect(newState.columnMapping).toEqual(state.previousColumnMapping);
    });
  });

  describe("[discover data structure] preview-request-error-message", () => {
    const editedSchema: ManagedTableSchema = { ...getSchemaTemplate() };
    editedSchema.columns[0].name = "Edited schema column name";

    const state: DiscoverDataState = {
      ...initialState,
      isFileDiscoveryRunning: true,
      editableSchema: editedSchema,
      editableFileOptions: {
        ...getFileOptionsTemplate(),
        delimiter: "tab",
      },
      previousEditableSchema: getSchemaTemplate(),
      previousEditableFileOptions: getFileOptionsTemplate(),
    };

    const newState = reducer(state, {
      type: "[discover data structure] preview-request-error-message",
      error: getTestErrorTemplate(),
    });

    it("should return a new object", () => {
      expect(state === newState).toBeFalsy();
    });

    it("should disable discovery running flag", () => {
      expect(newState.isFileDiscoveryRunning).toBeFalsy();
    });

    it("should not overwrite current editable schema with the stored previous value", () => {
      expect(newState.editableSchema).toEqual(state.editableSchema);
    });

    it("should not overwrite current editable file options with the stored previous value", () => {
      expect(newState.editableFileOptions).toEqual(state.editableFileOptions);
    });

    it("should overwrite the fieldErrors of the local state slice", () => {
      expect(newState.error).toEqual(getTestErrorTemplate());
    });
  });
});
