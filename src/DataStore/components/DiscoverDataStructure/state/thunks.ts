import {
  FileDiscoveryRequest,
  FileDiscoveryResponse,
  FileOptions,
  FilePreviewRequest,
  FilePreviewResponse,
  ManagedTableSchema,
} from "../../../../queries/hub/hub";

import { IMessage } from "@operational/components/lib/OperationalContext/OperationalContext";
import { MutateMethod } from "restful-react";
import { DiscoverDataState } from "../state";
import { getErrorData, getGeneralErrors } from "../../../../util/errors";
import { DiscoverDataStructureAction } from "./actions";
import reducer from "./reducer";

export const discoverFileThunk = (
  action: DiscoverDataStructureAction,
  state: DiscoverDataState,
  discoverFile: MutateMethod<FileDiscoveryResponse, FileDiscoveryRequest>,
  pushMessage: (message: IMessage) => void,
) => (dispatch: React.Dispatch<DiscoverDataStructureAction>) => {
  dispatch(action);
  const newState = reducer(state, action);
  const { editableFileOptions } = newState;
  if (editableFileOptions) {
    discover(dispatch, discoverFile, editableFileOptions, pushMessage);
  }
};

const discover = (
  dispatch: React.Dispatch<DiscoverDataStructureAction>,
  discoverFile: MutateMethod<FileDiscoveryResponse, FileDiscoveryRequest>,
  editableFileOptions: FileOptions,
  pushMessage: (message: IMessage) => void,
) => {
  dispatch({ type: "[discover data structure] discovery-request-started" });
  if (!editableFileOptions) {
    pushMessage({ body: "Can not send discovery request: missing schema or options", type: "error" });
    dispatch({ type: "[discover data structure] discovery-request-failed" });
    return;
  }

  discoverFile({ options: editableFileOptions })
    .then(response => {
      dispatch({ type: "[discover data structure] discovery-request-success", response });
    })
    .catch(() => {
      dispatch({ type: "[discover data structure] discovery-request-failed" });
    });
};

export const previewFileThunk = (
  action: DiscoverDataStructureAction,
  state: DiscoverDataState,
  previewFile: MutateMethod<FilePreviewResponse, FilePreviewRequest>,
  pushMessage: (message: IMessage) => void,
) => (dispatch: React.Dispatch<DiscoverDataStructureAction>) => {
  dispatch(action);
  const newState = reducer(state, action);
  const { editableSchema, editableFileOptions } = newState;
  if (editableSchema && editableFileOptions) {
    preview(dispatch, previewFile, editableSchema, editableFileOptions, pushMessage);
  }
};

const preview = (
  dispatch: React.Dispatch<DiscoverDataStructureAction>,
  previewFile: MutateMethod<FilePreviewResponse, FilePreviewRequest>,
  editableSchema: ManagedTableSchema,
  editableFileOptions: FileOptions,
  pushMessage: (message: IMessage) => void,
) => {
  dispatch({ type: "[discover data structure] preview-request-started" });
  if (!editableSchema || !editableFileOptions) {
    pushMessage({ body: "Can not send preview request: missing schema or options", type: "error" });
    dispatch({ type: "[discover data structure] preview-request-failed" });
    return;
  }

  previewFile({ schema: editableSchema, options: editableFileOptions })
    .then(response => {
      dispatch({ type: "[discover data structure] preview-request-success", response });
    })
    .catch(error => {
      if (error.status === 422 && error.data) {
        dispatch({ type: "[discover data structure] preview-request-error-message", error: getErrorData(error) });
        return;
      }
      const errors = getGeneralErrors(getErrorData(error));
      pushMessage({ body: errors ? errors : "Error", type: "error" });
      dispatch({ type: "[discover data structure] preview-request-failed" });
    });
};
