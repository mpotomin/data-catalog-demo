import React from "react";
import { Maybe } from "../../../types";
import { DiscoverDataState } from "./state";
import { DiscoverDataStructureAction } from "./state/actions";

const initial = Object.freeze({
  isDisabled: false,
  state: undefined as Maybe<DiscoverDataState>,
  discoverDispatch: undefined as Maybe<(action: DiscoverDataStructureAction) => void>,
  previewDispatch: undefined as Maybe<(action: DiscoverDataStructureAction) => void>,
});

const DiscoverDataStructureContext = React.createContext(initial);

export default DiscoverDataStructureContext;
