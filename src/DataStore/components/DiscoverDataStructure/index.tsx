import { CardColumn } from "@operational/components";
import React from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import FileDiscoveryOptions from "./FileDiscoveryOptions";
import MapSchemaView from "./MapSchemaView";
import ModifySchemaView from "./ModifySchemaView";

interface DiscoverDataStructureProps {
  mode: "modify-schema" | "map-to-existing-target-schema";
}

const DiscoverDataStructure = injectIntl(({ mode, intl }: DiscoverDataStructureProps & InjectedIntlProps) => {
  return (
    <CardColumn
      title={intl.formatMessage({
        id: "dataStore.tables.createWizard.confirmDataStructure.discovery.heading",
        defaultMessage: "Preview",
      })}
    >
      <>
        <FileDiscoveryOptions />
        {mode === "modify-schema" && <ModifySchemaView />}
        {mode === "map-to-existing-target-schema" && <MapSchemaView />}
      </>
    </CardColumn>
  );
});

export default DiscoverDataStructure;
