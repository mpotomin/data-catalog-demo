import { ResourceName, styled, Tooltip } from "@operational/components";
import { defaultRowHeight } from "@operational/components/lib/DataTable/DataTable.util";
import { setAlpha } from "@operational/components/lib/utils";
import React from "react";
import { sliceTextIfNeeded } from "../../../util/sliceTextIfNeeded";
import useHover from "../useHover";

interface Props {
  value: string;
  disabled: boolean;
  error: string;
  strong?: boolean;
}

const Container = styled.div<{ disabled: boolean; hasError: boolean; strong?: boolean }>`
  position: relative;
  display: flex;
  align-items: center;

  /* Offsetting parent DataTable cell padding + extra pixel for the border */
  margin: 0 ${({ theme }) => -theme.space.content - 1}px;
  padding: 0 ${({ theme }) => 2 + theme.space.content}px;
  width: calc(100% + ${({ theme }) => 2 + 2 * theme.space.content}px);
  height: ${defaultRowHeight + 1}px; /* 1px for the border */

  font-family: ${({ theme }) => theme.font.family.code};
  color: ${({ hasError, theme }) => (hasError ? theme.color.text : theme.color.text)};

  border-style: solid;
  border-width: ${({ strong }) => (strong ? 2 : 1)}px;
  border-color: ${({ disabled, hasError, theme }) => {
    if (disabled) {
      return theme.color.border.disabled;
    }
    if (hasError) {
      return theme.color.warning;
    }
    return "transparent";
  }};
  background-color: ${({ disabled, hasError, theme }) => {
    if (disabled) {
      return theme.color.background.grey;
    }
    if (hasError) {
      return setAlpha(0.2)(theme.color.warning);
    }
    return "transparent";
  }};
`;

const StyledResourceName = styled(ResourceName)`
  white-space: nowrap;
`;

const Cell = ({ value, disabled, error, strong }: Props) => {
  const { hoverRef, isHovered } = useHover<HTMLDivElement>();
  const isStrong = strong || value === "null";

  return (
    <Container ref={hoverRef} disabled={disabled} strong={strong} hasError={error.length > 0}>
      {<StyledResourceName strong={isStrong}>{sliceTextIfNeeded(value, 20)}</StyledResourceName>}
      {error.length > 0 && isHovered && <Tooltip position="top">{error}</Tooltip>}
    </Container>
  );
};

export default Cell;
