import { styled } from "@operational/components";

export const Container = styled.div`
  display: grid;
`;

export const DataTableWrapper = styled.div`
  overflow: auto;
`;
