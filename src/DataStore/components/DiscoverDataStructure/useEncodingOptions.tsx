import { useEffect, useState } from "react";
import { fileEncodingOptionOptions } from "../../../queries/hub/selectOptions";
import { DiscoverDataState } from "./state";

const useEncodingOptions = (state: DiscoverDataState) => {
  const [encodingOptions, setEncodingOptions] = useState(fileEncodingOptionOptions);
  useEffect(() => {
    if (state.fileDiscoveryResponse === undefined || state.fileDiscoveryResponse.options === undefined) {
      return;
    }

    const filtered = encodingOptions.filter(option => {
      if (state.fileDiscoveryResponse === undefined || state.fileDiscoveryResponse.options === undefined) {
        return false;
      }
      return option.value === state.fileDiscoveryResponse.options.encoding;
    });

    if (filtered.length < 1) {
      setEncodingOptions([
        {
          label: state.fileDiscoveryResponse.options.encoding,
          value: state.fileDiscoveryResponse.options.encoding,
        },
        ...encodingOptions,
      ]);
    }
  }, [state.fileDiscoveryResponse]);

  return encodingOptions;
};

export default useEncodingOptions;
