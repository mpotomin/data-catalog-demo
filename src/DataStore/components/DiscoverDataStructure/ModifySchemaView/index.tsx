import React, { useCallback, useContext } from "react";
import { ManagedTableColumnType } from "../../../../queries/hub/hub";
import DiscoverDataStructureContext from "../DiscoverDataStructureContext";
import ModifySchemaViewPresentational from "./ModifySchemaViewPresentational";

const ModifySchemaView = () => {
  const { isDisabled, state, previewDispatch } = useContext(DiscoverDataStructureContext);
  if (!state) {
    return null;
  }

  const { editableFileOptions, editableSchema, preview, error } = state;
  if (!editableFileOptions || !editableSchema) {
    return null;
  }

  const onColumnNameChange = useCallback(
    (index: number, name: string) => {
      if (previewDispatch) {
        previewDispatch({
          type: "[discover data structure] column-name-changed",
          index,
          name,
        });
      }
    },
    [previewDispatch],
  );

  const onColumnTypeChange = useCallback(
    (index: number, columnType: ManagedTableColumnType) => {
      if (previewDispatch && columnType !== null) {
        previewDispatch({
          type: "[discover data structure] column-type-changed",
          index,
          columnType,
        });
      }
    },
    [previewDispatch],
  );

  return (
    <ModifySchemaViewPresentational
      schema={editableSchema}
      preview={preview}
      isDisabled={isDisabled}
      onColumnNameChange={onColumnNameChange}
      onColumnTypeChange={onColumnTypeChange}
      error={error}
    />
  );
};

export default ModifySchemaView;
