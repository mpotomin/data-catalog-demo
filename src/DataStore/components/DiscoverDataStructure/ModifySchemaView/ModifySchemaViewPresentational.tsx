import { DataTable, DataTableSelect } from "@operational/components";
import React, { useMemo } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { ManagedTableColumnType, ManagedTableSchema, SchemaPreviewCell } from "../../../../queries/hub/hub";
import { managedTableColumnTypeOptions } from "../../../../queries/hub/selectOptions";
import { getColumnHeaderError } from "../../../../queries/hub/utils";
import { ErrorResponse } from "../../../../util/errors";
import { getTableHeight } from "../../SchemaTable/tableBodyUtils";
import StatefulDataTableInput from "../../StatefulDataTableInput";
import TableInfo from "../../TableInfo";
import { Container, DataTableWrapper } from "../styled";
import { getEmptyTableBody, getTableBody } from "../utils";

interface DiscoverDataStructurePresentationalProps {
  schema: ManagedTableSchema;
  preview: SchemaPreviewCell[][] | undefined;
  isDisabled: boolean;
  onColumnNameChange: (columnIndex: number, name: string) => void;
  onColumnTypeChange: (columnIndex: number, columnType: ManagedTableColumnType) => void;
  error: ErrorResponse | null;
}

const FilePreviewPresentational = injectIntl(
  ({
    schema,
    preview,
    isDisabled,
    onColumnNameChange,
    onColumnTypeChange,
    error,
    intl,
  }: DiscoverDataStructurePresentationalProps & InjectedIntlProps) => {
    const placeholderText = useMemo(
      () =>
        intl.formatMessage({
          id: "dataStore.tables.createWizard.confirmDataStructure.field.placeholder",
          defaultMessage: "Fill here",
        }),
      [],
    );

    const tableData = useMemo(() => {
      const { columns } = schema;
      const data = preview && preview.length !== undefined ? preview : getEmptyTableBody(columns.length, 10);
      const tableBody = getTableBody(data);
      return {
        columns: columns.map(({ name, type }, index) => [
          <StatefulDataTableInput
            index={index}
            placeholderText={placeholderText}
            value={name}
            disabled={isDisabled}
            setGlobalState={(colIndex: number, newColName: string) => {
              if (name !== newColName) {
                onColumnNameChange(colIndex, newColName);
              }
            }}
            error={error ? getColumnHeaderError(error, index) : ""}
          />,
          <DataTableSelect
            options={managedTableColumnTypeOptions}
            value={type}
            disabled={isDisabled}
            onChange={newValue => {
              onColumnTypeChange(index, newValue as ManagedTableColumnType);
            }}
          />,
        ]),
        rows: tableBody,
      };
    }, [schema, onColumnTypeChange, error]);

    return (
      <Container>
        <TableInfo showInfo={false} preview={preview} error={error} showSchemaColumnHeaderErrors />
        <DataTableWrapper>
          <DataTable height={getTableHeight(tableData.rows.length)} columns={tableData.columns} rows={tableData.rows} />
        </DataTableWrapper>
      </Container>
    );
  },
);

export default FilePreviewPresentational;
