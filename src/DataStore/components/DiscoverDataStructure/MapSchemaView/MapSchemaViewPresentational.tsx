import { DataTable, DataTableSelect } from "@operational/components";
import { IOption } from "@operational/components/lib/Select/Select.types";
import React, { useMemo } from "react";
import { InjectedIntlProps, injectIntl } from "react-intl";
import { ColumnMapping, ManagedTableSchema, SchemaPreviewCell } from "../../../../queries/hub/hub";
import { ErrorResponse } from "../../../../util/errors";
import { getTableHeight } from "../../SchemaTable/tableBodyUtils";
import TableInfo from "../../TableInfo";
import { Container, DataTableWrapper } from "../styled";
import { getEmptyTableBody, getTableBody } from "../utils";

interface MapSchemaViewPresentationalProps {
  sourceSchema: ManagedTableSchema;
  targetColumnOptions: IOption[];
  columnMapping: ColumnMapping;
  preview: SchemaPreviewCell[][] | undefined;
  isDisabled: boolean;
  onMappingChanged: (columnIndex: number, sourceColumnName: string) => void;
  error: ErrorResponse | null;
}

const maxTargetSelectOptions = 7;

const MapSchemaViewPresentational = injectIntl(
  ({
    sourceSchema,
    targetColumnOptions,
    columnMapping,
    preview,
    isDisabled,
    onMappingChanged,
    error,
  }: MapSchemaViewPresentationalProps & InjectedIntlProps) => {
    const tableData = useMemo(() => {
      const { columns } = sourceSchema;
      const data = preview && preview.length !== undefined ? preview : getEmptyTableBody(columns.length, 10);
      const tableBody = getTableBody(data, columnMapping);
      return {
        columns: columns.map(({ name }, index) => [
          name,
          <DataTableSelect
            options={targetColumnOptions}
            maxOptions={targetColumnOptions.length > maxTargetSelectOptions ? maxTargetSelectOptions : undefined}
            filterable={targetColumnOptions.length > maxTargetSelectOptions ? true : false}
            value={columnMapping[index].target}
            disabled={isDisabled}
            onChange={newValue => {
              onMappingChanged(index, newValue as string);
            }}
          />,
        ]),
        rows: tableBody,
      };
    }, [sourceSchema, columnMapping, onMappingChanged, error]);

    return (
      <Container>
        <TableInfo showInfo={false} preview={preview} columnMapping={columnMapping} error={error} />
        <DataTableWrapper>
          <DataTable height={getTableHeight(tableData.rows.length)} columns={tableData.columns} rows={tableData.rows} />
        </DataTableWrapper>
      </Container>
    );
  },
);

export default MapSchemaViewPresentational;
