import { IOption } from "@operational/components/lib/Select/Select.types";
import { ColumnMapping, ManagedTableSchema } from "../../../../queries/hub/hub";

export const getEmptyTableSchema = (): ManagedTableSchema => ({
  columns: [],
  indexes: [],
});

export const dropColumnOptionValue = "__drop__column__";
const dropColumnOption: IOption = { label: "[Drop column]", value: dropColumnOptionValue };

export const getTargetColumnOptions = (schema: ManagedTableSchema): IOption[] => {
  const options = schema.columns.map(column => ({ label: `${column.name}[${column.type}]`, value: column.name }));
  return [...options, dropColumnOption];
};

export const createBestGuessColumnMapping = (
  sourceSchema: ManagedTableSchema,
  targetSchema: ManagedTableSchema,
): ColumnMapping => {
  return sourceSchema.columns.map(sourceColumn => {
    const bestFitTarget = targetSchema.columns.find(targetColumn => targetColumn.name === sourceColumn.name);
    return {
      source: sourceColumn.name,
      target: bestFitTarget ? bestFitTarget.name : dropColumnOptionValue,
    };
  });
};

export const getTargetColumnType = (
  sourceColumnIndex: number,
  sourceSchema: ManagedTableSchema,
  columnMapping: ColumnMapping,
  targetSchema: ManagedTableSchema,
) => {
  const sourceColumn = sourceSchema.columns[sourceColumnIndex];
  const mapItem = columnMapping.find(item => item.source === sourceColumn.name);
  const targetItem = targetSchema.columns.find(column => (mapItem ? column.name === mapItem.target : false));
  return targetItem ? targetItem.type : sourceColumn.type;
};
