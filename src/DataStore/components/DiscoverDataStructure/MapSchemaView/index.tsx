import React, { useCallback, useContext } from "react";
import DiscoverDataStructureContext from "../DiscoverDataStructureContext";
import MapSchemaViewPresentational from "./MapSchemaViewPresentational";

const MapSchemaView = () => {
  const { isDisabled, state, previewDispatch } = useContext(DiscoverDataStructureContext);
  if (!state) {
    return null;
  }

  const onMappingChanged = useCallback(
    (index: number, targetColumnName: string) => {
      if (previewDispatch && targetColumnName) {
        previewDispatch({
          type: "[discover data structure] column-mapping-changed",
          index,
          targetColumnName,
        });
      }
    },
    [previewDispatch],
  );

  const { editableSchema, targetSchema, targetColumnOptions, columnMapping, preview, error } = state;
  if (!editableSchema || !targetSchema || !targetColumnOptions || !columnMapping) {
    return null;
  }

  return (
    <MapSchemaViewPresentational
      sourceSchema={editableSchema}
      targetColumnOptions={targetColumnOptions}
      columnMapping={columnMapping}
      preview={preview}
      isDisabled={isDisabled}
      onMappingChanged={onMappingChanged}
      error={error}
    />
  );
};

export default MapSchemaView;
