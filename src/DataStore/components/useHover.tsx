import { useEffect, useRef, useState } from "react";

const useHover = <T extends HTMLElement | null>() => {
  const [isHovered, setIsHovered] = useState(false);
  const hoverRef = useRef<T>(null);

  const onMouseOver = () => setIsHovered(true);
  const onMouseOut = () => setIsHovered(false);

  useEffect(() => {
    const node = hoverRef.current;
    if (node) {
      node.addEventListener("mouseover", onMouseOver);
      node.addEventListener("mouseout", onMouseOut);

      return () => {
        node.removeEventListener("mouseover", onMouseOver);
        node.removeEventListener("mouseout", onMouseOut);
      };
    }
    return undefined;
  }, [hoverRef.current]);

  return { hoverRef, isHovered };
};

export default useHover;
