import { styled } from "@operational/components";

const NameDescriptionBox = styled.form`
  min-width: 800px;
  display: grid;
  grid-template-columns: 360px auto;
  grid-column-gap: ${({ theme }) => 2 * theme.space.big}px; /* double spacing according to the mockup */
  margin: 1px 1px ${({ theme }) => theme.space.big}px 1px; /* 1px for the input focus frame  */
`;

export default NameDescriptionBox;
