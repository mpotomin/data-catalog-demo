const acceptedUploadFileFormat = "text/csv,text/tab-separated-values";

export default acceptedUploadFileFormat;
