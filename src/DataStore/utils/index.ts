import { sliceTextIfNeeded } from "../../util/sliceTextIfNeeded";

export const getSlicedDescription = (description: string = ""): string =>
  description ? sliceTextIfNeeded(description, 100) : "";

export const removeLeadingSpace = (input: string): string => input.trimLeft();
