import { InfoIcon, SyncIcon, YesIcon } from "@operational/components";
import React from "react";
import { GetDataError } from "restful-react";
import { ManagedTableSchema } from "../../queries/hub/hub";
import { tableColumnNameRestriction } from "../../queries/hub/inputRestrictions";
import { InputRestrictions } from "../../queries/hub/types";
import { ErrorResponse } from "../../util/errors";

export const canCreateWithName = (nameState: NameState): boolean =>
  nameState.name.length > 0 && nameState.status === "available";

export type NameStatus = "empty" | "invalid" | "loading" | "in-use" | "available";

export const nameInitialState = Object.freeze({
  name: "",
  status: "empty" as NameStatus,
});

export type NameState = typeof nameInitialState;

export const getNameStatus = (
  name: string,
  restriction: InputRestrictions,
  nameMatcher: RegExp,
  isLoading: boolean,
  error: GetDataError<ErrorResponse> | null,
): NameStatus => {
  if (name.length < restriction.minLength) {
    return "empty";
  }

  if (name.length > restriction.maxLength) {
    return "invalid";
  }

  if (!nameMatcher.test(name)) {
    return "invalid";
  }

  if (isLoading) {
    return "loading";
  }

  if (error !== null) {
    return error.status === 404 ? "available" : "invalid";
  }

  return "in-use";
};

export const getInputStatusIcon = (status: NameStatus): React.ReactNode => {
  if (status === "loading") {
    return <SyncIcon size={14} color="primary" />;
  }

  if (status === "invalid" || status === "in-use") {
    return <InfoIcon color="error" size={16} />;
  }

  if (status === "available") {
    return <YesIcon color="success" size={14} />;
  }

  return null;
};

export const getInputRestrictionType = (status: NameStatus) => {
  if (status === "invalid" || status === "in-use") {
    return "error";
  }

  return "info";
};

export const isNameValid = (name: string, restriction: InputRestrictions, nameMatcher: RegExp): boolean => {
  if (name.length < restriction.minLength || name.length > restriction.maxLength) {
    return false;
  }

  if (!nameMatcher.test(name)) {
    return false;
  }

  return true;
};

export const tableColumnNameMatcher = RegExp(tableColumnNameRestriction.pattern);

export const getColumnNameLocalValidationStatuses = (schema: ManagedTableSchema): boolean[] =>
  schema.columns.map(column => isNameValid(column.name, tableColumnNameRestriction, tableColumnNameMatcher));
